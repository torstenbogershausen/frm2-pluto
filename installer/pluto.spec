# -*- mode: python -*-

import os
import sys
import subprocess
from os import path

rootdir = path.abspath('..')

# Make sure to generate the version file.
os.environ['PYTHONPATH'] = os.environ.get('PYTHONPATH', '') + path.pathsep + rootdir
subprocess.check_call([sys.executable,
                       path.join(rootdir, 'pluto', 'version.py')])


def find_modules(*modules):
    res = []
    startdir = path.join(rootdir, *modules)
    startmod = '.'.join(modules) + '.'
    for root, _dirs, files in os.walk(startdir):
        modpath = root[len(startdir) + 1:].replace(path.sep, '.')
        if modpath:
            modpath += '.'
        for mod in files:
            if mod.endswith('.py'):
                res.append(startmod + modpath + mod[:-3])
    return res


a = Analysis(['../plutomain.py'],
             pathex=['..'],
             hiddenimports=
                 find_modules('pluto', 'model', 'versions') +
                 find_modules('pluto', 'devices') +
                 find_modules('pluto', 'gui'),
             hookspath=None,
             runtime_hooks=['rthook_pyqt4.py'],
             datas = [('../pluto/RELEASE-VERSION', 'pluto'),
                      ('../pluto/gui/ui/*', 'pluto/gui/ui')])

for d in a.datas:
    if 'pyconfig' in d[0]:
        a.datas.remove(d)
        break

pyz = PYZ(a.pure, a.zipped_data)

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='Pluto.exe',
          debug=False,
          strip=None,
          upx=True,
          console=False)
