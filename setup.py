#!/usr/bin/env python
# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from os import path, listdir

from setuptools import setup, find_packages

import pluto.version

scripts = ['plutomain.py']

uidir = path.join(path.dirname(__file__), 'pluto', 'gui', 'ui')
pkgdata = [path.join('gui', 'ui', entry) for entry in listdir(uidir)]
pkgdata.append('RELEASE-VERSION')

setup(
    name = 'Pluto',
    version = pluto.version.get_version(),
    license = 'GPL',
    author = 'Stefan Rainow',
    author_email = 's.rainow@fz-juelich.de',
    description = 'PLC Debug Tool',
    packages = find_packages(),
    package_data = {'pluto': pkgdata},
    scripts = scripts,
    classifiers = [
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'Natural Language :: English',
        'License :: OSI Approved :: GPL License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Topic :: Scientific/Engineering',
        'Topic :: Scientific/Engineering :: Human Machine Interfaces',
    ],
)
