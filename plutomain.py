#!/usr/bin/env python
# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

import logging
import sys

from pluto.qt import QApplication, QCoreApplication

from pluto.gui.mainwindow import MainWindow


def main(argv=None):
    useStdOutForLogging = False
    if not argv:
        argv = sys.argv

    argvidx = 1
    while argvidx < len(argv):
        arg = argv[argvidx]
        if arg == '--logstdout':
            useStdOutForLogging = True
        elif arg.startswith('-'):
            print('usage   plutomain [--logstdout] [comm]')
            print('example plutomain tango://host:port/domain/class/member')
            print('example plutomain --logstdout tango://host:port/domain/class/member')
            print('example plutomain ads://127.0.0.1/127.0.0.1.1.1:852')
            print('example plutomain --logstdout ads://127.0.0.1/127.0.0.1.1.1:852')
            print('example plutomain # defaults to PyModbus')
            print('example plutomain --logstdout # defaults to PyModbus')
            sys.exit(1)
        else:
            address = arg
            if address.startswith('tango://'):
                comm = 'TANGO'
            elif address.startswith('ads://'):
                comm = 'ADS'
            else:
                comm = 'PyModbus'
        argvidx = argvidx + 1

    app = QApplication(argv)

    QCoreApplication.setOrganizationName('MLZ')
    QCoreApplication.setApplicationName('Pluto')

    window = MainWindow()
    window.move(0, 0)

    window.show()

    window.createNewPLC({
        'name': 'PLC',
        'stats': {
            'address': address,
            'name': 'PLC',
            'refresh': 500,
            'offset': 0,
        },
        'rawmode': None,
        'manual': None,
        'communication': comm,
    })

    if useStdOutForLogging:
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s: %(levelname)s: '
                            '%(pathname)s:%(lineno)d/%(funcName)s: %(message)s',
                            datefmt='%Y-%m-%d %I:%M:%S')
    else:
        logging.basicConfig(filename='pluto.log',
                            level=logging.INFO,
                            format='%(asctime)s: %(levelname)s: '
                            '%(pathname)s:%(lineno)d/%(funcName)s: %(message)s',
                            datefmt='%Y-%m-%d %I:%M:%S')

    return app.exec_()


if __name__ == '__main__':
    sys.exit(main(sys.argv))
