#!/usr/bin/env python
# -*- coding: utf-8 -*-

try:
    from configparser import SafeConfigParser
except ImportError:
    from ConfigParser import SafeConfigParser

import argparse
from pickle import loads
import ntpath


def getParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--path',
                        help='Path to the pluto save file you want to convert')
    return parser


def unpickle(path=None):
    if not path:
        path = getParser().parse_args().path

    if not ntpath.basename(path):
        print('No file specified.')
        return

    output = '%s.ini' % path.split('.')[0]

    unpickleParser = SafeConfigParser()
    unpickleParser.read(path)

    paramDicts = {}
    globalDict = {}
    for section in unpickleParser.sections():
        if section == 'global':
            globalDict = {option: unpickleParser.get(section, option)
                          for option in unpickleParser.options('global')}
        else:
            paramDicts[section] = loads(unpickleParser.get(section, 'paramDict'))

    vString = 'v%s_%s' % (globalDict['version'][:4], globalDict['version'][5:])
    version = __import__('pluto.versions.%s' % vString, None, None, ['*'])
    verConstants = version.VersionConstants()

    createIni(paramDicts, globalDict, output, verConstants)


def createIni(paramDicts, globalDict, output, verConsts):
    iniParser = SafeConfigParser()

    for section in sorted(paramDicts):
        paramDict = paramDicts[section]
        iniParser.add_section(section)

        # write all saved paramDict entries
        for option in paramDict:
            iniParser.set(section, option, str(paramDict[option]))

        # add additional keys here
        # e.g.:
        iniParser.set(section, 'hexoffset',
                      hex(int(paramDict['register']) + int(globalDict['offset'])))
        deviceCode = verConsts.getDeviceCode(paramDict['deviceType'],
                                             paramDict['subType'])
        iniParser.set(section, 'devicecode', hex(deviceCode))
        iniParser.set(section, 'subTypeName', paramDict['subType'][0].__name__)

    with open(output, 'w') as f:
        iniParser.write(f)
