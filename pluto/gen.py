# encoding: utf-8
# template generator for pluto
import logging
from os import path


# internal storage classes
class Mapping(dict):
    def __init__(self, _dict_=None, **kwds):
        dict.__init__(self)
        for i in range(256):
            self[i] = 'param%d' % i
            self['param%d' % i] = i
        for key, val in list((_dict_ or {}).items()) + list(kwds.items()):
            try:
                lower_val = val.lower()
            except AttributeError:
                lower_val = val
            try:
                lower_key = key.lower()
            except AttributeError:
                lower_key = key
            self[lower_val] = key
            self[lower_key] = val
            self[val] = key
            self[key] = val


class AttrDict(object):
    def __getitem__(self, key):
        return self.__dict__[key]


class PLC(AttrDict):
    alignment = 1
    COUPLER_TYPES = ['CX8090', 'BC9100']

    def __init__(self, plc_data):
        self.name = plc_data.get('name', '<PLC-Projektname>')
        self.coupler = plc_data.get('coupler', 'CX8090')
        self.version = plc_data.get('version', 'V0.0.0-\alpha')
        self.author1 = plc_data.get('author1', 'somebody')
        self.author2 = plc_data.get('author2', 'somewhere')
        self.indexer_size = max(6, min(66, (int(
            plc_data.get('indexer_size', 34)) + 1) & 0xFE))
        self.indexer_datasize = self.indexer_size - 2
        self.indexer_offset = int(plc_data.get('indexer_offset', 66))
        self.magic = round(plc_data.get('magic', 2015.02), 4)
        if len(self.name) > self.indexer_datasize:
            self.name = self.name[:self.indexer_datasize]
            print("WARNING: PLC.name too long, shortening to %d chars:\n%r\n" %
                  (self.indexer_datasize, self.name))
        if len(self.version) > self.indexer_datasize:
            self.version = self.version[:self.indexer_datasize]
            print("WARNING: PLC.version too long, shortening to %d chars:\n%r\n" %
                  (self.indexer_datasize, self.version))
        if len(self.author1) > self.indexer_datasize:
            self.author1 = self.author1[:self.indexer_datasize]
            print("WARNING: PLC.author1 too long, shortening to %d chars:\n%r\n" %
                  (self.indexer_datasize, self.author1))
        if len(self.author2) > self.indexer_datasize:
            self.author2 = self.author2[:self.indexer_datasize]
            print("WARNING: PLC.author2 too long, shortening to %d chars:\n%r\n" %
                  (self.indexer_datasize, self.author2))

ALL_DEVICES = {}


class Device(AttrDict):
    def __init__(self, d):
        # mandatory stuff
        self.name = d['name']
        self.typcode = d['typecode']
        if self.name in ALL_DEVICES:
            raise ValueError('Device %s already existing!' % self.name)
        ALL_DEVICES[self.name] = self
        # optional stuff
        self.aux = list(d.get('aux', []))
        self.size = d.get('size', 0)
        self.typsize = (self.typcode & 0xFF) * 2
        if self.size:
            self.size = max(self.size, self.typsize)
            if self.size == self.typsize:
                self.size = 0  # no need to specify

        self.unitcode = 0
        self.unit = d.get('unit', '')
        if self.unit not in UNITCODES:
            raise ValueError('Illegal unit %r' % self.unit)
        if isinstance(self.unit, str):
            self.unitcode = UNITCODES[self.unit]
        else:
            self.unitcode = self.unit
            self.unit = UNITCODES[self.unit]

        self.offset = d.get('offset', 0)
        self.klemme = d.get('module', '')  # if given: connect this device to that HW
        self.version = d.get('version', '')
        self.author1 = d.get('author1', '')
        self.author2 = d.get('author2', '')
        # if given: connect this device to those 'attached devices'
        self.adevs = d.get('adevs', [])
        if self.adevs and self.klemme:
            raise ValueError('Device %r can\'t use a klemme and adevs at the '
                             'same time!' % self.name)

        # set names needed later
        try:
            entry = TYPCODE_MAPPING[self.typcode]
        except KeyError:
            typcodes = [v for v in TYPCODE_MAPPING if isinstance(v, int)]
            higher = 0xffff
            lower = 0
            for code in typcodes:
                if self.typcode < code < higher:
                    higher = code
                elif self.typcode > code > lower:
                    lower = code
            raise ValueError('Unknown Typcode 0x%04x specified for device %s, '
                             'maybe you wanted:\n'
                             '0x%04x: %s\n0x%04x: %s' % (
                                 self.typcode, self.name,
                                 lower, TYPCODE_MAPPING.get(lower, '<UNKNOWN>'),
                                 higher, TYPCODE_MAPPING.get(higher, '<UNKNOWN>')
                             ))
        self.typname = entry[0]
        self.parameters = entry[1] if len(entry) > 1 else False
        self.num_values = 1
        if self.typcode >> 12 in (4, 5): # PARAM/VectorDevices
            self.num_values = ((self.typcode >> 8) & 0x0f) + 1
        self.ST_name = 'ST_' + self.typname
        self.FB_name = 'FB_' + (self.klemme if self.klemme else self.name)
        self.fb_name = 'fb' + self.name
        self.if_name = 'if_' + self.name
        self.has_persist = bool(self.klemme)
        if self.has_persist:
            self.hw_in_name = 'ST_%s_in' % self.klemme
            self.hw_out_name = 'ST_%s_out' % self.klemme
            self.persist_name = 'ST_%s_persist' % self.klemme

        # map strings to numbers + sort
        self.params = sorted(set([PARAM_MAPPING[p] if isinstance(p, str) else p
                                  for p in d.get('params', [])]))
        self.has_params = True if self.params else False
        # split into params and cmds
        self.cmds = [p for p in self.params if (p > 224) or (128 <= p < 192)]
        self.params = [p for p in self.params if (p < 128) or (192 <= p < 224)]
        # also keep the names in the same order
        self.paramnames = [PARAM_MAPPING[p] for p in self.params]
        # same for commands
        self.cmdnames = [PARAM_MAPPING[p] for p in self.cmds]
        # build DevInfo pattern (bitmap for non-flat, paramlist for flat)
        if self.params or self.cmds:
            if self.parameters is True:
                # IDX
                self.initparams = [0] * 16
                # create bitmap
                for param in self.params + self.cmds:
                    self.initparams[param // 16] |= 1 << (param & 15)
            else:  # FLAT
                # list parameters one by one
                if len(self.params) > self.parameters:
                    raise ValueError('Too many Parameters specified, only %d '
                                     'allowed for %s:%s' %
                                     (self.parameters, self.typname, self.name))
                params = self.params + [0]
                self.initparams = []
                for _ in range(0, len(self.params), 2):
                    a = params.pop(0)
                    b = params.pop(0)
                    self.initparams.append(b * 256 + a)

            # now shorten initparams as much as possible
            while self.initparams:
                if self.initparams[-1] == 0:
                    self.initparams.pop()
                else:
                    break

        # derive flags for code generation
        self.has_target = 'Output' in self.typname
        self.is_special = 'put' not in self.typname
        self.has_status = 'Simple' not in self.typname
        self.analog = 'Discrete' not in self.typname
        self.has_flat_params = 'Flat' in self.typname
        self.has_idx_params = self.typname.startswith(('Param', 'Vector'))
        self.datatype = "REAL" if self.analog else "INT "
        if self.num_values > 1:
            self.datatype = "ARRAY [1..%d] OF %s" % (self.num_values, self.datatype)
        self.minval = '-1E38' if self.analog else '-32768'
        self.maxval = '1E38' if self.analog else '32767'
        if self.has_flat_params:  # determine number of params
            self.has_flat_params = int(self.ST_name[self.ST_name.find('put') + 3:])


# mapping from typcode to (PLC-Stucture types, [params|paramIF])
TYPCODE_MAPPING = {
    0x0100: ('DEBUG',),
    0x1201: ('SimpleDiscreteInput',),
    0x1302: ('SimpleAnalogInput',),
    0x1401: ('KeyValue',),
    0x1502: ('RealValue',),
    0x1602: ('SimpleDiscreteOutput',),
    0x1704: ('SimpleAnalogOutput',),
    0x1801: ('Statuswort',),
    0x1a02: ('DiscreteInput',),
    0x1b03: ('AnalogInput',),
    0x1e03: ('DiscreteOutput',),
    0x1f05: ('AnalogOutput',),
    0x2006: ('FlatInput1', 1,),
    0x2108: ('FlatInput2', 2,),
    0x220a: ('FlatInput3', 3,),
    0x230c: ('FlatInput4', 4,),
    0x240e: ('FlatInput5', 5,),
    0x2510: ('FlatInput6', 6,),
    0x2612: ('FlatInput7', 7,),
    0x2714: ('FlatInput8', 8,),
    0x2816: ('FlatInput9', 9,),
    0x2918: ('FlatInput10', 10,),
    0x2a1a: ('FlatInput11', 11,),
    0x2b1c: ('FlatInput12', 12,),
    0x2c1e: ('FlatInput13', 13,),
    0x2d20: ('FlatInput14', 14,),
    0x2e22: ('FlatInput15', 15,),
    0x2f24: ('FlatInput16', 16,),
    0x3008: ('FlatOutput1', 1,),
    0x310a: ('FlatOutput2', 2,),
    0x320c: ('FlatOutput3', 3,),
    0x330e: ('FlatOutput4', 4,),
    0x3410: ('FlatOutput5', 5,),
    0x3512: ('FlatOutput6', 6,),
    0x3614: ('FlatOutput7', 7,),
    0x3716: ('FlatOutput8', 8,),
    0x3818: ('FlatOutput9', 9,),
    0x391a: ('FlatOutput10', 10,),
    0x3a1c: ('FlatOutput11', 11,),
    0x3b1e: ('FlatOutput12', 12,),
    0x3c20: ('FlatOutput13', 13,),
    0x3d22: ('FlatOutput14', 14,),
    0x3e24: ('FlatOutput15', 15,),
    0x3f26: ('FlatOutput16', 16,),
    0x4006: ('ParamInput', True,),
    0x5008: ('ParamOutput', True,),
    0x5010: ('ParamOutput64', True,),
    # since january 2016 (magic 2015.02)
    0x4108: ('VectorInput2', True,),
    0x420a: ('VectorInput3', True,),
    0x430c: ('VectorInput4', True,),
    0x440e: ('VectorInput5', True,),
    0x4510: ('VectorInput6', True,),
    0x4612: ('VectorInput7', True,),
    0x4714: ('VectorInput8', True,),
    0x4816: ('VectorInput9', True,),
    0x4918: ('VectorInput10', True,),
    0x4a1a: ('VectorInput11', True,),
    0x4b1c: ('VectorInput12', True,),
    0x4c1e: ('VectorInput13', True,),
    0x4d20: ('VectorInput14', True,),
    0x4e22: ('VectorInput15', True,),
    0x4f24: ('VectorInput16', True,),
    0x5010: ('ParamOutput64', True,),
    0x510c: ('VectorOutput2', True,),
    0x5210: ('VectorOutput3', True,),
    0x5314: ('VectorOutput4', True,),
    0x5418: ('VectorOutput5', True,),
    0x551c: ('VectorOutput6', True,),
    0x5620: ('VectorOutput7', True,),
    0x5724: ('VectorOutput8', True,),
    0x5828: ('VectorOutput9', True,),
    0x592c: ('VectorOutput10', True,),
    0x5a30: ('VectorOutput11', True,),
    0x5b34: ('VectorOutput12', True,),
    0x5c38: ('VectorOutput13', True,),
    0x5d3c: ('VectorOutput14', True,),
    0x5e40: ('VectorOutput15', True,),
    0x5f44: ('VectorOutput16', True,),
}

# indexes from 0 to 18
UNIT_MAPPING = '* V A W m g Hz T K degC degF bar deg Ohm m/s m^2/s m^3/s ' \
               's cts bar/s bar/s^2 F H'.split(' ')

# indexes from -24 to 24
PREFIX_MAPPING = '* da h k * * M * * G * * T * * P * * E * * Z * * Y | y * * ' \
                 'z * * a * * f * * p * * n * * u * * m c d'.split()

SPECIAL_UNITS = {0xfe00: '%', 0xfd10: 'l/s'}

class Unitcodes(dict):
    '''a dict containing a bidir mapping of all defined unitcodes/units.'''
    def __init__(self):
        dict.__init__(self)
        self[0] = ''
        self[''] = 0
        for unitcode, unit in enumerate(UNIT_MAPPING):
            if not unitcode:
                continue
            for prefixcode in range(-24, 25):
                prefix = PREFIX_MAPPING[prefixcode]
                if not prefixcode:
                    prefix = ''
                fullunitcode = ((prefixcode & 255) << 8) + unitcode
                if prefix != '*':
                    fullunit = prefix + unit
                else:
                    fullunit = '10^%d %s' % (prefixcode, unit)
                self[fullunitcode] = fullunit
                self[fullunit] = fullunitcode
        for k, v in SPECIAL_UNITS.items():
            self[k] = v
            self[v] = k

UNITCODES = Unitcodes()

PARAM_MAPPING = Mapping(
    # known and fixed numbers
    AbsMin=30,
    AbsMax=31,
    UserMin=32,
    UserMax=33,
    WarnMin=34,
    WarnMax=35,

    # not yet finally fixed but will probably stay
    Jerk=59,
    Speed=60,
    Accel=61,
    IdleCurrent=62,
    RampCurrent=63,
    MoveCurrent=64,
    StopCurrent=65,
    Deccel=66, # (Nothalt, nutzt keiner)
    MicroSteps=67,
    Slope=68,
    RefPos=69,

    # not yet fixed commands
    doAbort=130,
    doRefNeg=131,
    doRefPos=132,
    doReference=133,
    doSetPosition=137,

    # testing command, may move!
    doContMove=142,  # (* continuous move until stopped *)
    Offset=40,
    Unit=3,
    Status=4,
    ExtStatus=5,
    SafeVal=41,  # (braucht keiner)
    TimeOut=36,
    MaxTravelDist=37,
    PidP=51,
    PidI=52,
    PidD=53,
    DragError=55,  # max dragerror (diff: motor-encoder)
    Hysteresis=56,  # Zweipunktregler/deadband
)


class VAR(object):
    def __init__(self, name='', addr='', datatype='', initval='', comment=''):
        self.name = name
        self.addr = addr
        self.datatype = datatype if '_' in datatype else datatype.upper()
        self.initval = initval
        self.comment = comment
        # split datatype into basetype and multiplier
        self.multiplier = 1
        self.basetype = datatype.upper()
        if datatype.upper().startswith('ARRAY'):
            # determine array size and multiplier
            counts, rest = self.datatype.split('[')[1].split(']')
            self.basetype = rest.rsplit(' ')[1].upper()
            if '..' in counts:
                left, right = counts.split('..', 1)
                self.multiplier = int(right) - int(left) + 1
            else:
                self.multiplier = int(counts)
    _known_types = dict(BOOL=1, BYTE=1, WORD=2, DWORD=4, SINT=1, INT=2, DINT=4,
                        LINT=8, USINT=1, UINT=2, UDINT=4, ULINT=8, REAL=4, LREAL=8)

    @property
    def size(self):
        '''returns size of this var in bytes'''
        if self.basetype in self._known_types:
            return self.multiplier * self._known_types[self.basetype]
        if self.datatype in ALL_POU:
            return ALL_POU[self.datatype].size

ALL_POU = {}


class POU(object):
    def __init__(self, name, section=''):
        if name in ALL_POU:
            logging.debug( "Duplicate %s!" , '%s/%s' % (section, name) if section else name)
            raise ValueError('Can not create new %s of name %s as it already exist.' %
                             (self.__class__, name))
        self.name = name
        self.section = section
        ALL_POU[name] = self
        self.var = []
        self.var_input = []
        self.var_output = []
        self.var_in_out = []
        self.var_persist = []
        self.var_retain = []
        self.code = []  # each line is one code line

    def addCode(self, line):
        self.code.append(line)

    def addSep(self, stuff=''):
        '''put an empty line to separate things in vars'''
        self.var.append(stuff)

    def addVar(self, **kwds):
        self.var.append(VAR(**kwds))

    def addVar_Persist(self, **kwds):
        self.var_persist.append(VAR(**kwds))

    def addVar_Retain(self, **kwds):
        self.var_retain.append(VAR(**kwds))

    def addVar_Input(self, **kwds):
        self.var_input.append(VAR(**kwds))

    def addVar_Output(self, **kwds):
        self.var_output.append(VAR(**kwds))

    def addVar_In_Out(self, **kwds):
        self.var_in_out.append(VAR(**kwds))

    def dumpVars(self, vars_):
        '''format Var's and return a tuple of lines'''
        var_namelen = 0
        var_addrlen = 0
        for var in self.var + self.var_input + self.var_output + self.var_in_out:
            if not isinstance(var, VAR):
                continue
            var_namelen = max(var_namelen, len(var.name))
            var_addrlen = max(var_addrlen, len(var.addr))
        if var_addrlen:
            var_addrlen += 3  # add size of the 'AT '
        res = []
        for var in vars_:
            if not isinstance(var, VAR):
                try:
                    res.append(str(var))
                except Exception:
                    res.append('')
                continue
            if var.comment:
                res.append('    (* %s *)' % var.comment)
            l = ['    ']
            l.append(var.name.ljust(var_namelen))
            if var_addrlen:
                l.append(' ')
                if var.addr:
                    l.append(('AT %s' % var.addr).ljust(var_addrlen).upper())
                else:
                    l.append(' ' * var_addrlen)
            l.append(' : ')
            l.append(var.datatype)
            if var.initval:
                l.append(' := ')
                if 'STRING' in var.datatype:
                    l.append(repr(var.initval))
                else:
                    l.append(str(var.initval))
            l.append(';')
            res.append(''.join(l))
        return res

    def emit(self):
        '''should return a string correctly formatted for *.exp storage'''
        raise NotImplementedError

    def _ST_ENUM_header(self, p):
        p("")
        p("(* @NESTEDCOMMENTS := 'Yes' *)")
        p("(* @PATH := '%s' *)" % (('\\/%s' % self.section) if self.section else ''))
        p("(* @OBJECTFLAGS := '0, 8' *)")

    def _FB_PRG_header(self, p):
        p("")
        self._ST_ENUM_header(p)
        p("(* @SYMFILEFLAGS := '2048' *)")  # for FB/Program, not for STRUCT/ENUM


class FB(POU):
    def emit(self):
        '''returns the full *.exp type definition of this FB'''
        res = []
        p = res.append
        self._FB_PRG_header(p)
        p("FUNCTION_BLOCK %s" % self.name)
        if self.var_input:
            p("VAR_INPUT")
            res.extend(self.dumpVars(self.var_input))
            p("END_VAR")
        if self.var_output:
            p("VAR_OUTPUT")
            res.extend(self.dumpVars(self.var_output))
            p("END_VAR")
        if self.var_in_out:
            p("VAR_IN_OUT")
            res.extend(self.dumpVars(self.var_in_out))
            p("END_VAR")
        if self.var:
            p("VAR")
            res.extend(self.dumpVars(self.var))
            p("END_VAR")
        p("(* @END_DECLARATION := '0' *)")
        res.extend(self.code)
        p("END_FUNCTION_BLOCK")
        return '\n'.join(res)


class FUNC(POU):
    def __init__(self, name, returntype, section=''):
        POU.__init__(self, name, section)
        self.returntype = returntype

    def emit(self):
        '''returns the full *.exp type definition of this FUNC'''
        res = []
        p = res.append()
        self._FB_PRG_header(p)
        p("FUNCTION %s : %s" % (self.name, self.returntype))
        p("VAR_INPUT")
        res.extend(self.dumpVars(self.var_input))
        p("END_VAR")
        p("VAR")
        res.extend(self.dumpVars(self.var))
        p("END_VAR")
        p("(* @END_DECLARATION := '0' *)")
        if self.code:
            res.extend(self.code)
        else:
            p.append('')
        p("END_FUNCTION")
        return '\n'.join(res)


class PRG(POU):
    def emit(self):
        '''returns the full *.exp type definition of this PRG'''
        res = []
        p = res.append
        self._FB_PRG_header(p)
        p("PROGRAM %s" % self.name)
        if self.var:
            p("VAR")
            res.extend(self.dumpVars(self.var))
            p("END_VAR")
        if self.var_persist:
            p("VAR PERSISTENT")
            res.extend(self.dumpVars(self.var_persist))
            p("END_VAR")
        if self.var_retain:
            p("VAR RETAIN")
            res.extend(self.dumpVars(self.var_retain))
            p("END_VAR")
        p("(* @END_DECLARATION := '0' *)")
        res.extend(self.code)
        while res[-1] == '':
            res.pop()
        p("END_PROGRAM")
        return '\n'.join(res)


class ST(POU):
    def emit(self):
        '''returns the full *.exp type definition of this STRUCT'''
        res = []
        p = res.append
        self._ST_ENUM_header(p)
        p("TYPE %s :" % self.name)
        p("STRUCT")
        res.extend(self.dumpVars(self.var))
        p("END_STRUCT")
        p("END_TYPE")
        p("(* @END_DECLARATION := '0' *)")
        return '\n'.join(res)

    @property
    def size(self):
        return sum(v.size for v in self.var)


class ENUM(POU):
    def addEnum(self, name, value):
        self.var.append(VAR(name=name, initval=value))

    def dumpEnums(self, enums):
        res = []
        namelen = 0
        for enum in enums:
            namelen = max(namelen, len(enum.name))
        for enum in enums:
            res.append('%s := %r' % (enum.name.ljust(namelen), enum.value))
        return '    ' + (',\n    '.join(res)) + '\n'

    def emit(self):
        '''returns the full *.exp type definition of this STRUCT'''
        res = []
        p = res.append()
        self._ST_ENUM_header(p)
        p("TYPE %s :" % self.name)
        p("(")
        p(self.dumpEnums(self.var))
        p(");")
        p("END_TYPE")
        p("(* @END_DECLARATION := '0' *)")
        return '\n'.join(res)


class GLOBAL(POU):
    def _ST_ENUM_header(self, p):
        self.name = self.name.replace(' ', '_')
        p("(* @NESTEDCOMMENTS := 'Yes' *)")
        p("(* @GLOBAL_VARIABLE_LIST := '%s' *)" % self.name)
        p("(* @PATH := '' *)")
        p("(* @OBJECTFLAGS := '0, 8' *)")

    def emit(self):
        res = []
        p = res.append
        self._FB_PRG_header(p)
        if self.var:
            p("VAR_GLOBAL")
            res.extend(self.dumpVars(self.var))
            p("END_VAR")
        if self.var_persist:
            p("VAR_GLOBAL PERSISTENT")
            res.extend(self.dumpVars(self.var_persist))
            p("END_VAR")
        if self.var_retain:
            p("VAR_GLOBAL RETAIN")
            res.extend(self.dumpVars(self.var_retain))
            p("END_VAR")
        p("(* @OBJECT_END := '%s' *)" % self.name)
        p("(* @CONNECTIONS := %s" % self.name)
        p("FILENAME : ''")
        p("FILETIME : 0")
        p("EXPORT : 0")
        p("NUMOFCONNECTIONS : 0")
        p("*)")
        return '\n'.join(res)

# map a few colornames to (r,g,b) tuples
COLORS = dict(BLACK=(0, 0, 0), WHITE=(255, 255, 255), RED=(255, 0, 0),
              BLUE=(0, 0, 255), GREEN=(0, 255, 0), YELLOW=(255, 255, 0),
              DARKGREY=(85, 85, 85), LIGHTGREY=(170, 170, 170), GREY=(128, 128, 128))


def colorCode(color):
    color = COLORS.get(color, color) or (0, 0, 0)
    try:
        return color[0] + (color[1] << 8) + (color[2] << 16)
    except TypeError:
        print("Can not convert Color %s" % color)
        raise


class Widget(object):
    widget_id = None
    bgcolor = None
    activebgcolor = 'DARKGREY'
    bordercolor = 'BLACK'
    activebordercolor = 'BLACK'
    activate = None
    x = 0
    y = 0
    width = 60
    height = 20
    execute = ''
    var_display = ''
    var_invisible = ''
    var_active = ''
    var_input_toggle = ''
    var_input_tasten = ''
    var_zoom_to_visu = ''
    var_text_flags = ''
    var_text_color = ''
    var_text_fontsize = ''
    var_text_fontflags = ''
    var_text_fontname = ''
    var_color_back = ''
    var_color_back_active = ''
    var_color_frame = ''
    var_color_frame_active = ''
    var_color_back_attr = ''
    var_color_frame_attr = ''
    var_deactivate_input = ''
    var_tooltip_text = ''
    var_linewidth = ''
    text_edit_type = None  # 0=Text, 1=NumPad, 2=KeyPad, 3=Schrittweite, 4=Position
    text_edit_min = ''
    text_edit_max = ''
    text_edit_title = ''
    display_text = ''
    display_tooltip = ''
    rounded = False
    line_width = 0

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            if k not in dir(Widget):
                raise RuntimeError('blubb')
            # clean strings
            if isinstance(v, str):
                v = v.replace('\n', '$R$N').replace('\r', '').replace('/', '\\/').\
                    replace("'", "$'")
            self.__dict__[k] = v
        if self.x < 0 or self.y < 0:
            raise ValueError('Positional coordinates must be non-negative!')

    def emit(self):
        '''
        returns the *.exp code fragment for THIS widget
        May look like:

        _SIMPLE : 1
        _LINEWIDTH : 0
        _NOCOLORS : 1,1
        _POS : 380,150,521,211,450,180
        _COLORS : 74565,5649426,2311527,7886388,0
        _VARIABLES : 'Rel_left','Rel_top','Rel_Right','Rel_bottom','ABS_X_OFS',\
        'ABS_Y_OFS','ABS_Skalierung','ABS_WInkel'
        _THRESH : 'var_invisible','var_active','var_input_toggle (_input|=0x10000)',\
        'var_input_tasten(_input|=0x20000)'
        _DSP : 'var_display','display_text','display_tooltip'
        _ZOOM : 'var_zoom_to_visu (_input|=0x40000)'
        _INPUT : 1048576
        _TEXT : 5,4294967284,400,0,61,0,0,0
        _FONT : ''
        _EXEC : 'execute (_input|=0x80000)'
        _TEXTVARIABLES : 'var_text_flags','var_text_color','var_text_fontsize',\
        'var_text_fontflags','var_text_fontname'
        _COLORVARIABLES : 'var_color_back','var_color_back_active','var_color_frame',\
        'var_color_frame_active','var_color_back_attr','var_color_frame_attr'
        _ACCESSLEVELS : 2,2,2,2,2,2,2,2
        _OBJECT : 0,4
        _THRESH2 : 'var_deactivate_input','var_tooltip_text','var_linewidth',''
        _INPUTTYPE : 'text_edit_min (_input|=0x100000)',\
        'text_edit_max (_input|=0x100000)','text_edit_title'
        _HIDDENINPUT : 0
        _END_ELEM
        (* @TEXTSCALINGVARS := '_TEXTSCALINGVARS: $'var_umrechnung$',\
        $'var_umrechnungfaktor$'' *)
        (* @EXTENDEDSIMPLESHAPE := '_SIMPLE: 0' *)
        (* @INPUTTAPFALSE := '_INPUTTAPFALSE: 1' *)
        '''
        res = []
        p = res.append
        p("")
        # form: 0=eckig, 1=abgerundet, 2=ellipse, 3=Line
        p("_SIMPLE : %d" % (1 if self.rounded else 0))
        p("_LINEWIDTH : %d" % self.line_width)
        p("_NOCOLORS : %d,%d" % (0 if self.bordercolor is None else 1,
                                 0 if self.bgcolor is None else 1))
        p("_POS : %d,%d,%d,%d,%d,%d" % (self.x, self.y, self.x + self.width + 1,
                                        self.y + self.height + 1,
                                        self.x + self.width // 2,
                                        self.y + self.height // 2))
        p("_COLORS : %d,%d,%d,%d,0" % (colorCode(self.bgcolor),
                                       colorCode(self.bordercolor),
                                       colorCode(self.activebgcolor),
                                       colorCode(self.activebordercolor)))
        # used for dynamic movement of widget (not used here)
        p("_VARIABLES : '','','','','','','',''")
        p("_THRESH : %r,%r,%r,%r" % (self.var_invisible, self.var_active,
                                     self.var_input_toggle, self.var_input_tasten))
        p(("_DSP : %r,%r,%r" % (self.var_display, self.display_text,
                                self.display_tooltip)).replace('\\\\', '\\'))
        p("_ZOOM : %r" % self.var_zoom_to_visu)  # switch to named VISU
        value = (1 if self.var_input_toggle else 0) + \
                (2 if self.var_input_tasten else 0) + \
                (4 if self.var_zoom_to_visu else 0) + \
                (8 if self.execute else 0) + \
                (16 if self.text_edit_type is not None else 0)
        p("_INPUT : %d" % (value << 16))
        p("_TEXT : 5,4294967284,400,0,%d,0,0,0" % self.widget_id)
        p("_FONT : ''")
        p("_EXEC : '%s'" % (('INTERN ASSIGN ' + self.execute) if self.execute else ''))
        p("_TEXTVARIABLES : %r,%r,%r,%r,%r" % (self.var_text_flags,
                                               self.var_text_color,
                                               self.var_text_fontsize,
                                               self.var_text_fontflags,
                                               self.var_text_fontname))
        p("_COLORVARIABLES : %r,%r,%r,%r,%r,%r" % (self.var_color_back,
                                                   self.var_color_back_active,
                                                   self.var_color_frame,
                                                   self.var_color_frame_active,
                                                   self.var_color_back_attr,
                                                   self.var_color_frame_attr))
        p("_ACCESSLEVELS : 2,2,2,2,2,2,2,2")
        p("_OBJECT : 0,%d" % (self.text_edit_type or 0))
        p("_THRESH2 : %r,%r,%r,''" % (self.var_deactivate_input,
                                      self.var_tooltip_text, self.var_linewidth))
        p("_INPUTTYPE : %r,%r,%r" % (self.text_edit_min, self.text_edit_max,
                                     self.text_edit_title))
        p("_HIDDENINPUT : 0")
        p("_END_ELEM")
        p("(* @TEXTSCALINGVARS := '_TEXTSCALINGVARS: $'$',$'$'' *)")
        p("(* @EXTENDEDSIMPLESHAPE := '_SIMPLE: 0' *)")
        p("(* @INPUTTAPFALSE := '_INPUTTAPFALSE: 0' *)")  # 1 if tap_false
        return '\n'.join(res)


def splitName(name, maxlength=14):
    '''split a too long name'''
    if len(name) < maxlength:
        return name
    i = len(name) // 2
    while i < len(name):
        if name[i] == name[i].upper():
            return splitName(name[:i]) + '\n' + splitName(name[i:])
        i += 1
    return name


class VISU(POU):
    def __init__(self, name, section=''):
        POU.__init__(self, name, section)
        self.widgets = []

    def add(self, w):
        self.widgets.append(w)
        w.widget_id = len(self.widgets)  # 1 based!
        return w

    def emit(self):
        res = []
        p = res.append

        p("")
        if self.section:
            p("(* @PATH := '\\/%s' *)" % self.section.replace('/', r'\/'))
        else:
            p("(* @PATH := '' *)")
        p("")
        p("VISUALISATION %s _VISU_TYPES : 1,1" % self.name)
        p("_BG_BMP : ''")
        p("_TEXT : 55")
        p("_PAINTZOOM : 100")
        for w in self.widgets:
            p(w.emit())
        p("_KEYINFOLIST : 0")
        p("END_VISUALISATION")
        p("")
        return '\n'.join(res)

    def addStatusWidgets(self, dev, x0, y0):
        """Widgets for displaying and manipulating the state of a device"""
        if not dev.has_status:
            return
        ifname = dev.if_name
        self.add(Widget(x=x0, y=y0, display_text='Reset', bgcolor='LIGHTGREY',
                        activebgcolor='BLUE', var_active='SHR(%s.Status,12)=0' % ifname,
                        var_deactivate_input='SHR(%s.Status,12)<8' % ifname,  # allow for >=8
                        execute='%s.Status:=(0)' % ifname,
                       ))
        self.add(Widget(x=x0, y=y0 + 20, display_text='IDLE', bgcolor='LIGHTGREY',
                        activebgcolor='GREEN', var_active='SHR(%s.Status,12)=1' % ifname,
                       ))
        self.add(Widget(x=x0, y=y0 + 40, display_text='WARN', bgcolor='LIGHTGREY',
                        activebgcolor='YELLOW', var_active='SHR(%s.Status,12)=3' % ifname,
                       ))
        if dev.has_target:
            self.add(Widget(x=x0, y=y0 + 60, display_text='Start', bgcolor='LIGHTGREY',
                            activebgcolor='GREEN', var_active='SHR(%s.Status,12)=5' % ifname,
                            var_deactivate_input='SHR(%s.Status AND 16#D000,12)<>1' % ifname,  # allow in 1,3
                            execute='%s.Status:=(16#5000)' % ifname,
                           ))
            self.add(Widget(x=x0, y=y0 + 80, display_text='BUSY', bgcolor='LIGHTGREY',
                            activebgcolor='YELLOW', var_active='SHR(%s.Status,12)=6' % ifname,
                           ))
            self.add(Widget(x=x0, y=y0 + 100, display_text='Stop', bgcolor='LIGHTGREY',
                            activebgcolor='RED', var_active='SHR(%s.Status,12)=7' % ifname,
                            var_deactivate_input='SHR(%s.Status,12)<>6' % ifname,  # allow for 6
                            execute='%s.Status:=(16#7000)' % ifname,
                           ))
        else:
            self.add(Widget(x=x0, y=y0 + 60, display_text='Start', bgcolor='LIGHTGREY',
                            activebgcolor='GREEN', var_active='SHR(%s.Status,12)=5' % ifname,
                           ))
            self.add(Widget(x=x0, y=y0 + 80, display_text='BUSY', bgcolor='LIGHTGREY',
                            activebgcolor='YELLOW', var_active='SHR(%s.Status,12)=6' % ifname,
                           ))
            self.add(Widget(x=x0, y=y0 + 100, display_text='Stop', bgcolor='LIGHTGREY',
                            activebgcolor='RED', var_active='SHR(%s.Status,12)=7' % ifname,
                           ))
        self.add(Widget(x=x0, y=y0 + 120, display_text='ERROR', bgcolor='LIGHTGREY',
                        activebgcolor='RED', var_active='SHR(%s.Status,12)>7' % ifname,
                       ))

    def addValueWidgets(self, dev, x0, y0):
        ifname = dev.if_name
        self.add(Widget(x=x0, y=y0, width=80, height=30,
                        display_text='Current:\n%s', var_display='%s.Value' % ifname,
                        bgcolor='WHITE', bordercolor='BLACK',
                       ))
        if dev.has_target:
            self.add(Widget(x=x0, y=y0 + 40, width=80, height=30, text_edit_type=0,
                            display_text='Target:\n%s', var_display='%s.Target' % ifname,
                            bgcolor='WHITE', bordercolor='BLACK',
                           ))

    def addParamStateWidgets(self, dev, x0, y0):
        """Widgets for displaying and manipulating the state of the param-interface"""
        ifname = dev.if_name
        self.add(Widget(x=x0, y=y0, display_text='INIT', bgcolor='LIGHTGREY',
                        activebgcolor='BLUE', var_active='SHR(%s.ParamControl,13)=0' % ifname,
                       ))
        self.add(Widget(x=x0, y=y0 + 20, display_text='doRead', bgcolor='LIGHTGREY',
                        activebgcolor='GREEN', var_active='SHR(%s.ParamControl,13)=1' % ifname,
                        var_deactivate_input='SHR(%s.ParamControl,13)<4' % ifname,
                        execute='%s.ParamControl:=((%s.ParamControl AND 16#1FFF) OR 16#2000)' % (ifname, ifname),
                       ))
        self.add(Widget(x=x0, y=y0 + 40, display_text='doWrite', bgcolor='LIGHTGREY',
                        activebgcolor='GREEN', var_active='SHR(%s.ParamControl,13)=2' % ifname,
                        var_deactivate_input='SHR(%s.ParamControl,13)<4' % ifname,
                        execute='%s.ParamControl:=((%s.ParamControl AND 16#1FFF) OR 16#4000)' % (ifname, ifname),
                       ))
        self.add(Widget(x=x0, y=y0 + 60, display_text='BUSY', bgcolor='LIGHTGREY',
                        activebgcolor='YELLOW', var_active='SHR(%s.ParamControl,13)=3' % ifname,
                       ))
        self.add(Widget(x=x0, y=y0 + 80, display_text='DONE', bgcolor='LIGHTGREY',
                        activebgcolor='GREEN', var_active='SHR(%s.ParamControl,13)=4' % ifname,
                       ))
        self.add(Widget(x=x0, y=y0 + 100, display_text='NO IDX', bgcolor='LIGHTGREY',
                        activebgcolor='RED', var_active='SHR(%s.ParamControl,13)=5' % ifname,
                       ))
        self.add(Widget(x=x0, y=y0 + 120, display_text='RO', bgcolor='LIGHTGREY',
                        activebgcolor='RED', var_active='SHR(%s.ParamControl,13)=6' % ifname,
                       ))
        self.add(Widget(x=x0, y=y0 + 140, display_text='RETRY', bgcolor='LIGHTGREY',
                        activebgcolor='RED', var_active='SHR(%s.ParamControl,13)=7' % ifname,
                       ))

    def addParamIDXWidgets(self, dev, x0, y0):
        """Widgets for displaying and manipulating parameter values"""
        ifname = dev.if_name
        self.add(Widget(x=x0, y=y0, width=170,
                        bgcolor='WHITE', bordercolor='BLACK',
                        display_text='ParamValue: %s', var_display='%s.ParamValue' % ifname,
                        text_edit_type=0,
                        var_deactivate_input='SHR(%s.ParamControl,13)<4' % ifname,
                       ))
        # indicator + quick-read buttons
        cols = [[30, 31, 32, 33, 34, 35],  # limits
                [60, 61, 66, 67, 68, 69],  # pos+speed+accel
                [59, 62, 63, 64, 65],  # currents
                [1, 2, 3, 4, 5, ],  # exp. stuff
                [10, 20, 36, 37, ],  # -''-
                [51, 52, 53, 55, 56],  # -''-
                [130, 131, 132, 137, 142]]  # commands

        for xc, col in enumerate(cols):
            for yc, n in enumerate(col):
                name = PARAM_MAPPING[n]
                if name.startswith('do'):
                    name = name[2:]
                    rounded = True
                else:
                    rounded = False
                self.add(Widget(x=x0 + 48 * xc - 1, y=y0 + 20 * yc + 30, rounded=rounded, width=48, height=18,
                                display_text=name, bgcolor='LIGHTGREY' if n in dev.params + dev.cmds else 'DARKGREY',
                                activebgcolor='GREEN', var_active='(%s.ParamControl AND 16#1FFF)=%d' % (ifname, n),
                                var_deactivate_input='SHR(%s.ParamControl,13)<4' % ifname,
                                execute='%s.ParamControl:=(16#2000 OR %d)' % (ifname, n),
                               ))
    def addAdevWidgets(self, dev, x0, y0, width=100):
        ifname = dev.if_name
        # name
        name = splitName(dev.name)
        self.add(Widget(x=x0, y=y0, width=width, height=20 + 10 * max(1, name.count('\n')),
                        display_text=name, bgcolor='WHITE', bordercolor='BLACK',
                        var_zoom_to_visu='VISU_' + dev.name,
                        line_width=2,
                       ))
        # current value
        y0 += 30 + 10 * max(1, name.count('\n'))
        width = max(60, min(80, width))
        self.add(Widget(x=x0, y=y0, width=width, height=30,
                        display_text='Current:\n%s', var_display='%s.Value' % ifname,
                        bgcolor='WHITE', bordercolor='BLACK',
                       ))
        y0 += 40
        if dev.has_status:
            for text, color, cond in zip('RESET IDLE WARN START BUSY STOP ERROR'.split(),
                                         'BLUE GREEN YELLOW GREEN YELLOW GREEN RED'.split(),
                                         '<>0 <>1 <>3 <>5 <>6 <>7 <8'.split()):
                self.add(Widget(x=x0, y=y0, width=width,
                                display_text=text, bgcolor=color,
                                var_invisible='SHR(%s.Status,12)%s' % (ifname, cond),
                               ))
        y0 += 30
        if dev.has_target:
            self.add(Widget(x=x0, y=y0, width=width, height=30,
                            display_text='Target:\n%s', var_display='%s.Target' % ifname,
                            bgcolor='WHITE', bordercolor='BLACK',
                           ))
        y0 += 40
        if dev.has_flat_params:
            for i, n in enumerate(dev.paramnames):
                self.add(Widget(x=x0, y=y0, width=width, height=30,
                                display_text='%s:\n%%s' % n, var_display='%s.param%d' % (ifname, i + 1),
                                bgcolor='WHITE', bordercolor='BLACK',
                               ))
                y0 += 40
        elif dev.has_idx_params:
            y0 += 10
            self.add(Widget(x=x0, y=y0, width=width, height=20,
                            display_text="SUB: %d", bgcolor='WHITE', bordercolor='BLACK',
                            var_display='SHR(%s.ParamControl AND 16#1F00, 8)' % ifname,
                           ))
            y0 += 30

            # Map IDX like CMD?
            self.add(Widget(x=x0, y=y0, width=width, height=20,
                            display_text="IDX: %d", bgcolor='WHITE', bordercolor='BLACK',
                            var_display='(%s.ParamControl AND 16#FF)' % ifname,
                           ))
            for n in dev.paramnames:
                self.add(Widget(x=x0, y=y0, width=width, height=20,
                                display_text=n, bgcolor='LIGHTGREY', bordercolor='BLACK',
                                var_invisible="(%s.ParamControl AND 16#FF)<>%d" %
                                (ifname, PARAM_MAPPING[n.lower()]),
                               ))
            y0 += 30

            for text, color, cond in zip('INIT doRead doWrite BUSY DONE ERR_NO_IDX ERR_RO RETRY_LATER'.split(),
                                         'BLUE GREEN GREEN YELLOW GREEN RED RED RED'.split(),
                                         '<>0 <>1 <>2 <>3 <>4 <>5 <>6 <>7'.split()):
                self.add(Widget(x=x0, y=y0, width=width,
                                display_text=text, bgcolor=color,
                                var_invisible='SHR(%s.ParamControl,13)%s' % (ifname, cond),
                               ))
            y0 += 30

            self.add(Widget(x=x0, y=y0, width=width, height=30,
                            display_text="PVal.: %s", bgcolor='WHITE', bordercolor='BLACK',
                            var_display='%s.ParamValue' % ifname,
                           ))
            y0 += 40




class PLCPROJECT(object):
    def __init__(self):
        ALL_DEVICES.clear()
        ALL_POU.clear()
        self.FBs = []
        self.FUNCs = []
        self.PRGs = []
        self.STs = []
        self.ENUMs = []
        self.GLOBALs = []
        self.VISUs = []

    def add(self, what):
        if what is None:
            return
        elif isinstance(what, FB):
            self.FBs.append(what)
        elif isinstance(what, FUNC):
            self.FUNCs.append(what)
        elif isinstance(what, PRG):
            self.PRGs.append(what)
        elif isinstance(what, ST):
            self.STs.append(what)
        elif isinstance(what, ENUM):
            self.ENUMs.append(what)
        elif isinstance(what, VISU):
            self.VISUs.append(what)
        elif isinstance(what, GLOBAL):
            self.GLOBALs.append(what)
        else:
            raise ValueError("Don't know how to handle %r!" % what)
        return what

    def emit(self):
        '''emit the whole project by collecting the output of all known POU's emit()'''
        res = []

        def sorter(a, b):
            mya = a.name.replace('_', ' ')
            myb = b.name.replace('_', ' ')
            if mya < myb:
                return -1
            elif mya > myb:
                return 1
            return 0
        l = sorted(self.FBs + self.FUNCs + self.PRGs, cmp=sorter)
        for e in l:
            res.append(e.emit())

        l = sorted(self.STs + self.ENUMs, cmp=sorter)
        for e in l:
            res.append(e.emit())

        for v in self.VISUs:
            res.append(v.emit())

        for e in self.GLOBALs:
            res.append(e.emit())
        res.append(TRAILER)
        return '\n'.join(res)

    def save(self, filename=None, dirname='.'):
        '''save the whole project by collecting the output of all known POU's emit()

        if a filename is given, dump everything in one file there.
        Otherwise emit each object in it own file.
        '''
        if filename is not None:
            with open(path.join(dirname, filename), 'wb') as f:
                f.write(self.emit().replace('\n', '\r\n'))
            return

        for pou in self.FBs + self.FUNCs + self.PRGs + self.STs + self.ENUMs + self.VISUs + self.GLOBALs:
            filename = pou.name
            if not filename.startswith(pou.__class__.__name__) and pou not in self.PRGs:
                filename = pou.__class__.__name__ + '_' + pou.name

            if not filename.endswith('.exp'):
                filename += '.exp'
            print('saving', filename)
            with open(path.join(dirname, filename), 'wb') as f:
                f.write(pou.emit().replace('\n', '\r\n'))

    def build(self, plc, devices):
        '''generates a PLC template from the given Information and returns a big string:

        plc should be a dict containing:
        name, version, author1, author2 as strings
        and may have: indexer_size, indexer_offset, magic

        device should be iterable over deviceitems which are dicts containing:
        typcode, name
        optional values are unit, offset, size, version, author1, author2, paramlist
        '''
        # convert, clean, sanitize given args
        plc = PLC(plc)
        devices = [Device(dev) for dev in devices]

        # generate Info Block
        self.gen_INFO(plc, devices)
        # generate FB's
        self.gen_FB(plc, devices)
        self.gen_Impl(plc, devices)
        self.gen_INDEXER(plc, devices)
        self.gen_MAIN(plc, devices)
        # gen ST's
        self.gen_ST(plc, devices)
        self.gen_ST_Indexer(plc, devices)
        self.gen_ST_DeviceInfo(plc, devices)
        self.gen_Global(plc, devices)
        self.gen_Persist(plc, devices)
        # gen Visualisations
        self.gen_Visualisations(plc, devices)

    def gen_FB(self, plc, devices):  # pylint: disable=R0915, R0912
        for dev in devices:
            if '___' in dev.name + dev.typname:
                continue
            try:
                fb = FB(dev.FB_name, 'Interface')
            except ValueError:
                continue  # FB already defined
            fb.addVar_In_Out(name='interface', datatype=dev.ST_name)
            add = fb.addCode
            add(';')
            if dev.is_special:
                add('(* please implement something useful here or use a different interface! *)')
                self.add(fb)
                continue
            add("(* FB for handling Device '%s' which is an %s *)" % (dev.name, dev.typname))

            if dev.klemme:
                # XXX
                fb.addVar_Input(name=dev.hw_in_name[3:], datatype=dev.hw_in_name)
                fb.addVar_Output(name=dev.hw_out_name[3:], datatype=dev.hw_out_name)
                if dev.paramnames:
                    fb.addVar_In_Out(name=dev.persist_name[3:], datatype=dev.persist_name)
                add('(* generate %s in each Cycle! *)' % dev.hw_out_name[3:])
                add('')
                add('(* write to %s ONLY IF NEEDED! *)' % dev.persist_name[3:])
                for n in dev.paramnames:
                    add('%s.%s;' % (dev.persist_name[3:], n))

            elif dev.adevs:
                for adevname in dev.adevs:
                    adev = ALL_DEVICES[adevname]
                    fb.addVar_In_Out(name=adevname, datatype=adev.ST_name)
                add("(* Implement interaction with %s *)" % (','.join(dev.adevs)))
                add("")
                add("(* obtain current value *)")
            elif dev.num_values == 1:
                # generic approach
                fb.addVar_Input(name='current_value', datatype=dev.datatype, initval=','.join(['0'] * dev.num_values))
                add("(* obtain current value *)")
                add("interface.Value := current_value;")
            else: # vector stuff
                fb.addVar_Input(name='current_value', datatype=dev.datatype, initval=','.join(['0'] * dev.num_values))
                add("(* obtain current value *)")
                for _ in range(1, 1 + dev.num_values):
                    add("interface.Value[%d] := current_value[%s];" % (_,_))
            if dev.has_flat_params:
                add("(* XXX TODO: handle parameters *)")
                add("")
            if dev.has_idx_params:
                add("(* XXX Implement: handle parameters *)")
                add("")
                add("(* handle Parameter interface *)")
                add("CASE interface.ParamControl OF")
                add("    0 :     (* initial value *)")
                add("            interface.ParamControl := 16#8000; (* DONE, SUB=0, IDX=0 *)")
                add("")
                add("(* doRead *)")
                # iterate over all params for reading
                for p in dev.params:
                    pn = PARAM_MAPPING[p]
                    add("    16#%04x : (* read %s *)" % (0x2000 + p, pn))
                    if dev.klemme:
                        add("            interface.ParamValue := %s.%s;" % (dev.persist_name, pn))
                    else:
                        add("            interface.ParamValue := %s;" % pn.title())
                        fb.addVar(name=pn, datatype='REAL', initval='0.0')
                    add("            interface.ParamControl := 16#%04x; (* BUSY, SUB=0, IDX=%s *)" %
                        (0x6000 + p, pn))
                    add("(*            interface.ParamControl := 16#%04x; (* DONE, SUB=0, IDX=%s *)*)" %
                        (0x8000 + p, pn))
                    add("(*            interface.ParamControl := 16#%04x; (* ERR_RETRY, SUB=0, IDX=%s *)*)" %
                        (0xE000 + p, pn))
                    add("")
                    if dev.num_values > 1:
                        for channel in range(1, 1 + dev.num_values):
                            add("    16#%04x : (* read %s, channel %d *)" % (0x2000 + p + channel * 256, pn, channel))
                            add("            (* no code can be generated here, please adjust manually! *)")
                            add("            interface.ParamControl := 16#%04x; (* BUSY, SUB=%d, IDX=%s *)" %
                                (0x6000 + p + channel * 256, channel, pn))
                            add("(*            interface.ParamControl := 16#%04x; (* DONE, SUB=%d, IDX=%s *)*)" %
                                (0x8000 + p + channel * 256, channel, pn))
                            add("(*            interface.ParamControl := 16#%04x; (* ERR_RETRY, SUB=%d, IDX=%s *)*)" %
                                (0xE000 + p + channel * 256, channel, pn))
                            add("")

                add("")
                add("(* doWrite *)")
                # iterate over all params for writing
                for p in dev.params:
                    pn = PARAM_MAPPING[p]
                    add("    16#%04x : (* write %s *)" % (0x4000 + p, pn))
                    if dev.klemme:
                        add("            %s.%s := interface.ParamValue;" % (dev.persist_name, pn))
                    else:
                        add("            %s := interface.ParamValue;" % pn)
                    add("            interface.ParamControl := 16#%04x; (* doRead, SUB=0, IDX=%s *)" %
                        (0x2000 + p, pn))
                    add("(*            interface.ParamControl := 16#%04x; (* BUSY, SUB=0, IDX=%s *)*)" %
                        (0x6000 + p, pn))
                    add("(*            interface.ParamControl := 16#%04x; (* DONE, SUB=0, IDX=%s *)*)" %
                        (0x8000 + p, pn))
                    add("(*            interface.ParamControl := 16#%04x; (* ERR_RO, SUB=0, IDX=%s *)*)" %
                        (0xC000 + p, pn))
                    add("(*            interface.ParamControl := 16#%04x; (* ERR_RETRY, SUB=0, IDX=%s *)*)" %
                        (0xE000 + p, pn))
                    add("")
                    add("    16#%04x : (* BUSY %s *)" % (0x6000 + p, pn))
                    add("            interface.ParamControl := 16#%04x; (* DONE, SUB=0, IDX=%s *)" %
                        (0x8000 + p, pn))
                    add("")
                    if dev.num_values > 1:
                        for channel in range(1, 1 + dev.num_values):
                            add("    16#%04x : (* write %s, channel %d *)" % (0x4000 + p + channel * 256, pn, channel))
                            add("            (* no code can be generated here, please adjust manually! *)")
                            add("            interface.ParamControl := 16#%04x; (* doRead, SUB=%d, IDX=%s *)" %
                                (0x2000 + p + channel * 256, channel, pn))
                            add("(*            interface.ParamControl := 16#%04x; (* BUSY, SUB=%d, IDX=%s *)*)" %
                                (0x6000 + p + channel * 256, channel, pn))
                            add("(*            interface.ParamControl := 16#%04x; (* DONE, SUB=%d, IDX=%s *)*)" %
                                (0x8000 + p + channel * 256, channel, pn))
                            add("(*            interface.ParamControl := 16#%04x; (* ERR_RO, SUB=%d, IDX=%s *)*)" %
                                (0xC000 + p + channel * 256, channel, pn))
                            add("(*            interface.ParamControl := 16#%04x; (* ERR_RETRY, SUB=%d, IDX=%s *)*)" %
                                (0xE000 + p + channel * 256, channel, pn))
                            add("")
                            add("    16#%04x : (* BUSY %s, channel %d *)" % (0x6000 + p + channel * 256, pn, channel))
                            add("            interface.ParamControl := 16#%04x; (* DONE, SUB=%d, IDX=%s *)" %
                                (0x8000 + p + channel * 256, channel, pn))
                            add("")

                if dev.cmds:
                    add("")
                    add("(* CMD's *)")
                # iterate over all params for CMD's
                for p in dev.cmds:
                    pn = PARAM_MAPPING[p]
                    add("    16#%04x : (* CMD %s *)" % (0x4000 + p, pn))
                    add("            interface.ParamValue; (* XXX TODO! *)")
                    add("            interface.ParamControl := 16#%04x; (* BUSY, SUB=0, IDX=%s *)" %
                        (0x6000 + p, pn))
                    add("(*            interface.ParamControl := 16#%04x; (* ERR_RETRY_LATER, SUB=0, IDX=%s *)*)" %
                        (0xE000 + p, pn))
                    add("")
                    add("    16#%04x : (* doing CMD %s *)" % (0x6000 + p, pn))
                    add("            interface.ParamValue; (* XXX TODO! *)")
                    add("            interface.ParamControl := 16#%04x; (* doRead, SUB=0, IDX=%s *)" %
                        (0x2000 + p, pn))
                    add("")
                    add("    16#%04x : (* done CMD %s *)" % (0x2000 + p, pn))
                    add("            interface.ParamValue; (* XXX TODO! *)")
                    add("            interface.ParamControl := 16#%04x; (* DONE, SUB=0, IDX=%s *)" %
                        (0x8000 + p, pn))
                    add("")
                    if dev.num_values > 1:
                        for channel in range(1, 1 + dev.num_values):
                            _id = p + 256 * channel
                            add("    16#%04x : (* CMD %s, channel %d *)" % (0x4000 + _id, pn, channel))
                            add("            interface.ParamValue; (* XXX TODO! *)")
                            add("            interface.ParamControl := 16#%04x; (* BUSY, SUB=%d, IDX=%s *)" %
                                (0x6000 + _id, channel, pn))
                            add("(*            interface.ParamControl := 16#%04x; (* %s, SUB=%d, IDX=%s *)*)" %
                                (0xE000 + _id, "ERR_RETRY_LATER", channel, pn))
                            add("")
                            add("    16#%04x : (* doing CMD %s, channel %d *)" % (0x6000 + _id, pn, channel))
                            add("            interface.ParamValue; (* XXX TODO! *)")
                            add("            interface.ParamControl := 16#%04x; (* doRead, SUB=%d, IDX=%s *)" %
                                (0x2000 + _id, channel, pn))
                            add("")
                            add("    16#%04x : (* done CMD %s, channel %d *)" % (0x2000 + _id, pn, channel))
                            add("            interface.ParamValue; (* XXX TODO! *)")
                            add("            interface.ParamControl := 16#%04x; (* DONE, SUB=%d, IDX=%s *)" %
                                (0x8000 + _id, channel, pn))
                            add("")

                add("")
                add("ELSE")
                add("    (* ERR_NO_IDX *)")
                add("    IF NOT interface.ParamControl.15 THEN")
                add("        interface.ParamControl := (interface.ParamControl AND 16#1FFF) OR 16#A000;")
                add("    END_IF")
                add("END_CASE")
                add("")
            if dev.has_target and dev.num_values == 1:
                fb.addVar_Input(name='target_reached', datatype='BOOL', initval='TRUE')
                fb.addVar_Input(name='target_minvalue', datatype=dev.datatype, initval=dev.minval)
                fb.addVar_Input(name='target_maxvalue', datatype=dev.datatype, initval=dev.maxval)
                fb.addVar_Output(name='target_value', datatype=dev.datatype, initval='0')
                fb.addVar_Output(name='move_to_target', datatype='BOOL', initval='FALSE')
                add("(* limit target *)")
                if dev.num_values > 1:
                    for _ in range(1, 1 + dev.num_values):
                        add("interface.Target[%d] := "
                            "LIMIT(target_minvalue, interface.Target[%d], target_maxvalue);" %(_,_))
                else:
                    add("interface.Target := LIMIT(target_minvalue, interface.Target, target_maxvalue);")
                add("")

            if dev.has_status:
                add("(* state machine *)")
                add("CASE SHR(interface.Status, 12) OF")
                add("    0 : (* RESET *)")
                add("        interface.Status := 16#1000;")
                add("    1 : (* IDLE *)")
                add("        interface.Status := 16#1000;")
                add("    3 : (* WARN *)")
                add("        interface.Status := 16#1000;")
                if dev.has_target and dev.num_values == 1:
                    add("    5 : (* START *)")
                    add("        target_value := interface.Target;")
                    add("        move_to_target := TRUE;")
                    add("        interface.Status := 16#6000;")
                    add("    6 : (* BUSY *)")
                    add("        IF target_reached THEN")
                    add("            move_to_target := FALSE;")
                    add("            interface.Status := 16#1000;")
                    add("        END_IF")
                    add("    7 : (* STOP *)")
                    add("        move_to_target := FALSE;")
                    add("        interface.Status := 16#1000;")
                else:
                    add("    5 : (* START *)")
                    add("        interface.Status := 16#1000;")
                    add("    6 : (* BUSY *)")
                    add("        interface.Status := 16#1000;")
                    add("    7 : (* STOP *)")
                    add("        interface.Status := 16#1000;")
                add("ELSE")
                add("    (* errors stick! *)")
                add("    interface.Status := 16#8000;")
                add("END_CASE")
            self.add(fb)

    def gen_Persist(self, plc, devices):
        g = GLOBAL('persistent_variables')
        if plc.coupler == 'BC9100':
            addr = 6  # first free addr
            for dev in devices:
                if dev.klemme and dev.paramnames:
                    g.addVar(name='%s_%s' % (dev.name, dev.persist_name[3:]),
                             datatype=dev.persist_name, addr='%%M%d' % addr)
                    addr += (ALL_POU[dev.persist_name].size + plc.alignment - 1) & -plc.alignment
        else:
            for dev in devices:
                if dev.klemme and dev.paramnames:
                    g.addVar_Persist(name='%s_%s' % (dev.name, dev.persist_name[3:]),
                                     datatype=dev.persist_name)
        self.add(g)

    def gen_Impl(self, plc, devices):
        p = PRG('Implementation', section='Interface')
        for dev in devices:
            if '___' not in dev.name + dev.typname:
                p.addVar(name=dev.fb_name, datatype=dev.FB_name)
        p.addSep()
        for dev in devices:
            if dev.klemme:
                p.addVar(name='%s_%s' % (dev.name, dev.hw_in_name[3:]),
                         datatype=dev.hw_in_name, addr='%I*')
                p.addVar(name='%s_%s' % (dev.name, dev.hw_out_name[3:]),
                         datatype=dev.hw_out_name, addr='%Q*')
                # persistent vars are stored elsewhere (gen_Persist)
        # code to make persistent vars persistent for CX8090
        if plc.coupler == 'CX8090':
            p.addSep()
            p.addVar(name='fbUPS', datatype='FB_S_UPS_CX80xx', comment='UPS-FB instance')
            p.addCode('(* handle OS-call to make persistent vars persistent. '
                      'you may use the output of bPowerFailDetect *)')
            p.addCode('fbUPS(eUpsMode := eSUPS_WrPersistData_Shutdown,\tbPowerFailDetect => );')
            p.addCode('')
        for dev in devices:
            # add Code to call the FB's defined just now
            if dev.is_special:
                continue
            p.addCode('%s(interface:=%s' % (dev.fb_name, dev.if_name))
            spacer = ' ' * len(dev.fb_name)
            if dev.klemme:
                p.addCode('%s,%s:=%s' % (spacer, dev.hw_in_name[3:], '%s_%s' %
                                         (dev.name, dev.hw_in_name[3:])))
                if dev.paramnames:
                    p.addCode('%s,%s:=%s' % (spacer, dev.persist_name[3:], '%s_%s' %
                                             (dev.name, dev.persist_name[3:])))
                p.addCode('%s,%s=>%s' % (spacer, dev.hw_out_name[3:], '%s_%s' %
                                         (dev.name, dev.hw_out_name[3:])))
            elif dev.adevs:
                for adevname in dev.adevs:
                    adev = ALL_DEVICES[adevname]
                    p.addCode('%s,%s:=%s' % (spacer, adevname, adev.if_name))
            p.addCode(spacer + ');')
            p.addCode('')
        self.add(p)

    def gen_ST(self, plc, devices):
        for dev in devices:
            if dev.klemme:
                # create ST's for the Klemme I/O/persist
                try:
                    st = ST(dev.hw_in_name)
                    st.addVar(name='State', datatype='BYTE')
                    st.addVar(name='_unused_', datatype='BYTE')
                    st.addVar(name='Value', datatype='WORD')
                    self.add(st)
                except Exception:
                    pass
                try:
                    st = ST(dev.hw_out_name)
                    st.addVar(name='Control', datatype='BYTE')
                    st.addVar(name='_unused_', datatype='BYTE')
                    st.addVar(name='Value', datatype='WORD')
                    self.add(st)
                except Exception:
                    pass
                if dev.paramnames:
                    try:
                        st = ST(dev.persist_name)
                        for n in dev.paramnames:
                            st.addVar(name=n, datatype='REAL')
                        self.add(st)
                    except Exception:
                        pass
            try:
                st = ST(dev.ST_name)
            except ValueError:
                continue  # ST already defined
            if dev.is_special:
                if dev.typname == 'KeyValue':
                    st.addVar(name='Value', datatype='WORD', initval='0')
                elif dev.typname == 'RealValue':
                    st.addVar(name='Value', datatype='REAL', initval='0')
                elif dev.typname == 'Statuswort':
                    st.addVar(name='Status', datatype='WORD', initval='0')
                elif 'DEBUG' in dev.typname:
                    continue
                else:
                    raise ValueError('Unknown ST-type %s for device %r' % (dev.ST_name, dev.name))
                return st

            st.addVar(name='Value', datatype=dev.datatype)
            if dev.has_target:
                st.addVar(name='Target', datatype=dev.datatype)
            if dev.has_status:
                st.addVar(name='Status', datatype='WORD')
            if dev.has_flat_params:
                st.addVar(name='AUX', datatype='WORD')
                # st.addVar(name='arrParams', datatype='ARRAY [1..%d] OF REAL' % dev.has_flat_params)
                # OR
                for i in range(1, dev.has_flat_params + 1):
                    st.addVar(name='Param%d' % i, datatype='REAL', comment=dev.paramnames[i - 1])
            elif dev.has_idx_params:
                st.addVar(name='ParamControl', datatype='WORD', comment='Parameter index + cmd')
                st.addVar(name='ParamValue', datatype='REAL', comment='Parameter Value')
            self.add(st)

    def gen_Global(self, plc, devices):
        g = GLOBAL('global_variables')
        g.addVar(name='fMagic', addr='%MB0', datatype='REAL', initval=plc.magic,
                 comment='persistent stuff up to %MB64')
        g.addVar(name='iOffset', addr='%MB4', datatype='WORD', initval=plc.indexer_offset)
        g.addSep()
        g.addVar(name='stIndexer', addr='%%MB%d' % plc.indexer_offset, datatype='ST_Indexer')
        g.addSep()
        offset = plc.indexer_offset
        size = plc.indexer_size
        for dev in devices:
            offset = dev.offset or offset + size
            # 32bit alignment for CX8090 (may over-align for discrete types!)
            if plc.coupler == 'CX8090':
                offset = (offset + 3) & -4
                dev.offset = offset
            if '___' not in dev.name + dev.typname:
                g.addVar(name=dev.if_name, addr='%%MB%d' % offset, datatype=dev.ST_name)
            size = dev.size or dev.typsize
        g.addSep("    (* next free is at %%MB%d *)" % (offset + size))
        g.addSep("")
        l = []
        for dev in devices:
            # remove obvious data
            if dev.offset == offset + size:
                dev.offset = 0
            offset = dev.offset or offset + size
            if plc.coupler == 'CX8090':
                offset = (offset + 3) & -4
            size = dev.size or dev.typsize
            e = []
            e.append("(TypCode:=16#%04x" % dev.typcode)
            e.append("Name:='%s'" % dev.name)
            for v in 'offset size'.split():
                if dev[v]:
                    e.append('%s:=%d' % (v.title(), dev[v]))
            if dev.unitcode:
                e.append('Unit:=16#%04x (* %r *)' % (dev.unitcode, dev.unit))
            for v in 'version author1 author2'.split():
                if dev[v.lower()]:
                    e.append("%s:=%r" % (v.title(), dev[v.lower()]))
            if (dev.params or dev.cmds) and dev.initparams:
                e.append("Params:=%s" % (', '.join('16#%04x' % i for i in dev.initparams)))
            for i, auxname in enumerate(dev.aux):
                if i== 0:
                    e.append("AUX:='%s'" % auxname)
                else:
                    e.append("'%s'" % auxname)
            if '___' not in dev.name + dev.typname:
                l.append(', '.join(e) + ')')
        g.addVar(name='Devices', datatype='ARRAY [1..%d] OF ST_DeviceInfo' % len(devices),
                 initval=('\n        ' + ',\n        '.join(l)))
        g.addSep()
        idx_datasize = plc.indexer_size - 2
        g.addVar(name='sPLCName', datatype='STRING[%d]' % idx_datasize, initval=plc.name)
        g.addVar(name='sPLCVersion', datatype='STRING[%d]' % idx_datasize, initval=plc.version)
        g.addVar(name='sPLCAuthor1', datatype='STRING[%d]' % idx_datasize, initval=plc.author1)
        g.addVar(name='sPLCAuthor2', datatype='STRING[%d]' % idx_datasize, initval=plc.author2)
        g.addSep()
        g.addVar(name='iCycle', datatype='WORD', initval='0')
        g.addVar(name='is_initialized', datatype='BOOL', initval='FALSE')
        self.add(g)

    def gen_INFO(self, plc, devices):
        p = PRG('_Info_', section='_Info_')
        p.addCode("(*")
        p.addCode(" * %s" % plc.name)
        p.addCode("")
        p.addCode(" * HW:")
        p.addCode("    - %s    PLC" % plc.coupler)
        for dev in devices:
            if dev.klemme:
                p.addCode('    - %s    %s' % (dev.klemme, dev.name))
        p.addCode("    - KL9010    Busendklemme")
        p.addCode("")
        p.addCode(" * Description:")
        p.addCode("    - Device exported as <Interface>")
        p.addCode("")
        p.addCode(" * exported devices:")
        for dev in devices:
            if '___' not in dev.name + dev.typname:
                p.addCode("   - %r (%s) in %s" % (dev.name, dev.typname, dev.FB_name))
        p.addCode(" *);")
        self.add(p)

    def gen_INDEXER(self, plc, devices):
        p = PRG(name='Indexer', section='Interface')
        p.addVar(name='nDevices', datatype='WORD')
        p.addVar(name='devnum', datatype='WORD')
        p.addVar(name='infotype', datatype='WORD')
        p.addSep()
        p.addVar(name='is_initialized', datatype='BOOL', initval='FALSE')
        p.addVar(name='itemp', datatype='BYTE')
        p.addVar(name='tempofs', datatype='WORD')

        auxmax = 0
        for dev in devices:
            auxmax = max(auxmax, len(dev.aux))

        blob = """
(* Indexer: tell host software which devices we have and of what type *)
(* AUTOGENERATED: DO NOT MODIFY ! *)

(* fill in optional details: ofs, size *)
IF is_initialized = FALSE THEN
    tempofs := iOffset + SIZEOF(stIndexer);
    nDevices := SIZEOF(Devices)/SIZEOF(Devices[1]);
    itemp := 1;
    WHILE itemp <= nDevices DO
        IF Devices[itemp].Size < SHL(Devices[itemp].TypCode AND 16#FF, 1) THEN
            Devices[itemp].Size := SHL(Devices[itemp].TypCode AND 16#FF, 1);
        END_IF
        IF Devices[itemp].Offset = 0 THEN
            Devices[itemp].Offset := tempofs;
        ELSE
            tempofs := Devices[itemp].Offset;
        END_IF
        tempofs := tempofs + Devices[itemp].Size;
        itemp := itemp + 1;
    END_WHILE
    is_initialized := TRUE;
END_IF

IF fMAGIC <> %(magic)s THEN
    fMAGIC := %(magic)s;
END_IF
IF iOffset <> %(ofs)d THEN
    iOffset := %(ofs)d;
END_IF

(* Handle Indexer requests *)
devnum := stIndexer.Request AND 16#FF;
infotype := SHR(stIndexer.Request, 8) AND 16#7F;

(* CLEAR Data-area *)
MEMSET(ADR(stIndexer.Data), 0, SIZEOF(stIndexer.Data));

(* fill in data *)
IF devnum = 0 THEN
    (* PLC specific *)
    CASE infotype OF
        (* Indexer Size *)
        1 : stIndexer.Data[1] := SIZEOF(stIndexer);
        (* Firmware Version *)
        4 : MEMCPY(ADR(stIndexer.Data), ADR(sPLCName),    MIN(SIZEOF(sPLCName),    SIZEOF(stIndexer.Data)));
        5 : MEMCPY(ADR(stIndexer.Data), ADR(sPLCVersion), MIN(SIZEOF(sPLCVersion), SIZEOF(stIndexer.Data)));
        6 : MEMCPY(ADR(stIndexer.Data), ADR(sPLCAuthor1), MIN(SIZEOF(sPLCAuthor1), SIZEOF(stIndexer.Data)));
        7 : MEMCPY(ADR(stIndexer.Data), ADR(sPLCAuthor2), MIN(SIZEOF(sPLCAuthor2), SIZEOF(stIndexer.Data)));
    END_CASE
ELSIF devnum <= nDevices THEN
    CASE(infotype) OF
        (* typcode *)
        0 : stIndexer.Data[1] := Devices[devnum].TypCode;
        (* DeviceSize *)
        1 : stIndexer.Data[1] := Devices[devnum].Size;
        (* DeviceAddr *)
        2 : stIndexer.Data[1] := Devices[devnum].Offset;
        (* unit *)
        3 : stIndexer.Data[1] := Devices[devnum].Unit;
        (* NAME *)
        4 : MEMCPY(ADR(stIndexer.Data), ADR(Devices[devnum].Name),
                   MIN(SIZEOF(Devices[devnum].Name), SIZEOF(stIndexer.Data)));
        (* Version *)
        5 : IF LEN(Devices[devnum].Version) > 0 THEN
                MEMCPY(ADR(stIndexer.Data), ADR(Devices[devnum].Version),
                       MIN(SIZEOF(Devices[devnum].Version), SIZEOF(stIndexer.Data)));
            ELSE
                (* fall back to PLC info if nothing specific is given *)
                MEMCPY(ADR(stIndexer.Data), ADR(sPLCVersion),
                       MIN(SIZEOF(sPLCVersion), SIZEOF(stIndexer.Data)));
            END_IF
        (* Author 1 *)
        6 : IF LEN(Devices[devnum].Author1) > 0 THEN
                MEMCPY(ADR(stIndexer.Data), ADR(Devices[devnum].Author1),
                       MIN(SIZEOF(Devices[devnum].Author1), SIZEOF(stIndexer.Data)));
            ELSE
                (* fall back to PLC info if nothing specific is given *)
                MEMCPY(ADR(stIndexer.Data), ADR(sPLCAuthor1),
                       MIN(SIZEOF(sPLCAuthor1), SIZEOF(stIndexer.Data)));
            END_IF
        (* Author 2 *)
        7 : IF LEN(Devices[devnum].Author2) > 0 THEN
                MEMCPY(ADR(stIndexer.Data), ADR(Devices[devnum].Author2),
                       MIN(SIZEOF(Devices[devnum].Author2), SIZEOF(stIndexer.Data)));
            ELSE
                (* fall back to PLC info if nothing specific is given *)
                MEMCPY(ADR(stIndexer.Data), ADR(sPLCAuthor2),
                       MIN(SIZEOF(sPLCAuthor2), SIZEOF(stIndexer.Data)));
            END_IF
        (* Supported parameters *)
        15 : MEMCPY(ADR(stIndexer.Data), ADR(Devices[devnum].Params),
                    MIN(SIZEOF(Devices[devnum].Params), SIZEOF(stIndexer.Data)));
%(auxcases)s
    END_CASE
END_IF

IF infotype = 127 THEN
    stIndexer.Data[1] := iCycle;
END_IF

stIndexer.Request.15 := 1;

iCycle := iCycle + 1;
"""
        auxcases = ['        16#%s : MEMCPY(ADR(stIndexer.Data), ADR(Devices[devnum].AUX[%d]),\n' \
                    '                       MIN(SIZEOF(Devices[devnum].AUX[%d]), SIZEOF(stIndexer.Data)));' \
                    % (hex(i+16)[-2:], i, i) for i in range(auxmax)]
        blobtext = blob % dict(auxcases = '\n'.join(auxcases), magic=plc.magic, ofs=plc.indexer_offset)
        for l in blobtext.split('\n'):
            p.addCode(l)

        self.add(p)

    def gen_MAIN(self, plc, devices):
        p = PRG(name='MAIN')
        p.addCode('')
        p.addCode('Indexer;')
        p.addCode('')
        p.addCode('Implementation;')
        self.add(p)

    def gen_ST_DeviceInfo(self, plc, devices):
        # size of strings the indexer supports
        strdef = 'STRING[%d]' % (plc.indexer_size - 2)

        # number of used aux definitions
        auxmax = 0
        for dev in devices:
            auxmax = max(auxmax, len(dev.aux))

        st = ST(name='ST_DeviceInfo')
        st.addVar(name='TypCode', datatype='WORD', initval='0', comment='Typcode -> Tabelle')
        st.addVar(name='Size', datatype='WORD', initval='0', comment='DeviceSize in Bytes')
        st.addVar(name='Offset', datatype='WORD', initval='0', comment='Offset in Merkerbereich')
        st.addVar(name='Unit', datatype='WORD', initval='0', comment='Unitcode -> Tabelle')
        st.addVar(name='Params', datatype='ARRAY[1..16] OF WORD',
                  initval='0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0', comment='param IDX or Bitmap')
        st.addVar(name='Name', datatype=strdef, comment='proposed Name of Device')
        st.addVar(name='Version', datatype=strdef, comment='Version of Implementation of Device')
        st.addVar(name='Author1', datatype=strdef, comment='Author of Impls. of Device')
        st.addVar(name='Author2', datatype=strdef)
        if auxmax:
            st.addVar(name='AUX', datatype='ARRAY [0..%d] OF %s' % (auxmax-1, strdef))
        self.add(st)

    def gen_ST_Indexer(self, plc, devices):
        st = ST(name="ST_Indexer")
        st.addVar(name='Request', datatype='WORD')
        st.addVar(name='Data', datatype='ARRAY [1..%d] OF WORD' % ((plc.indexer_size - 2) // 2))
        self.add(st)

    def gen_Visualisations(self, plc, devices):
        v = self.add(VISU('VISU_' + plc.name))
        x = 10
        realdevices = []
        debuglevels = 0
        for dev in devices:
            if dev.name.startswith('___'):
                debuglevels += 1
                continue
            realdevices.append(dev)

        border = 5
        width = 80
        while border * (1 + debuglevels) + len(realdevices) * (border + width) < 640:
            border *= 2
            width += 10
        while border * (1 + debuglevels) + len(realdevices) * (border + width) >= 640:
            width -= 1
            border -= 1
        x = border
        border = 10
        x = 10
        width = 60
        for dev in devices:
            if dev.name.startswith('___'):
                x += 2 * border
                continue
            v.addAdevWidgets(dev, x, 30, width=width)
            x += width + border

        for dev in realdevices:
            x0, y0 = 20, 50
            v = self.add(VISU('VISU_' + dev.name))
            # link back to PLC
            v.add(Widget(x=10, y=10, width=10 * len(plc.name),
                         bgcolor='LIGHTGREY', bordercolor='BLACK', line_width=2,
                         display_text=plc.name,
                         var_zoom_to_visu='VISU_' + plc.name,
                        ))
            # device name label
            v.add(Widget(x=x0 + 80, y=y0 + 10, width=200, height=30,
                         bgcolor='WHITE', bordercolor='BLACK',
                         display_text=dev.name, line_width=3,
                        ))
            v.addStatusWidgets(dev, x0, y0 + 10)
            v.addValueWidgets(dev, x0 + 80, y0 + 60)
            if dev.params:
                v.addParamStateWidgets(dev, x0 + 340, 480 - 10 - 8 * 20)
                v.addParamIDXWidgets(dev, x0, 480 - 10 - 7 * 20 - 30)
            if dev.adevs:
                for i, a in enumerate(dev.adevs):
                    v.addAdevWidgets(ALL_DEVICES[a], 420 + i * 110, 20)


TRAILER = '''

LIBRARY
PlcHelperBC.lb6 7.5.01 12:03:00
(* @LIBRARYSYMFILEINFO := '0' *)
NumOfPOUs: 3
MEMCMP: 0
MEMCPY: 0
MEMSET: 0
NumOfGVLs: 1
Global_Variables: 0
END_LIBRARY

LIBRARY
STANDARD.LB6 5.6.98 12:03:02
(* @LIBRARYSYMFILEINFO := '0' *)
NumOfPOUs: 20
CONCAT: 0
CTD: 0
CTU: 0
CTUD: 0
DELETE: 0
F_TRIG: 0
FIND: 0
INSERT: 0
LEFT: 0
LEN: 0
MID: 0
R_TRIG: 0
REPLACE: 0
RIGHT: 0
RS: 0
SEMA: 0
SR: 0
TOF: 0
TON: 0
TP: 0
NumOfGVLs: 1
'Global Variables 0': 0
END_LIBRARY
'''


def main():
    #~ plc = dict(name='MIRA_Magnet', version='V0.1',
               #~ author1='Dr. Enrico Faulhaber, MLZ (FRMII)',
               #~ author2='Lichtenbergstr. 1, 84748 Garching',
               #~ indexer_size=36, indexer_offset=64, magic=2015.02, coupler='BC9100')

    #~ devices = [
        #~ dict(name='T1', unit='C', typcode=0x2108, params=[32, 33], klemme='KL3208'),
        #~ dict(name='T2', unit=9, typcode=0x2108, params=[32, 33], klemme='KL3208'),
        #~ dict(name='T3', unit=9, typcode=0x2108, params=[32, 33], klemme='KL3208'),
        #~ dict(name='T4', unit=9, typcode=0x2108, params=[32, 33], klemme='KL3208'),
        #~ dict(name='U_sense', unit=1, typcode=0x1b03, klemme='KL3162'),
        #~ dict(name='I_sense', unit=2, typcode=0x1b03, klemme='KL3162'),
        #~ dict(name='I', unit=2, typcode=0x3008, params=[60], adevs=['Unipolar', 'polarity']),
        #~ dict(name='Unipolar', unit=2, typcode=0x3008, params=[60], aux=['Switching failed']),
        #~ dict(name='polarity', typcode=0x1e03),
    #~ ]


    # ~ _plc = dict(name='Hochdruckstand', version='V0.1',
                # ~ author1='Dr. Herbert Weiss, MLZ (FRMII)',
                # ~ author2='Lichtenbergstr. 1, 84748 Garching',
                # ~ indexer_size=36, indexer_offset=64, magic=2015.02, coupler='Cxxxx')

    # ~ _devices = [
        # ~ dict(name='p', unit='bar', typcode=0x3008, params=[60], klemme='Doli_p'),
        # ~ dict(name='piston', unit='mm', typcode=0x1B03, klemme='Doli_piston'),
        # ~ dict(name='hydraulic', unit='bar', typcode=0x1b03, klemme='Doli_hydraulic'),
    # ~ ]


    # ~ _plc = dict(name='TOFTOF Radialcollimator', version='V0.1',
                # ~ author1='Dr. Enrico Faulhaber, MLZ (FRMII)',
                # ~ author2='Lichtenbergstr. 1, 84748 Garching',
                # ~ indexer_size=36, indexer_offset=64, magic=2015.02, coupler='BC9100')

    # ~ _devices = [
        # ~ dict(name='RC_OnOff',typcode=0x1e03),
        # ~ dict(name='RC_Motor', unit=12, typcode=0x5008, klemme='KL2541',
        # ~      params=[30,31,32,33,34,35,59,60,61,62,63,64,65,66,67,68,69,130,131,132,137,142]),
    # ~ ]


    # ~ _plc = dict(name='Antares Magnet (GARFIELD)', version='V0.1',
                # ~ author1='somebody else, MLZ (FRMII)',
                # ~ author2='Lichtenbergstr. 1, 84748 Garching',
                # ~ indexer_size=36, indexer_offset=64, magic=2015.02, coupler='BX8xxx')

    # ~ _devices = [
        # ~ dict(name='ParSer', typcode=0x1e03, klemme='IO1'),
        # ~ dict(name='Polarity', typcode=0x1e03, klemme='IO2'),
        # ~ dict(name='OnOff', typcode=0x1e03, klemme='IO3'),
        # ~ dict(name='Temperature', typcode=0x1302, unit=9, klemme='IO4'),
    # ~ ]

    # klemme: reference implementation
    plc = dict(name='klemme', version='2016.09',
               author1='Enrico Faulhaber, MLZ (FRMII)',
               author2='Lichtenbergstr. 1, 85748 Garching',
               indexer_size=36, indexer_offset=64, magic=2015.02, coupler='BC9100',
              )
    devices = [
        dict(name='VectorIn2', typcode=0x4108, params=[30,31]),
        dict(name='VectorOut2', typcode=0x510c, params=[30,31]),
        ]

    prj = PLCPROJECT()
    prj.build(plc, devices)
    prj.save(dirname='.')
    prj.save(filename='Enno.exp')


if __name__ == '__main__':
    main()
