# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2015 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

import re
from time import sleep

from pluto.qt import QObject, pyqtSignal


class CommunicationDevice(QObject):
    MAXCONNECTIONATTEMPTS = 3
    reconnecting = pyqtSignal(bool)
    status = pyqtSignal(str)

    IP_FQDN = r'((?:[A-Z\d]+.)*[A-Z\d]+)'

    TEMPLATE = ''
    HELP = ''
    RE_TEMPLATE = re.compile('')

    def __init__(self, address, offset, parent=None):
        super(CommunicationDevice, self).__init__(parent)
        self._offset = offset
        self.error = ''
        self.terminateconnection = False
        self._addressMatch = self.validateAddress(address)
        if self._addressMatch:
            self._address = address
        else:
            self._address = ''

    def findOffset(self):
        self.error = 'Find Offset not implemented for connection type!'

    def readRegisters(self, start, amount):
        pass

    def writeRegisters(self, start, values):
        pass

    def initialConnect(self):
        pass

    def _sleep(self, seconds):
        while seconds:
            if self.terminateconnection:
                return False
            sleep(0.5)
            seconds -= 0.5

        return True

    def validateAddress(self, address):
        # return address
        adr = self.RE_TEMPLATE.match(address)
        if adr:
            return adr

        self.error = ('Invalid address, must be %s' % self.TEMPLATE)

    def getStats(self):
        return {
            'offset': self._offset,
            'address': self._address
        }
