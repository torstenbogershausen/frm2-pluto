# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2015 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

import re
import socket
import logging
import threading
from struct import Struct, pack, unpack, unpack_from, calcsize

from pluto.qt import QTimer

from pluto.model.communication import CommunicationDevice


# # we can omit the first 4 NetID bytes if it's the same as the IP address,
# # and the last two if they are 1.1 - but we always require the port
# addr_re = re.compile(r'ads://(.*?)/(\d+.\d+(.\d+.\d+(.\d+.\d+)?)?)?:(\d+)$')

# ADS port.
ADS_PORT = 0xBF02

# AMS header including AMS/TCP header.
REQ_HEADER = Struct('<xxIQQHHIII')
# AMS reply header including error field.
REP_HEADER = Struct('<xxIQQHHIIII')
# Reply to device info command.
DEVINFO = '<BBH16s'

REQ_HEADER_SIZE = REQ_HEADER.size
REP_HEADER_SIZE = REP_HEADER.size
AMS_HEADER_SIZE = REQ_HEADER_SIZE - 6

# ADS commands.
ADS_DEVINFO = 1
ADS_READ    = 2
ADS_WRITE   = 3

# Formats for payload.
ADS_ADR_LEN = Struct('<III')
ADS_WRITE_ONE = Struct('<IIIH')
ADS_LEN = Struct('<I')

# Index group for %M memory area.
INDEXGROUP_M = 0x4020


class CommunicationFailure(Exception):
    pass


class AdsCommunicationDevice(CommunicationDevice):

    TEMPLATE = 'ads://host/amsnetid:amsport'
    HELP = 'host: host ip or fqdn\n' \
           'amsnetid: x.x.x.x.x.x (defaults to [host].1.1)\n' \
           'amsport: mandatory'
    RE_TEMPLATE = re.compile(r'ads://%s/(\d+.\d+(.\d+.\d+(.\d+.\d+)?)?)?:(\d+)$'
                             % CommunicationDevice.IP_FQDN, re.IGNORECASE)

    def __init__(self, address, parent=None):
        super(AdsCommunicationDevice, self).__init__(address, 0, parent)
        self.disconnected = False

        if not self._addressMatch:
            self._iphostport = None
            return

        host = self._addressMatch.group(1)
        host_ip = socket.gethostbyname(host)

        if self._addressMatch.group(4):    # full spec
            netidstr = self._addressMatch.group(2)
        elif self._addressMatch.group(3):  # only first 4 specified
            netidstr = self._addressMatch.group(2) + '.1.1'
        elif self._addressMatch.group(2):  # only last 2 specified
            netidstr = host_ip + '.' + self._addressMatch.group(2)
        else:
            netidstr = host_ip + '.1.1'
        amsport = int(self._addressMatch.group(5))

        self._amsnetaddr = self._packNetID(netidstr, amsport)
        self._iphostport = (host, ADS_PORT)
        self._invid = 1  # InvokeID, should be incremented for every request

        self._socket = None
        self._lock = threading.Lock()

        # little endian floats
        self._f2w = lambda f: list(unpack('>HH', pack('>f', f)))
        self._w2f = lambda w: unpack('>f', pack('>HH', w[0], w[1]))[0]

    def _packNetID(self, netidstr, amsport):
        # convert a.b.c.d.e.f to a single integer
        amsnetid = i = 0
        for (i, x) in enumerate(netidstr.split('.')):
            amsnetid |= (int(x) << (8 * i))
        if i != 5:
            raise ValueError('incomplete NetID')

        # pack the whole address into a 64-bit integer
        return amsnetid | (amsport << 48)

    def initialConnect(self):
        if not self._iphostport:
            return False
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        for i in range(self.MAXCONNECTIONATTEMPTS):
            try:
                self._socket.connect(self._iphostport)
                self._myamsnetaddr = self._packNetID(
                    self._socket.getsockname()[0] + '.1.1', 800)
                v1, v2, v3, name = self._comm(ADS_DEVINFO, '', DEVINFO)
                hw_name = '%s %d.%d.%d' % (name, v1, v2, v3)
                logging.info('connected to %s', hw_name)
            except (IOError, ValueError, CommunicationFailure):
                logging.exception('while connecting')
                if i < self.MAXCONNECTIONATTEMPTS - 1:
                    self.status.emit('Connection attempt %d/%d failed. Retrying!'
                                     % (i + 1, self.MAXCONNECTIONATTEMPTS))
                    if not self._sleep(max(i, 1) * 3):
                        return False
            else:
                self.disconnected = False
                return True

        self.status.emit('Connection failed! Attempts: %d'
                         % self.MAXCONNECTIONATTEMPTS)
        self.error = ('Connection to "%s" could not be established!'
                      % self._address)
        return False

    def connect(self):
        return self.initialConnect()

    def _check_reply_header(self, data, invid):
        header = REP_HEADER.unpack_from(data)
        if header[4] != 0x5:
            raise CommunicationFailure('wrong flags in reply header')
        if header[5] != len(data) - REP_HEADER_SIZE + 4:
            raise CommunicationFailure('wrong length in reply header')
        if header[6]:
            raise CommunicationFailure('error set in reply packet: %s' %
                                       self._translate_error(header[11]))
        if header[7] != invid:
            raise CommunicationFailure('wrong InvokeID on reply packet')
        if header[8]:
            raise CommunicationFailure('error set in reply packet: %s' %
                                       self._translate_error(header[8]))

    def _translate_error(self, errorcode):
        return ADS_ERRORS.get(errorcode,
                              'Unknown code %#04x' % errorcode)

    def _gen_msg(self, cmd, invid, payload):
        mybytes = REQ_HEADER.pack(AMS_HEADER_SIZE + len(payload),
                                  self._amsnetaddr, self._myamsnetaddr, cmd,
                                  0x4, len(payload), 0, invid)
        ba = bytearray(mybytes)
        ba.extend(payload)
        return ba

    def _comm(self, cmd, payload, resunpack):
        invid, self._invid = self._invid, self._invid + 1
        msg = self._gen_msg(cmd, invid, payload)
        expected = REP_HEADER_SIZE + calcsize(resunpack)
        self._socket.sendall(msg)
        reply = b''
        while len(reply) < expected:
            data = self._socket.recv(expected - len(reply))
            if not data:
                raise CommunicationFailure('no data in read')
            reply += data
        self._check_reply_header(reply, invid)
        return unpack_from(resunpack, reply, REP_HEADER_SIZE)

    def _read(self, addr, number):
        # since there is only one data area, we can ignore the function number
        payload = ADS_ADR_LEN.pack(INDEXGROUP_M, 2*addr, 2*number)
        return self._comm(ADS_READ, payload, '<I%dH' % number)[1:]

    def _write(self, addr, values):
        mybytes = ADS_ADR_LEN.pack(INDEXGROUP_M, 2*addr, 2*len(values))
        payload2 = pack('<%dH' % len(values), *values)
        ba = bytearray(mybytes)
        ba.extend(payload2)
        self._comm(ADS_WRITE, ba, '')

    def reconnect(self):
        if self.connect():
            self.disconnected = False
            self._reconnects = 0
        else:
            self._reconnects += 1
            QTimer.singleShot(1000, self.reconnect)

    def startReconnectTimer(self):
        self.disconnected = True
        QTimer.singleShot(1000, self.reconnect)

    def writeRegisters(self, start, values):
        if self.disconnected:
            return
        try:
            with self._lock:
                self._write(start, values)
            return True
        except (IOError, CommunicationFailure):
            logging.exception('while writing %d regs to %#x',
                              len(values), start)
            self.startReconnectTimer()

    def readRegisters(self, start, amount):
        if self.disconnected:
            return
        try:
            with self._lock:
                return list(self._read(start, amount))
        except (IOError, CommunicationFailure):
            logging.exception('while reading %d regs from %#x', amount, start)
            self.startReconnectTimer()


ADS_ERRORS = {
    0x001: 'Internal error',
    0x002: 'No Rtime',
    0x003: 'Allocation locked memory error',
    0x004: 'Insert mailbox error',
    0x005: 'Wrong receive HMSG',
    0x006: 'Target port not found',
    0x007: 'Target machine not found',
    0x008: 'Unknown command ID',
    0x009: 'Bad task ID',
    0x00A: 'No IO',
    0x00B: 'Unknown AMS command',
    0x00C: 'Win32 error',
    0x00D: 'Port not connected',
    0x00E: 'Invalid AMS length',
    0x00F: 'Invalid AMS NetID',
    0x010: 'Low installation level',
    0x011: 'No debug available',
    0x012: 'Port disabled',
    0x013: 'Port already connected',
    0x014: 'AMS Sync Win32 error',
    0x015: 'AMS Sync Timeout',
    0x016: 'AMS Sync AMS error',
    0x017: 'AMS Sync no index map',
    0x018: 'Invalid AMS port',
    0x019: 'No memory',
    0x01A: 'TCP send error',
    0x01B: 'Host unreachable',

    0x500: 'Router: no locked memory',
    0x502: 'Router: mailbox full',

    0x700: 'Error class: device error',
    0x701: 'Service is not supported by server',
    0x702: 'Invalid index group',
    0x703: 'Invalid index offset',
    0x704: 'Reading/writing not permitted',
    0x705: 'Parameter size not correct',
    0x706: 'Invalid parameter value(s)',
    0x707: 'Device is not in a ready state',
    0x708: 'Device is busy',
    0x709: 'Invalid context (must be in Windows)',
    0x70A: 'Out of memory',
    0x70B: 'Invalid parameter value(s)',
    0x70C: 'Not found (files, ...)',
    0x70D: 'Syntax error in command or file',
    0x70E: 'Objects do not match',
    0x70F: 'Object already exists',
    0x710: 'Symbol not found',
    0x711: 'Symbol version invalid',
    0x712: 'Server is in invalid state',
    0x713: 'AdsTransMode not supported',
    0x714: 'Notification handle is invalid',
    0x715: 'Notification client not registered',
    0x716: 'No more notification handles',
    0x717: 'Size for watch too big',
    0x718: 'Device not initialized',
    0x719: 'Device has a timeout',
    0x71A: 'Query interface failed',
    0x71B: 'Wrong interface required',
    0x71C: 'Class ID is invalid',
    0x71D: 'Object ID is invalid',
    0x71E: 'Request is pending',
    0x71F: 'Request is aborted',
    0x720: 'Signal warning',
    0x721: 'Invalid array index',

    0x740: 'Error class: client error',
    0x741: 'Invalid parameter at service',
    0x742: 'Polling list is empty',
    0x743: 'Var connection already in use',
    0x744: 'Invoke ID in use',
    0x745: 'Timeout elapsed',
    0x746: 'Error in Win32 subsystem',
    0x748: 'ADS port not opened',
    0x750: 'Internal error in ADS sync',
    0x751: 'Hash table overflow',
    0x752: 'Key not found in hash',
    0x753: 'No more symbols in cache',
}
