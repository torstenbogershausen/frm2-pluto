# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2015 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA


import logging
import re

from pluto.model.communication import CommunicationDevice


try:
    import PyTango
    TANGO = True
except ImportError:
    TANGO = False


class TangoCommunicationDevice(CommunicationDevice):

    TEMPLATE = 'tango://host:port/domain/class/member'
    HELP = 'host: database ip or fqdn\n' \
           'port: usually 10000\n'
    RE_TEMPLATE = re.compile(r'tango://%s:\d+/[_A-Z]+/[_A-Z]+/[_A-Z]+'
                             % CommunicationDevice.IP_FQDN, re.IGNORECASE)

    def __init__(self, address, offset, parent=None):
        super(TangoCommunicationDevice, self).__init__(address, offset, parent)
        if not TANGO:
            self.error = 'No PyTango found on this host'
        self._proxy = None

    def findOffset(self):
        possible_offsets = [0, 0x3000, 0x4000]
        for offset in possible_offsets:
            try:
                magic = self._proxy.ReadOutputFloat(offset)
                if 2014.0 <= magic < 2046.0:
                    self._offset = offset
                    break
            except PyTango.DevFailed:
                pass
            except AttributeError:
                self.error = 'Unable to read output float from Tango server. ' \
                             'Is it a PLC server?'
                return
        else:
            self.error = 'Offset is not one of the following: %s' % \
                ', '.join(map(hex, possible_offsets))

    def initialConnect(self):
        try:
            self._proxy = PyTango.DeviceProxy(self._address)
            self.findOffset()
            return True
        except PyTango.DevFailed as e:
            logging.warning(e)
            self.status.emit('Connection failed.')
            self.error = ('Connection to "%s" could not be established!'
                          % self._address)
            return False

    def readRegisters(self, start, amount):
        return list(self._proxy.ReadOutputWords([self._offset + start, amount]))

    def writeRegisters(self, start, values):
        self._proxy.WriteOutputWords([self._offset + start] + values)
