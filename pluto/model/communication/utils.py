# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2018 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA


from pluto.model.communication.ads import AdsCommunicationDevice
from pluto.model.communication.direct import PymodbusCommunicationDevice
from pluto.model.communication.tango import TangoCommunicationDevice


def getConnectionDevice(identifier):
    if identifier == 'PyModbus':
        return PymodbusCommunicationDevice
    elif identifier == 'ADS':
        return AdsCommunicationDevice
    elif identifier == 'TANGO':
        return TangoCommunicationDevice


def getValidAddresses(comdev, inp):
    return [x for x in inp if comdev.RE_TEMPLATE.search(x)]
