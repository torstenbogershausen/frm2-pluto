# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2015 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA


import logging
import re
from time import sleep
from random import random
import threading

from pymodbus.client.sync import ModbusTcpClient
from pymodbus.exceptions import ConnectionException
from pymodbus.pdu import ExceptionResponse
from pymodbus.register_read_message import ReadHoldingRegistersResponse

from pluto.qt import QThread, QTimer

from pluto.model.communication import CommunicationDevice


class PymodbusCommunicationDevice(CommunicationDevice):
    CONNECTIONFAILURE = 0
    CONNECTIONSUCCESS = 1

    TEMPLATE = 'host:port'
    HELP = 'host: ip or fqdn\n' \
           'port: if omitted defaults to 502'
    RE_TEMPLATE = re.compile(r'^%s(:\d+)?$' % CommunicationDevice.IP_FQDN,
                             re.IGNORECASE)

    def __init__(self, address, offset, parent=None):
        super(PymodbusCommunicationDevice, self).__init__(address, offset, parent)
        self._bus = None
        self._reconnects = 0
        self._refreshThread = QThread()
        self.disconnected = True
        if ':' in self._address:
            self._address, self._port = self._address.split(':', 1)
            self._port = int(self._port)
        else:
            self._port = 502
        self._lock = threading.Lock()

    def findOffset(self):
        pOffsets = [0x3000, 0x4000]
        for offset in pOffsets:
            with self._lock:
                response = self._bus.read_holding_registers(offset, 1)
            if isinstance(response, ReadHoldingRegistersResponse):
                self._offset = offset
                break
        if not self._offset:
            self.error = 'Offset is not one of the following: %s' % pOffsets

    def writeRegisters(self, start, values):
        if self.disconnected:
            return

        try:
            with self._lock:
                response = self._bus.write_registers(self._offset + start, values)
            while not (response and
                       response.address == self._offset + start and
                       response.count == len(values)):
                logging.warning('no response from Modbus on write, waiting...')
                sleep(random())
                with self._lock:
                    response = self._bus.write_registers(self._offset + start, values)

            return True

        except ConnectionException:
            self.startReconnectTimer()

    def readRegisters(self, start, amount):
        if self.disconnected:
            return
        try:
            with self._lock:
                response = self._bus.read_holding_registers(self._offset + start, amount)
            while response is None:
                logging.warning('no response from Modbus on read, waiting...')
                sleep(random())
                with self._lock:
                    response = self._bus.read_holding_registers(self._offset + start, amount)
            if isinstance(response, ReadHoldingRegistersResponse):
                return response.registers
            elif isinstance(response, ExceptionResponse):
                logging.critical(response)
        except ConnectionException:
            self.startReconnectTimer()

    def connect(self):
        self._bus = ModbusTcpClient(self._address, port=self._port)
        if self._bus.connect():
            self.reconnecting.emit(self.CONNECTIONSUCCESS)
            return True

        return False

    def initialConnect(self):
        for i in range(self.MAXCONNECTIONATTEMPTS):
            if self.connect():
                self.disconnected = False
                if not self._offset:
                    self.findOffset()
                return True
            elif i < self.MAXCONNECTIONATTEMPTS - 1:
                self.status.emit('Connection attempt %d/%d failed. Retrying!'
                                 % (i + 1, self.MAXCONNECTIONATTEMPTS))

                if not self._sleep(max(i, 1) * 3):
                    return False

        self.status.emit('Connection failed! Attempts: %d'
                         % (self.MAXCONNECTIONATTEMPTS))
        self.error = ('Connection to "%s" could not be established!'
                      % self._address)

        return False

    def reconnect(self):
        if self.connect():
            self.disconnected = False
            self._reconnects = 0
        else:
            self._reconnects += 1
            QTimer.singleShot(self._reconnects * 1000, self.reconnect)

    def startReconnectTimer(self):
        self.disconnected = True
        self.reconnecting.emit(self.CONNECTIONFAILURE)

        QTimer.singleShot(1000, self.reconnect)
