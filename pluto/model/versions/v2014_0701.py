# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2015 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA


from collections import OrderedDict

from pluto.gen import UNITCODES
from pluto.model import DeviceExtractorBase, VersionConstantsBase, VERSION, NAME,\
    AUTHOR1, AUTHOR2, UNIT, PARAMS


class DeviceExtractor(DeviceExtractorBase):
    ERROR_FATAL = 0
    ERROR_WARNING = 1

    def __init__(self, comDev, verConstants, parent=None):
        super(DeviceExtractor, self).__init__(comDev, verConstants, parent)
        self.deviceDicts = []
        self.errors = []

    def readSecondaryInfo(self):
        info = {}
        device = self.deviceDicts[self._secondaryInfoDevice]
        deviceNumber = device['number']
        info['name'] = self.readDeviceName(deviceNumber)
        info['firmware'] = self.readFirmware(deviceNumber)
        info['unit'] = self.readUnit(deviceNumber)
        if device['deviceType'] == 'FlatDevice':
            info['params'] = self.readParams(deviceNumber,
                                             device['subType'][1])
        else:
            info['params'] = []
        self.secondaryInfoExtracted.emit(deviceNumber - 1, info)

        DeviceExtractorBase.readSecondaryInfo(self)

    def readDeviceName(self, deviceNumber):
        name = ''
        for i in range(8, 17):
            response = self._questionIndexer(i, deviceNumber)[0]
            if response == 0:
                break
            else:
                name += chr(response & 255)
                if response > 255:
                    name += chr(response >> 8)
                else:
                    break

        return name

    def readFirmware(self, deviceNumber):
        major = self._questionIndexer(NAME, deviceNumber)[0]
        minor = self._questionIndexer(VERSION, deviceNumber)[0]
        patch = self._questionIndexer(AUTHOR1, deviceNumber)[0]
        author = self._questionIndexer(AUTHOR2, deviceNumber)[0]

        if major or minor or patch:
            return '%d.%d.%d by %d' % (major, minor, patch, author)
        return 'unknown'

    def readUnit(self, deviceNumber):
        response = self._questionIndexer(UNIT, deviceNumber)[0]

        if response < 256:  # first bite is 0
            return ''

        unit = self._unitDict[response >> 8]

        if response & 255 > 1:
            unit += ' ^%d' % response & 255

        return unit

    def readParams(self, deviceNumber, quantity):
        params = []
        for i in range(quantity):
            params.append(self._questionIndexer(PARAMS + i, deviceNumber)[0])

        return [params]


class VersionConstants(VersionConstantsBase):
    VERSION = '2014.0701'

    # codes for the commands of status fields
    GO_CMD = 2 << 12
    STOP_CMD = 1 << 12
    RESET_CMD = 0

    # devices with parameters/subdevices (DISPLAYNAMES)
    FLATDEVICES = ['FlatAnalogInput',
                   'FlatAnalogOutput']
    PARAMETERDEVICES = ['ReadableInterface',
                        'MovableInterface']
    DEVICESWITHSUBS = ['MultiDevice']

    EXTENDEDDEVICES = []

    # number of parameters in PARAMDEVICES
    MINPARAMS = 1
    MAXPARAMS = 16
    # highest number a parameter can have
    MAXPARAMVAL = 127
    # number of devices in DEVICESWITHSUBS
    MINSUBS = 2
    MAXSUBS = 32

    def getStatus(self, value):
        '''
            Extracts the status from the given value.
            value: register value containing the statusvalue (first 4 bits)
        '''

        value = value >> 12

        if value >= 8:
            return ('ERROR', {'go': False, 'stop': False, 'reset': True})
        elif value >= 4:
            return ('WARNING', {'go': True, 'stop': False, 'reset': False})
        elif value >= 2:
            return ('BUSY', {'go': False, 'stop': True, 'reset': False})
        elif value == 1:
            return ('IDLE', {'go': True, 'stop': False, 'reset': False})
        return ('RESET', {'go': False, 'stop': False, 'reset': False})

    def getReason(self, value):
        value = value >> 8 & 15
        try:
            return self.reasons[value]
        except KeyError:
            return ''

    def isFunction(self, index):
        '''
        Return True if the specified parameter index belongs to a special
        function instead of a readable parameter.
        '''
        return False

    def isFunctionNoWriteValue(self, index):
        '''
        Return True if the specified parameter index belongs to a special
        function that does not allows a parameter (like HOME)
        instead of a readable parameter.
        '''
        return False

    def createParamIndices(self):
        '''
        The list of parameter indices. changes in the specification should be
        adjusted here.
        '''

        indices = OrderedDict()

        indices['TypCode'] = (0, 'int16')
        indices['Value'] = (1, 'float')
        indices['Target'] = (2, 'float')
        indices['Unit'] = (3, 'char')
        indices['Status'] = (4, 'int32')
        indices['ExtStatus'] = (5, 'int32')
        indices['Offset'] = (10, 'float')
        indices['SAFESTAT'] = (20, 'float')
        indices['ABSMIN'] = (30, 'float')
        indices['ABSMAX'] = (31, 'float')
        indices['USERMIN'] = (32, 'float')
        indices['USERMAX'] = (33, 'float')
        indices['WARNMIN'] = (34, 'float')
        indices['WARNMAX'] = (35, 'float')
        indices['TIMEOUT'] = (36, 'float')
        indices['MAXTRAVELDIST'] = (37, 'float')
        indices['PID_P'] = (51, 'float')
        indices['PID_I'] = (52, 'float')
        indices['PID_D'] = (53, 'float')
        indices['HYSTERESE'] = (56, 'float')
        indices['MAXSPEED'] = (60, 'float')
        indices['ACCEL'] = (61, 'float')
        indices['NOTHALT'] = (66, 'float')
        indices['IDLE_CUR'] = (62, 'float')
        indices['RAMP_CUR'] = (63, 'float')
        indices['MOVE_CUR'] = (64, 'float')
        indices['STOP_CUR'] = (65, 'float')
        indices['u_STEPS'] = (67, 'float')
        indices['SLOPE'] = (68, 'float')
        indices['REFPOS+'] = (69, 'float')
        indices['REFPOS-'] = (71, 'float')

        return indices

    def createParameters(self):
        '''
            Reverses key/value pairs of paramIndices
        '''

        return {self.paramIndices[key][0]: key
                for key in self.paramIndices}

    def createUnits(self):
        '''
            Returns a dictionary containing the units associated with the codes
            read from the indexer.
        '''

        return {
            0: u'',
            1: u'V',
            2: u'A',
            3: u'W',
            4: u'm',
            5: u'g',
            6: u'Hz',
            7: u'T',
            8: u'K',
            9: u'°C',
            10: u'°F',
            11: u'bar',
            12: u'°',
            13: u'Ohm',
            14: u'm/s',
            15: u'm²/s',
            16: u'm³/s',
            17: u'l/min'
        }

    def createExtendedUnits(self):
        '''
            Returns a dictionary for lookup of units like which differ from the
            baseunits like 'mbar' or 'nm'.
        '''
        return UNITCODES

    def createReasons(self):
        '''
            Returns a dictionary containing the phrased reasons asociated with the
            codes found in the status.
        '''

        return {
            0: '',
            1: 'disabled/inhibit active/dependencies missing',
            2: 'timeout/MAXTRAVELDIST reached',
            4: 'negative limit reached',
            8: 'positive limit reached'
        }

    def createDeviceTypeCodes(self):
        '''
            Returns a dictionary containing all the defined devices.
            Each device is accessible via it's main- and subdevice code.
                dict[main][1][sub]
        '''

        from pluto.devices.simplediscreteinput import SimpleDiscreteInput
        from pluto.devices.simpleanaloginput import SimpleAnalogInput
        from pluto.devices.realvalue import RealValue
        from pluto.devices.simplediscreteoutput import SimpleDiscreteOutput
        from pluto.devices.simpleanalogoutput import SimpleAnalogOutput
        from pluto.devices.discreteinput import DiscreteInput
        from pluto.devices.status import Status
        from pluto.devices.analoginput import AnalogInput
        from pluto.devices.discreteoutput import DiscreteOutput
        from pluto.devices.analogoutput import AnalogOutput
        from pluto.devices.flatanaloginput import FlatAnalogInput
        from pluto.devices.flatanalogoutput import FlatAnalogOutput
        from pluto.devices.keyword import Keyword
        from pluto.devices.paraminput import ReadableInterface
        from pluto.devices.paramoutput import MovableInterface

        from pluto.devices.compounddevice import PIDController, PIDControllerRamped
        from pluto.devices.multidevice import MultiDevice

        typeCodes = {
            # Legacy Devices
            0x1201: ('LegacyDevice', (SimpleDiscreteInput,)),
            0x1302: ('LegacyDevice', (SimpleAnalogInput,)),
            0x1401: ('LegacyDevice', (Keyword,)),
            0x1502: ('LegacyDevice', (RealValue,)),
            0x1602: ('LegacyDevice', (SimpleDiscreteOutput,)),
            0x1704: ('LegacyDevice', (SimpleAnalogOutput,)),
            0x1801: ('LegacyDevice', (Status,)),
            0x1a02: ('LegacyDevice', (DiscreteInput,)),
            0x1b03: ('LegacyDevice', (AnalogInput,)),
            0x1e03: ('LegacyDevice', (DiscreteOutput,)),
            0x1f05: ('LegacyDevice', (AnalogOutput,)),
            # Simple Devices
            0x2001: ('SimpleDevice', (SimpleDiscreteInput,)),
            0x2102: ('SimpleDevice', (SimpleAnalogInput,)),
            0x3002: ('SimpleDevice', (SimpleDiscreteOutput,)),
            0x3104: ('SimpleDevice', (SimpleAnalogOutput,)),
            # Devices
            0x4002: ('Device', (DiscreteInput,)),
            0x4103: ('Device', (AnalogInput,)),
            0x5003: ('Device', (DiscreteOutput,)),
            0x5105: ('Device', (AnalogOutput,)),
            # Complex Devices
            0x8006: ('ComplexDevice', (ReadableInterface,)),
            0x9008: ('ComplexDevice', (MovableInterface,)),
            # Compound Devices
            0xa008: ('CompoundDevice', (PIDController,)),
            0xa10a: ('CompoundDevice', (PIDControllerRamped,)),
            # Multidevice Default
            0xc000: ('MultiDevice', (MultiDevice, 0)),
        }

        for i in range(16):
            typeCodes[(0x60 + i << 8) + 3 + 2 * i] = ('FlatDevice',
                                                      (FlatAnalogInput, i))
            typeCodes[(0x70 + i << 8) + 5 + 2 * i] = ('FlatDevice',
                                                      (FlatAnalogOutput, i))

        for i in range(1, 32):
            typeCodes[(0xc0 + i << 8) + 3 + 3 * (i + 1)] = ('MultiDevice',
                                                            (MultiDevice, i + 1))

        return typeCodes
