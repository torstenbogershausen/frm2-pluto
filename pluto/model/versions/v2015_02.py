# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2015 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA


from collections import OrderedDict

from pluto import DEFAULTAUX
from pluto.gen import UNITCODES
from pluto.model import DeviceExtractorBase, VersionConstantsBase, NAME, \
    VERSION, AUTHOR1, AUTHOR2, PARAMS, AUX0, UNIT


class DeviceExtractor(DeviceExtractorBase):
    ERROR_FATAL = 0
    ERROR_WARNING = 1

    def __init__(self, comDev, verConstants, parent=None):
        super(DeviceExtractor, self).__init__(comDev, verConstants, parent)
        self.deviceDicts = []
        self.errors = []

    def readPLCStats(self):
        stats = {}
        stats['name'] = self.readText(NAME, 0)
        stats['versionString'] = self.readText(VERSION, 0)
        stats['author1'] = self.readText(AUTHOR1, 0)
        stats['author2'] = self.readText(AUTHOR2, 0)

        self.plcStatsExtracted.emit(stats)

    def readSecondaryInfo(self):
        info = {}
        device = self.deviceDicts[self._secondaryInfoDevice]
        deviceNumber = device['number']
        deviceCode = device['deviceCode']
        deviceType = device['deviceType']
        if device['name'].startswith('*'):
            info['name'] = self.readDeviceName(deviceNumber)
        # nobody uses these, anyway
        # info['firmware'] = self.readFirmware(deviceNumber)
        if device['unit'] is None:
            info['unit'] = self.readUnit(deviceNumber)
        # typcodes starting with 2,3,4,5 have parameters
        if (deviceCode >> 12) in [2, 3, 4, 5]:
            info['params'] = self.readParams(deviceNumber, deviceCode)
        else:
            info['params'] = []

        if self._constants.hasStatus(deviceType):
            info['aux'] = self.readAuxReasons(deviceNumber, deviceCode,
                                              device['flags'])
        else:
            info['aux'] = []

        self.secondaryInfoExtracted.emit(deviceNumber - 1, info)

        DeviceExtractorBase.readSecondaryInfo(self)

    def readText(self, infoType, deviceNumber):
        response = self._questionIndexer(infoType, deviceNumber)
        text = ''
        for entry in response:
            if entry == 0:
                break
            text += chr(entry & 255)
            if entry > 255:
                text += chr(entry >> 8)
            else:
                break

        return text

    def readDeviceName(self, deviceNumber):
        return self.readText(NAME, deviceNumber)

    def readFirmware(self, deviceNumber):
        version = self.readText(VERSION, deviceNumber)
        author1 = self.readText(AUTHOR1, deviceNumber)
        author2 = self.readText(AUTHOR2, deviceNumber)

        if version and author1 and author2:
            return '%s by %s (%s)' % (version, author1, author2)
        return 'unknown'

    def readUnit(self, deviceNumber):
        response = self._questionIndexer(UNIT, deviceNumber)[0]
        return self._convertUnit(response)

    def readParams(self, deviceNumber, devCode):
        params = []
        results = self._questionIndexer(PARAMS, deviceNumber)
        if (devCode >> 12) in (2, 3):
            for result in results:
                if result > 0:
                    params.append(result & 0xFF)
                    if (result >> 8) > 0:
                        params.append(result >> 8)
                else:
                    break

        else:
            for i, result in enumerate(results):
                for j in range(16):
                    if result & (1 << j):
                        params.append(16 * i + j)

        return [params]

    def readAuxReasons(self, deviceNumber, deviceCode, flags):
        numaux = self._constants.auxAmounts.get(deviceCode, 8)
        reasons = [DEFAULTAUX % i for i in range(numaux)]

        if flags is None:
            # old way: stop at the first empty one
            for i in range(numaux):
                reason = self.readText(AUX0 + i, deviceNumber).strip()
                if not reason:
                    # that's it
                    break
                reasons[i] = reason
        else:
            # new way: only read the flagged ones
            for i in range(numaux):
                if flags & (1 << i):
                    reasons[i] = self.readText(AUX0 + i, deviceNumber).strip()

        return reasons


class VersionConstants(VersionConstantsBase):
    VERSION = '2015.02'

    # codes for the commands of status fields
    GO_CMD = 5 << 12
    STOP_CMD = 7 << 12
    RESET_CMD = 0

    # devices with parameters/subdevices
    FLATDEVICES = [
        'FlatInput',
        'FlatOutput'
    ]
    DEVICESWITHSUBS = [
    ]
    PARAMETERDEVICES = [
        'ParamInput',
        'ParamOutput',
        'ParamOutput64',
        'VectorInput',
        'VectorOutput',
        'ESSMotor'
    ]

    # Later: add (Analog|Discrete)(Input|Output)Ext and StatusExt
    EXTENDEDDEVICES = FLATDEVICES

    # number of parameters in FLATDEVICES
    MINPARAMS = 1
    MAXPARAMS = 256
    # highest number a parameter can have
    MAXPARAMVAL = 255
    # number of devices in DEVICESWITHSUBS
    MINSUBS = 2
    MAXSUBS = 32

    def __init__(self, parent=None):
        super(VersionConstants, self).__init__(parent)
        self.auxAmounts = self.getAuxAmounts()

    def getStatus(self, value):
        '''
            Extracts the status from the given value.
            value: register value containing the statusvalue (first 4 bits)
        '''

        value = value >> 12

        if value == 8:
            return ('ERROR', {'go': False, 'stop': False, 'reset': True})
        elif value == 7:
            return ('STOP', {'go': False, 'stop': True, 'reset': False})
        elif value == 6:
            return ('BUSY', {'go': False, 'stop': True, 'reset': False})
        elif value == 5:
            return ('START', {'go': False, 'stop': True, 'reset': False})
        elif value == 3:
            return ('WARNING', {'go': True, 'stop': False, 'reset': False})
        elif value == 2:
            return ('DISABLED', {'go': False, 'stop': False, 'reset': False})
        elif value == 1:
            return ('IDLE', {'go': True, 'stop': False, 'reset': False})
        elif value == 0:
            return ('RESET', {'go': False, 'stop': False, 'reset': False})

        return VersionConstantsBase.getStatus(self, value)

    def getReason(self, value):
        value = value >> 8 & 15
        try:
            return self.reasons[value]
        except KeyError:
            return ''

    def getReason32(self, value):
        value = value >> 24 & 15
        try:
            return self.reasons[value]
        except KeyError:
            return ''

    def isFunction(self, index):
        '''
        Return True if the specified parameter index belongs to a special
        function instead of a readable parameter.
        '''
        if not index & 0x80:
            return False
        elif not index & 0x40:
            return True
        elif not index & 0x20:
            return False
        elif not index & 0x10:
            return True
        # else reserved -> don't know, assume parameter
        return False

    def isFunctionNoWriteValue(self, index):
        '''
        Return True if the specified parameter index belongs to a special
        function that does not allows a parameter (like HOME)
        instead of a readable parameter.
        '''
        if index == 142:
            return False  # Continous move has a parameter
        else:
            return self.isFunction(index)

    def createParamIndices(self):
        '''
        The list of parameter indices. changes in the specification should be
        adjusted here.
        '''

        indices = OrderedDict()

        indices['--invalid--'] = (0, '')
        indices['Mode'] = (1, 'uint')
        indices['Microsteps'] = (2, 'uint')
        indices['ExtendedStatus'] = (4, 'uint')
        indices['AbsMin'] = (30, 'float')
        indices['AbsMax'] = (31, 'float')
        indices['UserMin'] = (32, 'float')
        indices['UserMax'] = (33, 'float')
        indices['WarnMin'] = (34, 'float')
        indices['WarnMax'] = (35, 'float')
        indices['Timeout'] = (36, 'float')
        indices['MaxTravelDist'] = (37, 'float')
        indices['(Acceltime)'] = (38, 'float')
        indices['Offset'] = (40, 'float')
        indices['Blocksize'] = (43, 'float')
        indices['Window'] = (44, 'float')
        indices['PID_P'] = (51, 'float')
        indices['PID_I'] = (52, 'float')
        indices['PID_D'] = (53, 'float')
        indices['Dragerror'] = (55, 'float')
        indices['Deadband'] = (56, 'float')
        indices['Holdback'] = (57, 'float')
        indices['RefSpeed'] = (58, 'float')
        indices['Jerk'] = (59, 'float')
        indices['Speed'] = (60, 'float')
        indices['Accel'] = (61, 'float')
        indices['IdleCurrent'] = (62, 'float')
        indices['RampCurrent'] = (63, 'float')
        indices['MoveCurrent'] = (64, 'float')
        indices['StopCurrent'] = (65, 'float')
#        indices['NOTHALT'] = (66, 'float')
        indices['(u_STEPS)'] = (67, 'float')  # deprecated!
        indices['Slope'] = (68, 'float')
        indices['RefPos'] = (69, 'float')
        indices['Setpoint'] = (70, 'float')
#        indices['ABORT'] = (130, '')
        indices['FactoryReset'] = (128, '')
        indices['(REF_Neg)'] = (131, 'float')
        indices['(REF_Pos)'] = (132, 'float')
        indices['Home'] = (133, '')
        indices['SetPosition'] = (137, 'float')
        indices['ContMove'] = (142, 'float')

        return indices

    def createParameters(self):
        '''
            Reverses key/value pairs of paramIndices
        '''

        return {self.paramIndices[key][0]: key
                for key in self.paramIndices}

    def createUnits(self):
        '''
            Returns a dictionary containing the units associated with the codes
            read from the indexer.
        '''

        units = OrderedDict()

        units[0] = u''
        units[1] = u'V'
        units[2] = u'A'
        units[3] = u'W'
        units[4] = u'm'
        units[5] = u'g'
        units[6] = u'Hz'
        units[7] = u'T'
        units[8] = u'K'
        units[9] = u'°C'
        units[10] = u'°F'
        units[11] = u'bar'
        units[12] = u'°'
        units[13] = u'Ohm'
        units[14] = u'm/s'
        units[15] = u'm²/s'
        units[16] = u'm³/s'
        units[17] = u's'
        units[18] = u'counts'
        units[19] = u'bar/s'
        units[20] = u'bar/s²'
        units[21] = u'F'
        units[22] = u'H'

        return units

    def createExtendedUnits(self):
        '''
            Returns a dictionary for lookup of units like which differ from the
            baseunits like 'mbar' or 'nm'.
        '''
        return UNITCODES

    def createReasons(self):
        '''
            Returns a dictionary containing the phrased reasons asociated with the
            codes found in the status.
        '''

        return {
            0: '',
            1: 'disabled/inhibit active/dependencies missing',
            2: 'timeout/MAXTRAVELDIST reached',
            4: 'negative limit reached',
            8: 'positive limit reached'
        }

    def createDeviceTypeCodes(self):
        '''
            Returns a dictionary containing all the defined devices.
            Each device is accessible via it's main- and subdevice code.
                dict[main][1][sub]
        '''

        from pluto.devices.simplediscreteinput import SimpleDiscreteInput
        from pluto.devices.simplediscreteinput import SimpleDiscreteInput32
        from pluto.devices.simplediscreteinput64 import SimpleDiscreteInput64
        from pluto.devices.simpleanaloginput import SimpleAnalogInput
        from pluto.devices.realvalue import RealValue
        from pluto.devices.simplediscreteoutput import SimpleDiscreteOutput
        from pluto.devices.simpleanalogoutput import SimpleAnalogOutput
        from pluto.devices.discreteinput import DiscreteInput
        from pluto.devices.status import Status
        from pluto.devices.analoginput import AnalogInput
        from pluto.devices.discreteoutput import DiscreteOutput
        from pluto.devices.analogoutput import AnalogOutput
        from pluto.devices.flatanaloginput import FlatInput_201502
        from pluto.devices.flatanalogoutput import FlatOutput_201502
        from pluto.devices.keyword import Keyword
        from pluto.devices.paraminput import ParamInput
        from pluto.devices.paramoutput import ParamOutput, ESSMotor, ParamOutput64
        from pluto.devices.vectordevices import VectorInput, VectorOutput
        from pluto.devices.databytes import DataBytes

        typeCodes = {
            # Devices/SimpleDevices
            0x1201: ('Devices/SimpleDevices', (SimpleDiscreteInput,)),
            0x1202: ('Devices/SimpleDevices32', (SimpleDiscreteInput32,)),
            0x1302: ('Devices/SimpleDevices', (SimpleAnalogInput,)),
            0x1401: ('Devices/SimpleDevices', (Keyword,)),
            0x1502: ('Devices/SimpleDevices', (RealValue,)),
            0x1602: ('Devices/SimpleDevices', (SimpleDiscreteOutput,)),
            0x1704: ('Devices/SimpleDevices', (SimpleAnalogOutput,)),
            0x1801: ('Devices/SimpleDevices', (Status,)),
            0x1a02: ('Devices/SimpleDevices', (DiscreteInput,)),
            0x1b03: ('Devices/SimpleDevices', (AnalogInput,)),
            0x1e03: ('Devices/SimpleDevices', (DiscreteOutput,)),
            0x1f05: ('Devices/SimpleDevices', (AnalogOutput,)),
            # ParamDevice
            0x4006: ('ParamDevice', (ParamInput,)),
            0x5008: ('ParamDevice', (ParamOutput,)),
            0x500C: ('ESSMotor', (ESSMotor,)),
            0x5010: ('ParamDevice64', (ParamOutput64,)),
        }

        for i in range(1, 0x100):
            typeCodes[(0x0500 + i)] = ('DataBytes',  (DataBytes,))

        for i in range(16):
            typeCodes[(0x20 + i << 8) + 4 + 2 * (i + 1)] = ('FlatDevice',
                                                            (FlatInput_201502, i + 1))
            typeCodes[(0x30 + i << 8) + 6 + 2 * (i + 1)] = ('FlatDevice',
                                                            (FlatOutput_201502, i + 1))

        for i in range(1, 16):
            typeCodes[(0x40 + i << 8) + 4 + 2 * (i + 1)] = ('VectorInput',
                                                            (VectorInput, i + 1))
            typeCodes[(0x50 + i << 8) + 4 + 4 * (i + 1)] = ('VectorOutput',
                                                            (VectorOutput, i + 1))
        return typeCodes

    def createStateMachineStates(self):
        return {
            0: 'NOT_INITIALIZED',
            1: 'DO_READ',
            2: 'DO_WRITE',
            3: 'BUSY',
            4: 'DONE',
            5: 'ERROR_NO_INDEX',
            6: 'ERROR_READONLY',
            7: 'ERROR_RETRY_LATER'
        }

    def hasStatus(self, deviceCode):
        # pylint: disable=bad-continuation
        if deviceCode in [
            0x1201,  # SimpleDiscreteInput
            0x1202,  # SimpleDiscreteInput32
            0x1302,  # SimpleAnalogInput
            0x1401,  # Keyword
            0x1502,  # RealValue
            0x1602,  # SimpleDiscreteOutput
            0x1704,  # SimpleAnalogOutput
        ]:
            return False

        return True

    def getAuxAmounts(self):
        auxSize = {
            # Devices/SimpleDevices
            0x1201: 0,  # SimpleDiscreteInput
            0x1202: 0,  # SimpleDiscreteInput32
            0x1302: 0,  # SimpleAnalogInput
            0x1401: 0,  # Keyword
            0x1502: 0,  # RealValue
            0x1602: 0,  # SimpleDiscreteOutput
            0x1704: 0,  # SimpleAnalogOutput
            0x1801: 8,  # Status
            0x1a02: 8,  # DiscreteInput
            0x1b03: 8,  # AnalogInput
            0x1e03: 8,  # DiscreteOutput
            0x1f05: 8,  # AnalogOutput
            # ParamDevice
            0x4006: 8,  # ParamInput
            0x5008: 8,  # ParamOutput
            0x500C: 8,  # ESSMotor (actually only 5)
            0x5010: 24, # ParamOutput64
        }

        for i in range(16):
            auxSize[(0x20 + i << 8) + 4 + 2 * (i + 1)] = 24  # FlatInput
            auxSize[(0x30 + i << 8) + 6 + 2 * (i + 1)] = 24  # FlatOutput
            auxSize[(0x40 + i << 8) + 4 + 2 * (i + 1)] = 8  # VectorInput
            auxSize[(0x50 + i << 8) + 4 + 4 * (i + 1)] = 8  # VectorOutput

        return auxSize
