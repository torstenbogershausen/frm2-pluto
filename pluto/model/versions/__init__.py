# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2015 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

'''
    As of 9 Jan 2015:

    Every version file must be named 'v(major)_(minor).py'.
    It must contain the following methods:

    _readDevices(self)
    readSecondaryInfo(self)


    doing the following:

    _readDevices:
        generate a dictionary with the following content:

        'number': The number of the device in the indexer (int)
        'deviceType': The main type of the device extracted from 'Typcode' (str)
        'subType': The subtype of the device extracted from 'Typcode'
                   Tuple of the device-class and it's Subs (int)
        'register': The offset of the device relative to the global offset (int)
        'size': The amount of registers the device occupies (int)
        'name': The name of the device * (str)
        'firmware': The device's firmware * (str)
        'unit': The unit in which the device 'operates' * (str)
        'flags': The 32-bit bitmask of used AUX strings (int)
        'names': The names of the subdevices if subtype is Multidevice ** (list)

        * if empty, extracted from the indexer later
        ** set to default names later and overwritten
           (maybe provided by the indexer in later versions)

        and append it to 'self.deviceDicts'

    readSecondaryInfo:
        generate a dictionary with the following content:

        'name': The name of the device * (str)
        'firmware': The device's firmware * (str)
        'unit': The unit in which the device 'operates' * (str)
        'params': Used Parameter numbers (list)
        'aux': Aux strings * (list)

        * extracted from the indexer

        end with the following to hide the loading message:

        self.secondaryInfoExtracted.emit(-1, {})
'''


def getSupportedVersions():
    return ['2015.02', '2014.0701']
