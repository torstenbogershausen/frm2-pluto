# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2016 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA


import logging
from random import random
from collections import OrderedDict
from time import sleep

from pluto.qt import QObject, pyqtSignal, Qt, QTimer

from pluto import DEFAULTAUX

# indexer information types
TYPECODE = 0
SIZE = 1
ADDRESS = 2
UNIT = 3
NAME = 4
VERSION = 5
AUTHOR1 = 6
AUTHOR2 = 7
PARAMS = 15
AUX0 = 16
# additional types are not yet needed
# for completion reasons:
# AUXi = 16 +i for i in range(24)
# CYCLE = 127


class DeviceExtractorBase(QObject):
    status = pyqtSignal(str)
    progress = pyqtSignal(dict)
    primaryInfoExtracted = pyqtSignal()
    plcStatsExtracted = pyqtSignal(dict)
    secondaryInfoExtracted = pyqtSignal(int, dict)
    numberFound = pyqtSignal(int)
    triggerSecondaryInfoRead = pyqtSignal(int)

    def __init__(self, comDev, offset, verConstants, parent=None):
        super(DeviceExtractorBase, self).__init__(parent)
        self._comDev = comDev
        self._constants = verConstants
        self.indexer = None
        self.indexerSize = 1
        self._numberOfDevices = 0
        self._secondaryInfoDevice = 0
        self._errors = []

        self.triggerSecondaryInfoRead.connect(self.readSecondaryInfo,
                                              Qt.QueuedConnection)

    def readIndexerParameters(self):
        self._determineIndexerSize()
        self._findNumberOfDevices()

    def _extractInfoStruct(self, info):
        '''Extract the full information in the TYPECODE reply that is provided
        by newer PLC indexers, saving a lot of queries.

        Returns None if no such info is found.
        '''
        if len(info) < 10:
            return
        size, offset, unit = info[1:4]
        flags = (info[5] << 16) | info[4]
        if size + offset + unit + flags == 0:
            return
        # not used in Pluto
        # absmin = getFloat(info[6:8])
        # absmax = getFloat(info[8:10])
        bytelist = []
        for word in info[10:]:
            bytelist.append(word & 0xff)
            bytelist.append(word >> 8)
        try:
            # only use name if we're sure it's fully transferred,
            # i.e. contains the terminating null byte
            term = bytelist.index(0)
            name = ''.join(map(chr, bytelist[:term]))
        except ValueError:
            name = ''
        return size, offset, unit, flags, name

    def readPrimaryInfo(self):
        while not self.indexer:
            try:
                self.indexer = self._comDev.readRegisters(2, 1)[0] // 2
            except IndexError:
                pass
        self.readIndexerParameters()

        dOffset = self.indexer + 2

        for i in range(1, self._numberOfDevices + 1):
            info = self._questionIndexer(TYPECODE, i)

            deviceCode = info[0]
            if deviceCode == 0:
                logging.info('Done reading Devices. numberOfDevices=%u' % (self._numberOfDevices))
                break

            additional = self._extractInfoStruct(info)
            logging.info('DeviceNo=%u additional=%s' % (i, additional))
            if additional is not None:
                size = additional[0] // 2
                dOffset = additional[1] // 2
                unit = self._convertUnit(additional[2])
                flags, name = additional[3:]
                logging.info('DeviceNo=%u size=%u dOffset=%u unit=%s flags=0x%x name=%s' % (i, size, dOffset, unit, flags, name))
            else:
                size = self._questionIndexer(SIZE, i)[0] // 2
                if size == 0:
                    size = deviceCode & 255
                curOffset = self._questionIndexer(ADDRESS, i)[0] // 2
                if curOffset:
                    dOffset = curOffset
                # read these later
                unit = None
                flags = None
                name = ''

            try:
                subType = self._constants.deviceTypeCodes[deviceCode][1]
                devType = self._constants.deviceTypeCodes[deviceCode][0]
                name = name or '*%s' % subType[0].DISPLAYNAME

                device = {
                    'deviceCode': deviceCode,
                    'number': i,
                    'deviceType': devType,
                    'subType': subType,
                    'register': dOffset,
                    'size': size,
                    'name': name,
                    'firmware': '',
                    'unit': unit,
                    'flags': flags,
                    'names': [],
                }

                self.deviceDicts.append(device)
                self.progress.emit({'message': 'Device %d Extracted' % i,
                                    'step': i})
                logging.info('devType: %s, \n\tsubType: %s, \n\tregisters: %d',
                             devType, subType, size)

            except KeyError:
                error = ('Device %d could not be initiated.'
                         ' Interfaceversion %s does not specify %#x'
                         % (i, self._constants.VERSION, deviceCode))
                self.errors.append((self.ERROR_WARNING, error))
                logging.warning(error)
                self.progress.emit({'message': error, 'step': i})

            dOffset += size

        self.primaryInfoExtracted.emit()

        self.readPLCStats()

        if self.deviceDicts:
            QTimer.singleShot(50, self.readSecondaryInfo)
        else:
            self.secondaryInfoExtracted.emit(-1, {})

    def readPLCStats(self):
        pass

    def readSecondaryInfo(self):
        self._secondaryInfoDevice += 1
        if self._secondaryInfoDevice >= len(self.deviceDicts):
            self.secondaryInfoExtracted.emit(-1, {})
        else:
            QTimer.singleShot(50, self.readSecondaryInfo)

    def readAuxReasons(self, deviceNumber, deviceCode, flags):
        return [DEFAULTAUX % i for i in range(8)]

    def _determineIndexerSize(self):
        self.status.emit('Determining the size of the indexer.')
        size = self._questionIndexer(SIZE, 0)[0]

        if size:
            self.indexerSize = size // 2 - 1

    def _questionIndexer(self, infoType, devNumber):
        if infoType > 127:
            self._errors.append('invalid infoType for device %d' % devNumber)
            return 0
        try:
            question = (infoType << 8) + devNumber
            for i in range(1, 10):
                self._comDev.writeRegisters(self.indexer, [question])
                sleep(0.01)
                values = self._comDev.readRegisters(self.indexer,
                                                    1 + self.indexerSize)
                if values[0] == question | (1 << 15):
                    actual = int(values[0])
                    expect = question | (1 << 15)
                if actual == expect:
                    return values[1:]
                logging.warning('slow indexer query, waiting. devNumber=%d infoType=%d actual=0x%s expect=0x%x' \
                                % (devNumber, infoType, actual, expect))
                sleep(0.01 + random() * 2 * i)
            raise RuntimeError('no response to indexer query (%d, %d)' %
                               (devNumber, infoType))

        except Exception as e:
            logging.warning(e)
            raise

    def _findNumberOfDevices(self):
        self.status.emit('Searching for the amount of devices.')
        number = 128
        step = 64

        for _ in range(8):
            if self._questionIndexer(TYPECODE, number)[0]:
                number += step
                # if all following queries result in 0 THIS is the number
                # of devices
                self._numberOfDevices = number
            else:
                number -= step

            step //= 2

        self.numberFound.emit(self._numberOfDevices)

    def _convertUnit(self, response):
        if response in self._constants.extUnits:
            return self._constants.extUnits[response]
        numUnit = response & 0xFF
        exp = response >> 8
        if exp > 127:
            exp -= 256
        if numUnit in self._constants.units:
            return '10^%d %s' % (exp, self._constants.units[numUnit])
        return '10^%d unit%d' % (exp, numUnit)


class VersionConstantsBase(QObject):
    MAXPARAMVAL = 0

    def __init__(self, parent=None):
        super(VersionConstantsBase, self).__init__(parent)
        self.paramIndices = self.createParamIndices()
        self.parameters = self.createParameters()
        self.stateMachineStates = self.createStateMachineStates()
        self.units = self.createUnits()
        self.extUnits = self.createExtendedUnits()
        self.reasons = self.createReasons()
        self.deviceTypeCodes = self.createDeviceTypeCodes()
        self.deviceTypeSizes = self.createDeviceTypeSizes()
        self.wholeRangeParameters = self.createWholeRangeParameters()
        self.auxAmounts = {}

    def getUnitsList(self):
        if self.extUnits:
            return self.extUnits.values()
        return self.units.values()

    def getDeviceCode(self, main, sub):
        '''
        Extracts the code read from the indexer for any given device combo.
        main: 'deviceType' of the device
        sub: 'subType' of the device
        '''

        for typeCode in self.deviceTypeCodes:
            if self.deviceTypeCodes[typeCode][0] == main:
                if self.deviceTypeCodes[typeCode][1] == sub:
                    return typeCode

        return 'Combination of %s and %s is not specified' % (main, sub)

    def getStatus(self, value):
        return ('NOT IMPLEMENTED', {'go': False, 'stop': False, 'reset': False})

    def createParamIndices(self):
        return OrderedDict()

    def createStateMachineStates(self):
        pass

    def createParameters(self):
        return {}

    def createUnits(self):
        return {}

    def createExtendedUnits(self):
        return {}

    def createReasons(self):
        return {i: 'reasons not implemented in versionfile' for i in range(16)}

    def createDeviceTypeSizes(self):
        '''
        Returns an ordered dictionary containing the sizes of all the defined
        devices.
        '''

        # (DISPLAYNAME, subs): ((class, subs), size)

        sizes = OrderedDict()

        for deviceCode in sorted(self.deviceTypeCodes):
            device = self.deviceTypeCodes[deviceCode][1]
            name = device[0].DISPLAYNAME
            size = deviceCode & 255
            subs = device[1] if len(device) == 2 else ''
            sizes[(name, subs)] = (device, size)

        return sizes

    def createDeviceTypeCodes(self):
        return {}

    def createWholeRangeParameters(self):
        params = list(self.paramIndices)
        for i in range(self.MAXPARAMVAL + 1):
            if i not in self.parameters:
                params.append(str(i))

        return params
