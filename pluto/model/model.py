# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2015 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA


import logging
from time import sleep
from socket import gethostbyname, getfqdn
from struct import unpack, pack
from random import random
from datetime import datetime
from threading import Thread
from collections import OrderedDict

import numpy

from pluto.qt import Qt, QObject, QTimer, pyqtSignal, QThread, pyqtSlot

from pluto.devices.rawdevice import RawDevice
from pluto.model.communication.direct import PymodbusCommunicationDevice
from pluto.model.communication.tango import TangoCommunicationDevice
from pluto.model.communication.ads import AdsCommunicationDevice

# Converters: E.g: What happens if a float is send to the PLC ?
# First the float is converted into a byte array
# Second, the byte array is converted into 16 bit words.
# When modbus is used, each 16 bit word is written inte a modbus register
# Note that some "PLCs" use big endian here, so we need another conversion
# from bytes to registers (called NET here, since we see them on the network)
CCONV_FLOAT32_TO_BYTES = 0
CCONV_UINT32_TO_BYTES  = 1
CCONV_BYTES_TO_NET32   = 2
CCONV_FLOAT64_TO_BYTES = 3
CCONV_UINT64_TO_BYTES  = 4
CCONV_BYTES_TO_NET64   = 5
allconfs = (('<f', '<I', '<HH', '<d', '<Q', '<HHHH'),
            ('<f', '<I', '>HH', '<d', '<Q', '>HHHH'),
            ('>f', '>I', '<HH', '>d', '>Q', '<HHHH'),
            ('>f', '>I', '>HH', '>d', '>Q', '>HHHH'))

class Model(QObject):
    ERROR_FATAL = 0
    ERROR_WARNING = 1

    # comDev slots:
    reconnecting = pyqtSignal(bool)
    # regBuffer slots:
    dataUpdated = pyqtSignal()
    # devExtractor slots:
    status = pyqtSignal(str)
    progress = pyqtSignal(dict)
    primaryInfoExtracted = pyqtSignal()
    plcStatsExtracted = pyqtSignal()
    secondaryInfoExtracted = pyqtSignal(int, dict)
    numberFound = pyqtSignal(int)

    # model slots:
    netStatsExtracted = pyqtSignal()
    manualSignal = pyqtSignal()
    rawSignal = pyqtSignal()
    errorsOccurred = pyqtSignal()

    def __init__(self, stats, raw, manual, communication, parent=None):
        super(Model, self).__init__(parent)

        self.errors = []
        self._conv = None

        if raw:
            self._version = raw['version']
        if manual:
            self._version = manual['version']
        else:
            self._version = None

        if manual:
            self.versionString = manual['versionString']
            self.author1 = manual['author1']
            self.author2 = manual['author2']
        else:
            self.versionString = ''
            self.author1 = ''
            self.author2 = ''

        self.devices = []
        self.ip = ''
        self._raw = raw
        self._manual = manual
        self._communication = communication
        self.fqdn = ''
        self.netThread = None
        self.updateThread = None
        self._extractorThread = None
        self._communicationThread = None
        self.address = stats['address']
        self._offset = int(stats['offset'])
        self.refresh = stats['refresh']
        self.devExtractor = None
        self.comDev = None
        self.registerBuffer = None
        self.name = stats['name']

    def start(self):
        if not self.initiateCommunication():
            return

        self.registerBuffer = RegisterBuffer(self.comDev, self.refresh)

        self.registerBuffer.dataUpdated.connect(self.dataUpdated)

        if self._version:
            self.loadVersionSpecifics()

        if not self.comDev.initialConnect() or self.comDev.error:
            self.errors.append((self.ERROR_FATAL, self.comDev.error))
            self.errorsOccurred.emit()
            self.comDev = None
            return

        self.registerBuffer.updateData()

        if not self._version:
            if not self._readVersion():
                self.comDev = None
                return
            self.loadVersionSpecifics()

        self.deviceDicts = []

        if self._raw:
            self._first = self._raw['first']
            self._last = self._raw['last']
            self._conv = self._raw['conversion']
            self._setRawDevices()
            self.startAutoRefresh()
            self.rawSignal.emit()
        elif self._manual:
            self._conv = self._manual['conversion']
            self.startAutoRefresh()
            self.manualSignal.emit()
        else:
            self._extractorThread = QThread()
            self.devExtractor.moveToThread(self._extractorThread)
            self._extractorThread.started.connect(self.devExtractor.readPrimaryInfo)
            self.devExtractor.primaryInfoExtracted.connect(self.setInitialBufferRange)
            self.devExtractor.primaryInfoExtracted.connect(self.primaryInfoExtracted)
            self.devExtractor.plcStatsExtracted.connect(self.setPLCStats)
            self.devExtractor.secondaryInfoExtracted.connect(self.secondaryInfoExtracted)
            self.devExtractor.status.connect(self.status)
            self.devExtractor.progress.connect(self.progress)
            self.devExtractor.numberFound.connect(self.numberFound)

            self.devExtractor.primaryInfoExtracted.connect(self.startAutoRefresh)

            self._extractorThread.start()

    def initiateCommunication(self):
        if self._communication == 'PyModbus':
            self.comDev = PymodbusCommunicationDevice(self.address, self._offset)
            self.comDev.reconnecting.connect(self.reconnecting)
            self.comDev.status.connect(self.status)
            self._startNetThread()
        elif self._communication == 'ADS':
            self.comDev = AdsCommunicationDevice(self.address)
            self.comDev.status.connect(self.status)
        elif self._communication == 'TANGO':
            self.comDev = TangoCommunicationDevice(self.address, self._offset)
        else:
            self.errors.append((self.ERROR_FATAL,
                                'No supported communication selected'))
            self.errorsOccurred.emit()
            return
        if self.comDev.error:
            self.errors.append((self.ERROR_FATAL, self.comDev.error))
            self.errorsOccurred.emit()
            return

        return True

    def loadVersionSpecifics(self):
        vString = self.getVString()
        versionmod = __import__('pluto.model.versions.%s' % vString,
                                None, None, ['*'])
        self.verConstants = versionmod.VersionConstants()

        if not self._raw and not self._manual:
            self.devExtractor = versionmod.DeviceExtractor(self.comDev,
                                                           self._offset,
                                                           self.verConstants)

    def getVString(self):
        return 'v%s_%s' % (self._version[:4], self._version[5:])

    def closeSubThreads(self):
        if self._extractorThread:
            self._extractorThread.exit()
            self._extractorThread.wait()
        if self.updateThread:
            self.updateThread.exit()
            self.updateThread.wait()

    @pyqtSlot()
    def startAutoRefresh(self):
        self.registerBuffer.createRefreshTimer()

    @pyqtSlot()
    def setInitialBufferRange(self):
        '''Set initial buffer range to contain the last device, so that the
        buffer range is not increased step by step as devices are added.
        '''
        if not self.devExtractor.deviceDicts:
            # nothing todo if there are no devices....
            return
        first = self.devExtractor.deviceDicts[0]
        last = self.devExtractor.deviceDicts[-1]
        self.registerBuffer.changeUpdateRange(first['register'],
                                              last['register'] + last['size'])

    def getRefreshInterval(self):
        return self.registerBuffer.getRefreshInterval()

    def setRefreshInterval(self, interval):
        self.registerBuffer.setRefreshInterval(interval)

    def _readVersion(self):
        for conv in allconfs:
            # Probe the different endianess.
            if (2014.0701 <= round(self.getFloat(0, conv), 4)
                    <= datetime.now().year + 1):
                self._conv = conv
                self._version = str(round(self.getFloat(0, conv), 4))
                return self._version

        logging.error('No version magicnumber found!')
        self.errors.append((self.ERROR_FATAL, 'No version magicnumber found'))
        self.errorsOccurred.emit()
        return None

    def _setRawDevices(self):
        for i in range(self._first, self._last):
            self.deviceDicts.append({'deviceCode': 0,
                                     'number': -1,
                                     'deviceType': 'RawDevice',
                                     'subType': (RawDevice,),
                                     'register': i,
                                     'size': 1,
                                     'name': str(i),
                                     'firmware': '',
                                     'unit': '',
                                     'params': [],
                                     'names': []})

    def _floatToNet(self, val):
        return list(unpack(self._conv[CCONV_BYTES_TO_NET32], pack(self._conv[CCONV_FLOAT32_TO_BYTES], float(val))))

    def _doubleToNet(self, val):
        return list(unpack(self._conv[CCONV_BYTES_TO_NET64], pack(self._conv[CCONV_FLOAT64_TO_BYTES], val)))

    def _uint32ToNet(self, val):
        return list(unpack(self._conv[CCONV_BYTES_TO_NET32], pack(self._conv[CCONV_UINT32_TO_BYTES], val)))

    def _uint64ToNet(self, val):
        return list(unpack(self._conv[CCONV_BYTES_TO_NET64], pack(self._conv[CCONV_UINT64_TO_BYTES], val)))

    def getWord(self, reg, signed=False):
        rawValue = self.registerBuffer.getRegister(reg)
        if signed:
            return numpy.int16(rawValue)
        return rawValue

    def writeWord(self, value, register, signed=False):
        if signed:
            value = int(value)

        self.comDev.writeRegisters(register, [value])
        self.registerBuffer.triggerUpdate()

    def getMultipleWord(self, reg, size, signed=False):
        wordList = []
        for i in range(size):
            wordList.append(self.getWord(reg + i, signed))
        return wordList

    def writeMultipleWord(self, words, reg):
        values = []
        for word in words:
            if word[1]:
                values.append(int(word[0]))
            else:
                values.append(word[0])

        self.comDev.writeRegisters(reg, values)
        self.registerBuffer.triggerUpdate()

    def getFloat(self, reg, conv=None):
        try:
            regs = self.registerBuffer.getRegisters(reg, 2)

            if conv is None:
                if self._conv is None:
                    return
                else:
                    conv = self._conv

            return unpack(conv[CCONV_FLOAT32_TO_BYTES], pack(conv[CCONV_BYTES_TO_NET32], regs[0], regs[1]))[0]

        # pylint: disable=broad-except
        except Exception as e:
            logging.error(e)

    def getFloat64(self, reg, conv=None):
        try:
            regs = self.registerBuffer.getRegisters(reg, 4)

            if conv is None:
                if self._conv is None:
                    return
                else:
                    conv = self._conv

            return unpack(conv[CCONV_FLOAT64_TO_BYTES],
                          pack(conv[CCONV_BYTES_TO_NET64], regs[0], regs[1], regs[2], regs[3]))[0]
        except Exception as e:
            logging.error(e)
            
    def writeFloat(self, val, register):
        self.comDev.writeRegisters(register, self._floatToNet(val))
        self.registerBuffer.triggerUpdate()

    def getDWord(self, reg, conv=None):
        try:
            regs = self.registerBuffer.getRegisters(reg, 2)

            if conv is None:
                if self._conv is None:
                    return
                else:
                    conv = self._conv

            return unpack(conv[CCONV_UINT32_TO_BYTES], pack(conv[CCONV_BYTES_TO_NET32], regs[0], regs[1]))[0]

        # pylint: disable=broad-except
        except Exception as e:
            logging.error(e)

    def getQWord(self, reg, conv=None):
        try:
            regs = self.registerBuffer.getRegisters(reg, 4)

            if conv is None:
                if self._conv is None:
                    return
                else:
                    conv = self._conv

            return unpack(conv[CCONV_UINT64_TO_BYTES], pack(conv[CCONV_BYTES_TO_NET64], regs[0], regs[1], regs[2], regs[3]))[0]

        # pylint: disable=broad-except
        except Exception as e:
            logging.error(e)

    def writeDWord(self, val, register):
        self.comDev.writeRegisters(register, self._uint32ToNet(val))
        self.registerBuffer.triggerUpdate()

    def writeQWord(self, val, register):
        self.comDev.writeRegisters(register, self._uint64ToNet(val))
        self.registerBuffer.triggerUpdate()

    def getMultipleFloat(self, reg, size):
        floatList = []
        for i in range(size):
            floatList.append(self.getFloat(reg + i * 2))
        return floatList

    def writeMultipleFloat(self, floats, reg):
        values = []
        for value in floats:
            values += self._floatToNet(value)

        self.comDev.writeRegisters(reg, values)
        self.registerBuffer.triggerUpdate()

    def getWordFloat(self, reg):
        return (self.getWord(reg), self.getFloat(reg + 1))

    def getWordDouble(self, reg):
        return (self.getWord(reg), self.getFloat64(reg + 1))

    def writeWordFloat(self, wordVal, floatVal, reg):
        self.comDev.writeRegisters(reg, [wordVal] + self._floatToNet(floatVal))
        self.registerBuffer.triggerUpdate()

    def writeWordDouble(self, wordVal, doubleVal, reg):
        self.comDev.writeRegisters(reg, [wordVal] + self._doubleToNet(doubleVal))
        self.registerBuffer.triggerUpdate()

    def getWordDWord(self, reg):
        return (self.getWord(reg), self.getDWord(reg + 1))

    def getWordQWord(self, reg):
        return (self.getWord(reg), self.getQWord(reg + 1))
    
    def writeWordDWord(self, wordVal, dwordVal, reg):
        self.comDev.writeRegisters(reg, [wordVal] + self._uint32ToNet(dwordVal))
        self.registerBuffer.triggerUpdate()

    def writeWordQWord(self, wordVal, qwordVal, reg):
        self.comDev.writeRegisters(reg, [wordVal] + self._uint64ToNet(qwordVal))
        self.registerBuffer.triggerUpdate()

    def getFloatWord(self, reg):
        return (self.getFloat(reg), self.getWord(reg + 2))

    def writeFloatWord(self, floatVal, wordVal, reg):
        self.comDev.writeRegisters(reg, self._floatToNet(floatVal) + [wordVal])
        self.registerBuffer.triggerUpdate()

    def writeDoubleDWord(self, doubleVal, dwordVal, reg):
        self.comDev.writeRegisters(reg, self._doubleToNet(doubleVal)
                                        + self._uint32ToNet(dwordVal))
        self.registerBuffer.triggerUpdate()

    def _startNetThread(self):
        self.netThread = Thread(target=self._readNetStats, args=())
        self.netThread.daemon = True
        self.netThread.start()

    def _readNetStats(self):
        hostname = self.address.split(':')[0]
        self.ip = gethostbyname(hostname)
        self.fqdn = getfqdn(self.ip)
        while self.ip == self.fqdn:
            sleep(5)
            self.ip = gethostbyname(hostname)
            self.fqdn = getfqdn(self.ip)

        self.netStatsExtracted.emit()

    def getStats(self):
        stats = OrderedDict()
        stats['name'] = self.name
        stats['version'] = self._version
        stats['versionString'] = self.versionString
        stats['address'] = self.address
        stats['conversion'] = self._conv
        stats['communication'] = self._communication
        stats['author1'] = self.author1
        stats['author2'] = self.author2

        stats.update(self.comDev.getStats())
        stats.update(self.registerBuffer.getStats())

        return stats

    def getDeviceDicts(self):
        if self.deviceDicts:
            return self.deviceDicts
        return self.devExtractor.deviceDicts

    def getIndexerSize(self):
        return self.devExtractor.indexerSize if self.devExtractor else 0

    def getIndexerOffset(self):
        return self.devExtractor.indexer if self.devExtractor else 0

    def setPLCStats(self, stats):
        self.__dict__.update(stats)
        self.plcStatsExtracted.emit()


class RegisterBuffer(QObject):

    dataUpdated = pyqtSignal()

    def __init__(self, comDev, refresh, parent=None):
        super(RegisterBuffer, self).__init__(parent)
        self._comDev = comDev
        self._first = 0
        self._last = 3
        self._refresh = max(int(refresh), 100)
        self._registers = []
        self._timer = None

    def getStats(self):
        return {
            'refresh': self._refresh
        }

    def getRefreshInterval(self):
        return self._timer.interval()

    def setRefreshInterval(self, interval):
        self._timer.stop()
        self._timer.timeout.disconnect(self.updateData)
        self._refresh = max(interval, 100)
        self.createRefreshTimer()

    def getRegisters(self, first, amount=1):
        if first < 0 or amount < 1:
            return []
        self.checkBufferRange(first, first + amount)

        while True:
            try:
                return self._registers[first:first + amount]
            except IndexError:
                logging.warning('needed register not read yet, waiting...')
                sleep(random())

    def getRegister(self, register):
        self.checkBufferRange(register, register + 1)

        while True:
            try:
                return self._registers[int(register)]
            except IndexError:
                logging.warning('needed register not read yet, waiting...')
                sleep(random())

    def checkBufferRange(self, first, last):
        if self._first > first or self._last < last:
            self.changeUpdateRange(min(self._first, first),
                                   max(self._last, last))

    def changeUpdateRange(self, first, last):
        self._first = first
        self._last = last
        return self.updateData()

    def createRefreshTimer(self):
        self._timer = QTimer()
        self._timer.setInterval(self._refresh)
        self._timer.timeout.connect(self.updateData, Qt.QueuedConnection)
        self._timer.start()
        self.triggerUpdate()

    def triggerUpdate(self):
        self._timer.timeout.emit()

    def updateData(self):
        self.updateRegisters()
        self.dataUpdated.emit()

    def updateRegisters(self):
        # since the offset into the registers is always from the start address,
        # fill unneeded addresses with bogus values
        registers = [0] * self._first
        lastdone = self._first

        while lastdone < self._last:
            amount = min(125, self._last - lastdone)
            start = lastdone
            response = self._comDev.readRegisters(start, amount)
            while not isinstance(response, list) or len(response) != amount:
                logging.warning('registers not read, waiting...')
                sleep(random())
                response = self._comDev.readRegisters(start, amount)
            registers += response
            lastdone += amount

        self._registers = registers
