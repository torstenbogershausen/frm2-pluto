# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2015 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

"""Qt 4/5 compatibility layer."""

# pylint: disable=wildcard-import, unused-import, unused-wildcard-import
# pylint: disable=ungrouped-imports, invalid-name
# PyQt4.QtCore re-exports the original bin, hex and oct builtins
# pylint: disable=redefined-builtin

import sys

try:
    import PyQt5

except (ImportError, RuntimeError):
    import sip
    sip.setapi('QString', 2)
    sip.setapi('QVariant', 2)

    from PyQt4.QtGui import *
    from PyQt4.QtCore import *
    from PyQt4 import uic

    import pluto.gui.res_qt4

    # Compatibility fix: the QFileDialog methods in PyQt5 correspond
    # to the ...AndFilter methods in PyQt4.

    orig_QFileDialog = QFileDialog

    # pylint: disable=function-redefined
    class QFileDialog(orig_QFileDialog):

        @staticmethod
        def getOpenFileName(*args, **kwds):
            return orig_QFileDialog.getOpenFileNameAndFilter(*args, **kwds)

        @staticmethod
        def getOpenFileNames(*args, **kwds):
            return orig_QFileDialog.getOpenFileNamesAndFilter(*args, **kwds)

        @staticmethod
        def getSaveFileName(*args, **kwds):
            return orig_QFileDialog.getSaveFileNameAndFilter(*args, **kwds)

else:
    # Do not abort on exceptions in signal handlers.
    sys.excepthook = lambda *args: sys.__excepthook__(*args)

    from PyQt5.QtGui import *
    from PyQt5.QtWidgets import *
    from PyQt5.QtCore import *
    from PyQt5 import uic

    import pluto.gui.res_qt5
