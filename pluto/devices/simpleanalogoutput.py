# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.qt import pyqtSlot

from pluto.devices import BaseDevice
from pluto.devices.fields.currentvaluefield import CurrentValueFloatField
from pluto.devices.fields.targetvaluefield import TargetValueFloatField


class SimpleAnalogOutput(BaseDevice):
    '''
        N    Current(float32), low Word
        N+1  Current(float32), high Word
        N+2  Target(float32), low Word
        N+3  Target(float32), high Word
    '''

    DISPLAYNAME = 'SimpleAnalogOutput'

    def __init__(self, paramDict, model, parent=None):
        super(SimpleAnalogOutput, self).__init__(paramDict, model, parent)

        self.fields['target'].buttonGo.clicked.connect(self._go)
        self.fields['target'].buttonStop.clicked.connect(self._stop)

    @pyqtSlot()
    def updateValues(self):
        current = self.model.getFloat(self.register)
        target = self.model.getFloat(self.register + 2)

        if current != target:
            self._setGoEnabled(False)
            self._setStopEnabled()
        else:
            self._setGoEnabled()
            self._setStopEnabled(False)

        BaseDevice.updateValues(self)

    def _generateFields(self, register):
        self.fields['current'] = CurrentValueFloatField(register)
        self.fields['target'] = TargetValueFloatField(register + 2)

    def _insertFields(self):
        self.valueLayout.addWidget(self.fields['current'], 0, 0)
        self.valueLayout.addWidget(self.fields['target'], 0, 1)

    def updateRegisters(self, register):
        self.fields['current'].updateRegister(register)
        self.fields['target'].updateRegister(register + 2)

    @pyqtSlot()
    def _go(self):
        self.model.writeFloat(self.fields['target'].getValue(),
                              self.fields['target'].register)

    @pyqtSlot()
    def _stop(self):
        self.model.writeFloat(self.fields['current'].getValue(),
                              self.fields['target'].register)

    def _setGoEnabled(self, state=True):
        if state in (True, False):
            self.fields['target'].buttonGo.setEnabled(state)

    def _setStopEnabled(self, state=True):
        if state in (True, False):
            self.fields['target'].buttonStop.setEnabled(state)
