# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

import logging

from pluto.qt import pyqtSlot, pyqtSignal, Qt, QWidget, QGridLayout, \
    QHBoxLayout, QVBoxLayout, QSizePolicy

from pluto import isDefault
from pluto.devices.fields.namefield import NameField
from pluto.devices.fields.secondaryinfofield import SecondaryInfoField


class BaseDevice(QWidget):
    DISPLAYNAME = 'BaseDevice'

    secondaryToggled = pyqtSignal(bool)

    aux = 24 * ['']
    def __init__(self, paramDict, model, parent=None):
        super(BaseDevice, self).__init__(parent)

        self.model = model
        self.deviceType = paramDict['deviceType']
        self.subType = paramDict['subType']
        self.register = paramDict['register']
        self.size = paramDict['size']
        self.number = paramDict['number']
        self.name = paramDict['name']
        self.deviceCode = paramDict['deviceCode']

        # apply the entries if loaded from file or read from indexer
        self.params = paramDict.get('params', [])
        self.unit = paramDict.get('unit', '')
        self.firmware = paramDict.get('firmware', '')
        self.names = paramDict.get('names', [])

        self.fields = {}

        self.hLayout = QHBoxLayout()
        self.hLayout.setContentsMargins(0, 0, 0, 0)

        self.nameLayout = QVBoxLayout()
        self.nameLayout.setSpacing(0)
        self.statusLayout = QVBoxLayout()
        self.statusLayout.setSpacing(0)
        self.valueLayout = QGridLayout()
        self.valueLayout.setSpacing(0)

        self._addNameAndInfo()
        self._generateFields(self.register)
        self._insertFields()

        self.hLayout.addLayout(self.nameLayout)
        self.hLayout.addLayout(self.statusLayout)
        self.hLayout.addLayout(self.valueLayout)
        self.hLayout.addSpacing(3)
        self.setLayout(self.hLayout)
        self.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)

    def getDeviceCode(self):
        return self.deviceCode

    @pyqtSlot()
    def updateValues(self):
        for field in self.fields.values():
            field.updateValue(self.model)

    def _addNameAndInfo(self):
        self.nameField = NameField(self.name)

        registerString = '%d (%s)' % (self.model._offset + self.register,
                                      hex(self.model._offset + self.register))
        self.infoField = SecondaryInfoField(str(self.number), self.deviceType,
                                            self.subType[0].DISPLAYNAME,
                                            str(self.register), registerString,
                                            str(self.size))

        self.infoField.hide()

        self.updateInfo()

        self.nameLayout.addWidget(self.nameField, 0, Qt.AlignTop)
        self.nameLayout.addWidget(self.infoField, 0, Qt.AlignTop)

    def hasExtendedStatus(self):
        return self.DISPLAYNAME in self.model.verConstants.EXTENDEDDEVICES

    def _generateFields(self, register):
        pass

    def _insertFields(self):
        '''
            Creates additional fields to display data.
            If the fields need to be updated when the model updates it's
            registers, these fields should be added into self.fields
        '''
        pass

    def updateInfo(self):
        self.nameField.setToolTip('Main: %s<br>'
                                  'Sub: %s<br>'
                                  'Start: %d<br>'
                                  'Register: %d (%s)<br>'
                                  'Size: %d'
                                  % (self.deviceType,
                                     self.subType[0].DISPLAYNAME,
                                     self.register,
                                     self.model._offset + self.register,
                                     hex(self.model._offset + self.register),
                                     self.size))

        self.infoField.updateValues(str(self.number),
                                    self.deviceType,
                                    self.subType[0].DISPLAYNAME,
                                    str(self.register),
                                    '%d (%s)' % (self.model._offset +
                                                 self.register,
                                                 hex(self.model._offset +
                                                     self.register)),
                                    str(self.size))

    def setSecondaryInformation(self, info=None):
        if info:
            self.__dict__.update(info)

        self.nameField.setValue(self.name)

        if self.params:
            self.params.sort()
            self.initiateDeviceParams()

        if self.names:
            self.adjustDeviceNames(self.names)

        if info and info.get('aux'):
            self.fields['status'].setAuxReasons(info['aux'])

        self.adjustUnits()

    def initiateDeviceParams(self):
        pass

    def adjustDeviceNames(self, names):
        pass

    def adjustUnits(self):
        if self.unit:
            # TODO: currently not working for RealValue devices
            # and Vector devices
            if 'current' in self.fields:
                self.fields['current'].setSuffix(self.unit)
            if 'target' in self.fields:
                self.fields['target'].value.setSuffix(self.unit)

    def toggleDebugComponents(self):
        for field in self.fields.values():
            field.toggleDebugComponents()

    def getParamDict(self):
        return {
            'name': self.name,
            'deviceCode': self.deviceCode,
            'deviceType': self.deviceType,
            'subType': self.subType,
            'register': self.register,
            'size': self.size,
            'firmware': self.firmware,
            'unit': self.unit,
            'params': self.getParams(),
            'names': self.getNames(),
            'number': self.number,
            'aux': self.aux
        }

    def getEditDict(self):
        return {
            'name': self.name,
            'subType': self.subType,
            'register': self.register,
            'size': self.size,
            'params': self.getParams(),
            'names': self.getNames()
        }

    def getParams(self):
        return self.params

    def getAuxReasons(self):
        return [aux for aux in self.aux if not isDefault(aux, 'aux')]

    def getNames(self):
        return []

    def adjustStart(self, start):
        self.register = start

    def adjustName(self, name=None):
        if name:
            self.name = name

        self.nameField.setValue(self.name)

    def updateRegisters(self, register):
        pass

    def updateContent(self, editDict):
        currentNames = self.names
        self.__dict__.update(editDict)
        if self.params:
            self.syncParams()
        if self.names:
            self.syncSubs(currentNames)

        self.updateValues()
        self.updateInfo()
        self.adjustName()
        self.updateRegisters(self.register)

    def syncParams(self):
        '''
            Should be overwritten in devices with parameters.
            Logs a warning because 'normal' devices don't have parameters, thus
            this function should never be called.
        '''
        logging.warning('Params changed in %s', self.subType[0].DISPLAYNAME)

    # pylint: disable=unused-argument
    def syncSubs(self, nameList):
        '''
            Should be overwritten in devices with subdevices.(Multidevice)
            Logs a warning because 'normal' devices don't have subdevices, thus
            this function should never be called.
        '''
        logging.warning('Subs changed in %s', self.subType[0].DISPLAYNAME)

    def toggleSecondaryInformation(self, visible):
        self.infoField.setVisible(visible)
        self.secondaryToggled.emit(visible)
