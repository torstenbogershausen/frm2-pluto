# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA


from pluto.devices import BaseDevice
from pluto.devices.fields.currentvaluefield import CurrentValueIntField


class SimpleDiscreteInput64(BaseDevice):
    '''
        N    Current((u)int16)
    '''

    DISPLAYNAME = 'SimpleDiscreteInput64'
    # TODO
    def __init__(self, paramDict, model, signed=True, parent=None):
        super(SimpleDiscreteInput64, self).__init__(paramDict, model, parent)

        self.signed = signed
        if self.signed:
            self.fields['current'].setLineEditSigned()

    def _generateFields(self, register):
        self.fields['current'] = CurrentValueIntField(register)

    def _insertFields(self):
        self.valueLayout.addWidget(self.fields['current'], 0, 0)

    def updateRegisters(self, register):
        self.fields['current'].updateRegister(register)
