# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

"""v2014.0701 only!"""

from pluto import getDefaultSubName
from pluto.devices import BaseDevice
from pluto.devices.fields.cmdfield import CmdField
from pluto.devices.fields.multidevicesfield import MultiDevicesField


class MultiDevice(BaseDevice):
    # TODO: fix editing as soon as it is implemented into the spec
    '''
        N    CurrentD1(float32), low Word
        N+1  CurrentD1(float32), high Word
        N+2  StatusD1
        N+3  CurrentD2(float32), low Word
        N+4  CurrentD2(float32), high Word
        N+5  StatusD2
        ...
        N+3n    CMD(uint16)
        N+3n+1  Answer(float32/uint32), lowWord
        N+3n+2  Answer(float32/uint32), highWord
    '''

    DISPLAYNAME = 'MultiDevice'

    def _generateFields(self, register):
        devices = (self.size - 3) // 3

        # when reading the indexer the names are not provided.
        if not self.names:
            self.names = [getDefaultSubName(i) for i in range(1, devices + 1)]

        self.fields['devices'] = MultiDevicesField(register, devices, self.names)
        self.fields['cmd'] = CmdField(register, self.names,
                                      self.model)

    def _insertFields(self):
        '''
            Adds a MultiDeviceField, containing the subdevices
            and a CmdField to edit the devices' parameters.
        '''

        self.valueLayout.addWidget(self.fields['devices'], 0, 0)
        self.valueLayout.addWidget(self.fields['cmd'], 1, 0)

    def updateRegisters(self, register):
        self.fields['devices'].updateRegister(register)
        self.fields['cmd'].updateRegister(register)

    def getParams(self):
        return self.fields['cmd'].getParams()

    def getNames(self):
        return self.fields['devices'].getNames()

    def adjustDeviceNames(self, names):
        '''
            Sets the names parameter to the provided list and updates
            the subdevices accordingly.
        '''

        self.names = names
        self.fields['devices'].adjustNames(names)
        self.fields['cmd'].setParams(self.params)
        self.fields['cmd'].adjustNames(names)

    def syncSubs(self, nameList):
        currentAmount = len(nameList)
        expectedAmount = len(self.names)

        if currentAmount < expectedAmount:
            for i in range(currentAmount, expectedAmount):
                self.addDevice(i, self.names[i])
        elif currentAmount > expectedAmount:
            for i in range(currentAmount, expectedAmount, -1):
                self.removeDevices(i - 1)

        self.adjustDeviceNames(self.names)

    def addDevice(self, pos, name):
        self.fields['devices'].addDevice(pos, name)

    def removeDevices(self, pos):
        self.fields['devices'].removeDevices(pos)
