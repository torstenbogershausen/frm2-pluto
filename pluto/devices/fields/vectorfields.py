# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
# Copyright (C) 2016 Enrico Faulhaber
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

import logging

from pluto.qt import pyqtSignal, QSize, QWidget, QVBoxLayout, QHBoxLayout

from pluto.devices.fields import BaseField
from pluto.devices.fields.value import ValueFloat
from pluto.devices.fields.currentvaluefield import CurrentValueFloatField


class VectorInputField(BaseField):
    '''
        A field containing a multiple number of devices.
        Each device consists of a  CurrentValueField.
    '''

    namesChanged = pyqtSignal()

    def __init__(self, register, size, names, parent=None):
        super(VectorInputField, self).__init__(register, parent)

        self.mainLayout = QVBoxLayout()
        self.mainLayout.setContentsMargins(0, 0, 0, 0)
        self.mainLayout.setSpacing(0)
        self.setLayout(self.mainLayout)
        self.numberOfDevices = size
        self.devices = []

        self._fill(names)

    def updateValue(self, model):
        '''
            Updates the values of the fields to the current value read by the
            model.
        '''

        for device in self.devices:
            device.updateValue(model)

    def _fill(self, names):
        '''
            Adds 'numberOfDevices' devices to the layout.
        '''

        for i in range(self.numberOfDevices):
            self.addDevice(i, names[i])

    def addDevice(self, position, name=None):
        '''
            Adds a device at the given position into the layout and
            insets name and device in the corresponding lists.
        '''
        subDevice = InputSubdevice(name, self.register + position * 2)
        self.mainLayout.addWidget(subDevice)
        self.devices.insert(position, subDevice)

    def removeDevices(self, row):
        '''
            Removes a device from the layout and any list containing it.
        '''

        self.devices.pop(row)
        self.mainLayout.itemAt(row).deleteLater()

    def adjustNames(self, names):
        '''
            The provided list of names is set to the devices.
            If the list is too short the names are not set, if it is too long,
            the list is shortened to the appropriate number of names and then
            emitted via signal.
        '''

        if len(names) < len(self.devices):
            logging.info('Not enough names for renaming the devices')
            return
        else:
            # make sure the amount of names equals the amount of devices
            names = names[:len(self.devices)]
        for i, device in enumerate(self.devices):
            device.updateName(names[i])

        self.namesChanged.emit()
        self.adjustWidths()

    def getNames(self):
        return [device.name for device in self.devices]

    def adjustWidths(self):
        prefixSize = QSize(0, 0)
        for device in self.devices:
            label = device._currentField.labelPrefix
            if label.sizeHint().width() > prefixSize.width():
                prefixSize = label.sizeHint()

        for device in self.devices:
            device._currentField.labelPrefix.setFixedSize(prefixSize)


class VectorOutputField(VectorInputField):
    def addDevice(self, position, name=None):
        subDevice = OutputSubdevice(name,
                                    self.register + position * 2,
                                    self.register + position * 2 + self.numberOfDevices * 2)
        self.mainLayout.addWidget(subDevice)
        self.devices.insert(position, subDevice)


class InputSubdevice(QWidget):
    def __init__(self, name, register, parent=None):
        super(InputSubdevice, self).__init__(parent)
        self.name = name
        self.register = register
        self.mainLayout = QHBoxLayout()
        self.mainLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.mainLayout)

        self._insertFields()

    def _insertFields(self):
        self._generateFields()
        self.mainLayout.addWidget(self._currentField)

    def _generateFields(self):
        self._currentField = CurrentValueFloatField(self.register,
                                                    self.name)

    def updateValue(self, model):
        self._currentField.updateValue(model)

    def updateName(self, name):
        if name == self.name:
            return
        self._currentField.updatePrefix(name)
        self.name = name


class OutputSubdevice(InputSubdevice):
    def __init__(self, name, register, targetregister, parent=None):
        # targetregister has to be set first
        self.targetregister = targetregister
        InputSubdevice.__init__(self, name, register, parent)

    def _insertFields(self):
        InputSubdevice._insertFields(self)
        self.mainLayout.addWidget(self._targetField)

    def _generateFields(self):
        InputSubdevice._generateFields(self)
        self._targetField = ValueFloat(self.targetregister, 'Target:')

    def updateValue(self, model):
        InputSubdevice.updateValue(self, model)
        self._targetField.updateValue(model)
