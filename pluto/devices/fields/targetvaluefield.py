# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2015 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.qt import uic, pyqtSlot

from pluto import getUiPath
from pluto.devices.fields import BaseField
from pluto.devices.fields.value import ValueFloat, ValueInt


class TargetButtons(BaseField):
    """Just the stop and the Go button"""
    def __init__(self, register, parent=None):
        super(TargetButtons, self).__init__(register, parent)
        uic.loadUi(getUiPath('GoTo.ui'), self)

    @pyqtSlot(bool)
    def setGoButtonState(self, state):
        if state in (True, False):
            self.buttonGo.setEnabled(state)

    @pyqtSlot(bool)
    def setStopButtonState(self, state):
        if state in (True, False):
            self.buttonStop.setEnabled(state)


class TargetValueField(TargetButtons):
    def __init__(self, register, valueField, parent=None):
        super(TargetValueField, self).__init__(register, parent)
        self.value = valueField
        self.layout().insertWidget(0, self.value)
        self.value.setReadOnly(False)
        self.updateLineEdit = True
        self.buttonGo.clicked.connect(self._enableUpdates)
        self.buttonStop.clicked.connect(self._enableUpdates)

    def getValue(self):
        return self.value.getValue()

    @pyqtSlot()
    def _enableUpdates(self):
        self.value.enableUpdates()


class TargetValueFloatField(TargetValueField):
    def __init__(self, register, prefix=None, parent=None):
        super(TargetValueFloatField, self).__init__(register,
                                                    ValueFloat(register, 'Target:', prefix),
                                                    parent)

    def updateValue(self, model):
        self.value.updateValue(model)

class TargetValueFloatField64(TargetValueField):
    def __init__(self, register, prefix=None, parent=None):
        super(TargetValueFloatField64, self).__init__(register,
                                                      ValueFloat(register, 'Target:', prefix),
                                                      parent)

    def updateValue(self, model):
        self.value.updateValue(model)


class TargetValueIntField(TargetValueField):
    def __init__(self, register, prefix=None, signed=True, parent=None):
        super(TargetValueIntField, self).__init__(register,
                                                  ValueInt(register, 'Target:', prefix),
                                                  parent)
        if not signed:
            self.value.setLineEditUnsigned()

    def updateValue(self, model):
        self.value.updateValue(model)

    def setLineEditSigned(self):
        self.value.setLineEditSigned()

    def setLineEditUnSigned(self):
        self.value.setLineEditUnSigned()
