# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2015 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.qt import uic, pyqtSignal, pyqtSlot, QPixmap, QPalette, QColor

from pluto import getUiPath
from pluto.devices.fields import BaseField


class StatusField64(BaseField):
    goEnable = pyqtSignal(bool)
    stopEnable = pyqtSignal(bool)

    def __init__(self, register, parent=None):
        super(StatusField64, self).__init__(register, parent)
        uic.loadUi(getUiPath('Status.ui'), self)
        self.extFrame.hide()
        #self.extRegister = register + 1 if extended else 0

        self.auxReasons = []
        self.status = ''

        # extended panel stuff
        self._labels = [getattr(self, 'label%d' % i) for i in range(12)]
        self._labelsExt = [getattr(self, 'labelExt%d' % i) for i in range(16)]
        self.auxReasons = []
        self.onPalette = self.palette()
        self.offPalette = self.palette()
        c1 = self.onPalette.color(QPalette.Window)
        c2 = self.onPalette.color(QPalette.WindowText)
        # mix fore and background colors to get a diminished color for
        # inactive status bits
        new = QColor(0.5 * c1.red() + 0.5 * c2.red(),
                     0.5 * c1.green() + 0.5 * c2.green(),
                     0.5 * c1.blue() + 0.5 * c2.blue())
        self.offPalette.setColor(QPalette.WindowText, new)

        # hide additional aux word display for device that don't have it
        #if not extended:
        #    self.extended1.hide()
        #    self.extended2.hide()
        #    self.extended3.hide()
        #    self.extended4.hide()
        #    self.extendedLine.hide()

    @pyqtSlot(bool)
    def setExtVisible(self, visible):
        self.extFrame.setVisible(visible)

    def setAuxReasons(self, reasons):
        self.auxReasons = reasons
        for i in range(min(8, len(reasons))):
            self._labels[i].setText(reasons[i])
        for i in range(min(16, len(reasons) - 8)):
            self._labelsExt[i].setText(reasons[i + 8])

    def updateValue(self, model):
        value32 = model.getDWord(self.register)
        # Use value with 16 bits here for compatiblity with existing code
        value = (value32 >> 16 & 0xFF00) | (value32 & 0x00FF)
        statusTuple = model.verConstants.getStatus(value)
        self.status = statusTuple[0]
        self.labelStatus.setPixmap(QPixmap(':/led/%s' % self.status))
        reason = model.verConstants.getReason(value)
        toolTip = '%s' % self.status
        auxErrors = self.extractAuxErrors(value)
        if reason:
            toolTip += '\n%s' % reason

        # if there is an aux bit it will be displayed even if there is no error
        # this is intentional!
        if auxErrors:
            for error in auxErrors:
                toolTip += '\n%s' % error
        self.labelStatus.setToolTip(toolTip)
        self.setStatus(statusTuple[1])

        # extended panel
        self.labelMain.setText(statusTuple[0])
        v2 = format(value >> 8, '08b')
        self.labelBits2.setText(v2[:4] + ' ' + v2[4:])
        v1 = format(value & 0xFF, '08b')
        self.labelBits1.setText(v1[:4] + ' ' + v1[4:])

        # 12 bits from status/command/reason; including 8 aux bits.
        for i in range(12):
            if value & (1 << i):
                self._labels[i].setPalette(self.onPalette)
            else:
                self._labels[i].setPalette(self.offPalette)

        if True: #if self.extRegister
            # get AUX bit 23..16
            value = (value32 >> 8 & 0xFFFF)
            for i in range(16):
                if value & (1 << i):
                    self._labelsExt[i].setPalette(self.onPalette)
                else:
                    self._labelsExt[i].setPalette(self.offPalette)

    def setStatus(self, buttonStates):
        self.buttonReset.setEnabled(buttonStates['reset'])
        self.goEnable.emit(buttonStates['go'])
        self.stopEnable.emit(buttonStates['stop'])

    def getStatus(self):
        return self.status

    def extractAuxErrors(self, value):
        auxErrors = []
        if self.auxReasons:
            for i in range(8):
                if value & (1 << i):
                    auxErrors.append(self.auxReasons[i])

        return auxErrors
