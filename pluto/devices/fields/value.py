# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.qt import uic

from pluto import getUiPath
from pluto.devices.fields import BaseField
from pluto.devices.fields.validators import FloatValidator, Int16Validator, \
    UInt16Validator, Int32Validator, UInt32Validator


class Value(BaseField):
    def __init__(self, register, text, prefix=None, parent=None):
        super(Value, self).__init__(register, parent)
        uic.loadUi(getUiPath('Value.ui'), self)
        self.label.setText(text)
        self.updatePrefix(prefix)

        self.labelSuffix.hide()
        self.updatable = True

    def getValue(self):
        return 0

    def enableUpdates(self):
        self.valueEdit.enableUpdates()

    def setReadOnly(self, readonly):
        self.valueEdit.setReadOnly(readonly)

    def updateLabel(self, text):
        self.label.setText(text)

    def updatePrefix(self, prefix):
        if prefix:
            self.labelPrefix.setText(prefix.strip())
            self.labelPrefix.show()
        else:
            self.labelPrefix.hide()

    def setSuffix(self, suffix):
        if suffix:
            self.labelSuffix.show()
            self.labelSuffix.setText(suffix)
        else:
            self.labelSuffix.hide()


class ValueFloat(Value):
    def __init__(self, register, text, prefix=None, parent=None):
        super(ValueFloat, self).__init__(register, text, prefix, parent)
        self.valueEdit.setValidator(FloatValidator())

    def getValue(self):
        return float(self.valueEdit.text())

    def updateValue(self, model):
        self.valueEdit.updateValue(model.getFloat(self.register))

class ValueFloat64(Value):
    def __init__(self, register, text, prefix=None, parent=None):
        super(ValueFloat64, self).__init__(register, text, prefix, parent)
        self.valueEdit.setValidator(FloatValidator())

    def getValue(self):
        return float(self.valueEdit.text())

    def updateValue(self, model):
        self.valueEdit.updateValue(model.getFloat64(self.register))

class ValueInt(Value):
    def __init__(self, register, text, prefix=None, parent=None):
        super(ValueInt, self).__init__(register, text, prefix, parent)
        self.valueEdit.setValidator(UInt16Validator())
        self.signed = True

    def getValue(self):
        return self.valueEdit.validator().getValue(self.valueEdit.text())

    def updateValue(self, model):
        self.valueEdit.updateValue(model.getWord(self.register, self.signed))

    def setLineEditSigned(self):
        self.signed = True
        self.valueEdit.setValidator(Int16Validator())

    def setLineEditUnsigned(self):
        self.signed = False
        self.valueEdit.setValidator(UInt16Validator())


class ValueInt32(Value):
    def __init__(self, register, text, prefix=None, parent=None):
        super(ValueInt32, self).__init__(register, text, prefix, parent)
        self.valueEdit.setValidator(UInt32Validator())
        self.signed = False

    def getValue(self):
        return self.valueEdit.validator().getValue(self.valueEdit.text())

    def updateValue(self, model):
        self.valueEdit.updateValue(model.getDWord(self.register, self.signed))

    def setLineEditSigned(self):
        self.signed = True
        self.valueEdit.setValidator(Int32Validator())

    def setLineEditUnsigned(self):
        self.signed = False
        self.valueEdit.setValidator(UInt32Validator())
