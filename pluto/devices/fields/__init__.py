# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2015 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA


from pluto.qt import QWidget


class BaseField(QWidget):
    def __init__(self, register, parent=None):
        super(BaseField, self).__init__(parent)
        self.register = register

    def updateValue(self, model):
        pass

    def toggleDebugComponents(self):
        pass

    def updateRegister(self, register):
        self.register = register
