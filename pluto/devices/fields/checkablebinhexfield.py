# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA


from pluto.qt import uic, Qt, pyqtSlot

from pluto import getUiPath
from pluto.devices.fields import BaseField
from pluto.devices.fields.components.valueedit import ValueEdit
from pluto.devices.fields.validators import BinValidator, HexValidator


class CheckableBinHexField(BaseField):
    def __init__(self, register, parent=None):
        super(CheckableBinHexField, self).__init__(register, parent)
        uic.loadUi(getUiPath('ValueWithBin.ui'), self)
        self.valueEditBin = ValueEdit()
        self.valueEditBin.setValidator(BinValidator())
        self.valueEditBin.setReadOnly(True)
        self.mainLayout.insertWidget(2, self.valueEditBin)
        self.valueEditDefault.setValidator(HexValidator())
        self.valueEditBin.textEdited.connect(self._updatePair)
        self.valueEditDefault.textEdited.connect(self._updatePair)
        self.checkBox.stateChanged.connect(self._updateState)

    def updateValue(self, model):
        if (self.valueEditDefault.updatable and
                self.valueEditBin.updatable):
            value = model.getWord(self.register)
            self.valueEditDefault.updateValue(hex(value))
            self.valueEditBin.updateValue(bin(value))

    @pyqtSlot('QString')
    def _updatePair(self, text):
        sender = self.sender()
        text = text[2:]
        if sender == self.valueEditBin:
            if text == '':
                self.valueEditDefault.setText(0)
            else:
                self.valueEditDefault.setText('%#06X' % int(text, 2))
        elif sender == self.valueEditDefault:
            if text == '':
                self.valueEditBin.setText(0)
            else:
                self.valueEditBin.setText(format(int(text, 16), '#018b'))

    @pyqtSlot(int)
    def _updateState(self, state):
        if state == Qt.Checked:
            self.checkBox.setText('Target:')
            self.buttonSet.setEnabled(True)
            self.valueEditBin.setReadOnly(False)
            self.valueEditDefault.setReadOnly(False)
        else:
            self.checkBox.setText('Value:')
            self.buttonSet.setEnabled(False)
            self.valueEditBin.setReadOnly(True)
            self.valueEditBin.enableUpdates()
            self.valueEditDefault.setReadOnly(True)
            self.valueEditDefault.enableUpdates()

    def getValue(self):
        return int(self.valueEditDefault.text(), 16)
