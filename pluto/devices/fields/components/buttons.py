# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.qt import QSize, QPushButton, QIcon


class ButtonBase(QPushButton):
    def __init__(self, parent=None):
        super(ButtonBase, self).__init__(parent)
        self.setFixedSize(29, 29)


class SetToButton(ButtonBase):
    def __init__(self, parent=None):
        super(SetToButton, self).__init__(parent)
        self.setIcon(QIcon(':/buttons/set'))
        self.setToolTip('Set value')


class GoButton(ButtonBase):
    def __init__(self, parent=None):
        super(GoButton, self).__init__(parent)
        self.setIcon(QIcon(':/buttons/go'))
        self.setIconSize(QSize(20, 20))
        self.setToolTip('Start')


class StopButton(ButtonBase):
    def __init__(self, parent=None):
        super(StopButton, self).__init__(parent)
        self.setIcon(QIcon(':/buttons/stop'))
        self.setIconSize(QSize(20, 20))
        self.setToolTip('Stop')


class ResetButton(ButtonBase):
    def __init__(self, parent=None):
        super(ResetButton, self).__init__(parent)
        self.setIcon(QIcon(':/buttons/reset'))
        self.setToolTip('Reset')


class RefreshButton(ButtonBase):
    def __init__(self, parent=None):
        super(RefreshButton, self).__init__(parent)
        self.setIcon(QIcon(':/buttons/refresh'))
        self.setToolTip('Refresh')


class DebugButton(ButtonBase):
    def __init__(self, parent=None):
        super(DebugButton, self).__init__(parent)
        self.setIcon(QIcon(':/buttons/debug'))
        self.setToolTip('Show debug dialog')
