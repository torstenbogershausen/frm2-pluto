# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA


from pluto.qt import Qt, pyqtSlot, QLineEdit, QPalette


class ValueEdit(QLineEdit):
    DEFAULT = 0
    EDITED = 1
    INVALID = 2

    def __init__(self, parent=None):
        super(ValueEdit, self).__init__(parent)

        self.palettes = self.generatePalettes()
        self.setAlignment(Qt.AlignRight)

        self.updatable = True
        self.state = self.DEFAULT
        self.suffix = ''

        self.textEdited.connect(self.setDisplayState)

    def generatePalettes(self):
        palettes = {}

        palette = QPalette()
        palette.setColor(QPalette.Text, Qt.black)
        palettes[self.DEFAULT] = palette

        palette = QPalette()
        palette.setColor(QPalette.Text, Qt.blue)
        palettes[self.EDITED] = palette

        palette = QPalette()
        palette.setColor(QPalette.Text, Qt.red)
        palettes[self.INVALID] = palette

        return palettes

    def updateValue(self, value):
        if self.hasFocus():
            self.updatable = False
        elif not self.isModified():
            self.updatable = True

        if self.updatable:

            self.setText(str(value))

    def getValue(self):
        return self.validator().getValue(self.text()) if self.validator() \
            else float(self.text())

    def setSuffix(self, suffix):
        self.suffix = suffix

    @pyqtSlot()
    def enableUpdates(self):
        self.updatable = True
        self.setModified(False)
        self.setDisplayState(self.DEFAULT)

    def text(self):
        return QLineEdit.text(self) or '0'

    @pyqtSlot()
    def setDisplayState(self, state=1):
        """
        Sets the state of the lineedit and the textcolor to reflect that.
        :param state: oneof(DEFAULT, EDITED, INVALID) by default: EDITED
        :return:
        """

        if state not in [self.DEFAULT, self.EDITED, self.INVALID]:
            return

        if self.state == self.EDITED and state == self.INVALID:
            return

        self.state = state

        self.setPalette(self.palettes[self.state])

    def setText(self, value):
        try:
            ## TODO Need to add uint64 here
            if not self.validator():
                strvalue = str(value)
            elif self.validator().VALIDATION == 'uint32':
                strvalue = str(int(value))
            else:
                strvalue = '%g' % value
        except TypeError:
            strvalue = str(value)

        if self.validator() and not self.validator().validate(strvalue, 0)[0]:
            strvalue = '(%s)' % strvalue

        QLineEdit.setText(self, strvalue)
