# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.qt import uic

from pluto import getUiPath
from pluto.devices.fields import BaseField
from pluto.devices.fields.validators import FloatValidator, UInt32Validator


class MultiParamField(BaseField):
    DO_READ = 1
    DO_WRITE = 2

    def __init__(self, register, verConstants, parent=None):
        super(MultiParamField, self).__init__(register, parent)
        uic.loadUi(getUiPath('MultiParamField.ui'), self)

        self.fullParamList = verConstants.parameters
        self.validationList = {key: value[1] for key, value
                               in verConstants.paramIndices.items()}

        self.comboBox.currentIndexChanged.connect(self.adjustValidator)
        self.comboBox.currentIndexChanged.connect(self.valueEdit.enableUpdates)

    def adjustValidator(self):
        key = self.comboBox.currentText()

        if not key in self.validationList:
            self.valueEdit.setValidator(None)
            return

        if self.validationList[key] == 'uint':
            self.valueEdit.setValidator(UInt32Validator())
        elif self.validationList[key] == 'float':
            self.valueEdit.setValidator(FloatValidator())
        else: # remove Validator
            self.valueEdit.setValidator(None)

    def setParams(self, params):
        self.comboBox.clear()
        params = sorted(set(params[0]))
        for param in params:
            try:
                self.comboBox.addItem(self.fullParamList[param])
            except KeyError:
                self.comboBox.addItem(str(param))

        self.adjustValidator()

    def removeParam(self, position):
        self.comboBox.removeItem(position)

    def updateValue(self, model):
        self.valueEdit.updateValue(model.getFloat(self.register +
                                                  self.getAdditionalOffset()))

    def getAdditionalOffset(self):
        return max(0, self.comboBox.currentIndex() * 2)

    def getSetRegister(self):
        return self.register + max(0, self.comboBox.currentIndex() * 2)

    def getValue(self):
        if self.valueEdit.validator():
            validation = self.valueEdit.validator().VALIDATION
        else:
            validation = ''

        value = self.valueEdit.getValue()
        self.valueEdit.enableUpdates()

        return validation, value
