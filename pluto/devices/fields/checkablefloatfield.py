# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.qt import uic, Qt, pyqtSlot

from pluto import getUiPath
from pluto.devices.fields import BaseField
from pluto.devices.fields.validators import FloatValidator


class CheckableFloatField(BaseField):
    def __init__(self, register, parent=None):
        super(CheckableFloatField, self).__init__(register, parent)
        uic.loadUi(getUiPath('ValueWithBin.ui'), self)
        self.valueEditDefault.setValidator(FloatValidator())
        self.checkBox.stateChanged.connect(self._updateState)

    def updateValue(self, model):
        if not self.checkBox.isChecked() \
           and not self.valueEditDefault.hasFocus():
            self.valueEditDefault.updateValue(round(model.getFloat(self.register), 4))

    @pyqtSlot(int)
    def _updateState(self, state):
        if state == Qt.Checked:
            self.checkBox.setText('Target:')
            self.buttonSet.setEnabled(True)
            self.valueEditDefault.setReadOnly(False)
        else:
            self.checkBox.setText('Value:')
            self.buttonSet.setEnabled(False)
            self.valueEditDefault.setReadOnly(True)
            self.valueEditDefault.enableUpdates()

    def getValue(self):
        return float(self.valueEditDefault.text())
