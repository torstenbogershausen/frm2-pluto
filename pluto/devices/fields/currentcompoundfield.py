# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.qt import pyqtSlot, QGridLayout, QFont

from pluto.devices.fields import BaseField
from pluto.devices.fields.value import ValueFloat


class CurrentCompoundField(BaseField):
    def __init__(self, register, components, parent=None):
        super(CurrentCompoundField, self).__init__(register, parent)
        self.fields = []

        self.fill(components)
        self.adjustLabelSize()

    def updateValue(self, model):
        for field in self.fields:
            field.updateValue(model)

    @pyqtSlot(int)
    def updateMode(self, modus):
        for i in range(len(self.fields)):
            if i == modus:
                self.highlightLabel(i)
            else:
                self.normalizeLabel(i)

    def highlightLabel(self, fieldindex):
        field = self.fields[fieldindex]
        font = QFont()
        font.setBold(True)
        field.label.setFont(font)
        field.labelPrefix.setFont(font)

    def normalizeLabel(self, fieldindex):
        field = self.fields[fieldindex]
        font = QFont()
        font.setBold(False)
        field.label.setFont(font)
        field.labelPrefix.setFont(font)

    def fill(self, components):
        self.mainLayout = QGridLayout()
        self.mainLayout.setContentsMargins(0, 0, 0, 0)

        for i, component in enumerate(components):
            field = ValueFloat(self.register + 2 * i, component)

            # insert in 2 columns of devices
            self.mainLayout.addWidget(field, i % 2, i // 2)

            self.fields.append(field)

        self.setLayout(self.mainLayout)

        for field in self.fields:
            field.setReadOnly(True)

    def adjustLabelSize(self):
        width = 0
        for field in self.fields:
            if field.label.width() > width:
                width = field.label.width()

        for field in self.fields:
            field.label.setMinimumWidth(width)
