# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.devices.fields.value import ValueFloat, ValueFloat64, ValueInt, ValueInt32


class CurrentValueFloatField(ValueFloat):
    def __init__(self, register, prefix=None, parent=None):
        super(CurrentValueFloatField, self).__init__(register, 'Value:',
                                                     prefix, parent)
        self.setReadOnly(True)

    def updateValue(self, model):
        self.valueEdit.updateValue(model.getFloat(self.register))

class CurrentValueFloatField64(ValueFloat64):
    def __init__(self, register, prefix=None, parent=None):
        super(CurrentValueFloatField64, self).__init__(register, 'Value:',
                                                       prefix, parent)
        self.setReadOnly(True)

    def updateValue(self, model):
        self.valueEdit.updateValue(model.getFloat64(self.register))


class CurrentValueIntField(ValueInt):
    def __init__(self, register, prefix=None, signed=True, fmtstr='%d', parent=None):
        super(CurrentValueIntField, self).__init__(register, 'Value:',
                                                   prefix, parent)
        self.setReadOnly(True)
        self.signed = signed
        self.fmtstr = fmtstr

    def updateValue(self, model):
        value = model.getWord(self.register, self.signed)
        self.valueEdit.updateValue(self.fmtstr % int(value))


class CurrentValueInt32Field(ValueInt32):
    def __init__(self, register, prefix=None, signed=True, fmtstr='%d', parent=None):
        super(CurrentValueInt32Field, self).__init__(register, 'Value:',
                                                     prefix, parent)
        self.setReadOnly(True)
        self.signed = signed
        self.fmtstr = fmtstr

    def updateValue(self, model):
        value = model.getDWord(self.register)
        self.valueEdit.updateValue(self.fmtstr % int(value))
