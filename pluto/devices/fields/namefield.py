# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.qt import uic, Qt, pyqtSignal, QWidget

from pluto import getUiPath


class NameField(QWidget):
    nameChanged = pyqtSignal()

    def __init__(self, name, parent=None):
        super(NameField, self).__init__(parent)
        uic.loadUi(getUiPath('Name.ui'), self)
        self.setValue(name)

    def setValue(self, name):
        self.labelName.setText(name)
        self.nameChanged.emit()

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton \
           and self.labelEdit.mapFromParent(event.pos()) in self.labelEdit.rect():
            self.parent().parent().parent().startEditContent()

        event.accept()
