# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2015 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.qt import uic

from pluto import getUiPath
from pluto.devices.fields import BaseField


class SecondaryInfoField(BaseField):
    def __init__(self, number, deviceType, subType, start, register, size, parent=None):
        super(SecondaryInfoField, self).__init__(register, parent)
        uic.loadUi(getUiPath('SecondaryInfoField.ui'), self)
        self.updateValues(number, deviceType, subType, start, register, size)

    def updateValues(self, number, deviceType, subType, start, register, size):
        self.labelNumber.setText(number)
        self.labelMain.setText(deviceType)
        self.labelSub.setText(subType)
        self.labelStart.setText(start)
        self.labelRegister.setText(register)
        self.labelSize.setText(size)
