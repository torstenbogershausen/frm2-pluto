# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.qt import QRegExp, Qt, QRegExpValidator, QIntValidator, QValidator


class ValidationError(Exception):
    def __init__(self, validation):
        Exception.__init__(self)
        self.validation = validation

    def __str__(self):
        return ('No rule for %s-validation' % self.validation)


class BinValidator(QRegExpValidator):
    VALIDATION = 'bin'

    def __init__(self, parent=None):
        super(BinValidator, self).__init__(parent)
        self.setRegExp(QRegExp('0b[01]{,16}', Qt.CaseInsensitive))

    def getValue(self, text):
        return text


class CharValidator(QRegExpValidator):
    VALIDATION = 'char'

    def __init__(self, parent=None):
        super(CharValidator, self).__init__(parent)
        self.setRegExp((QRegExp('[A-Za-z0-9]{,4}')))

    def getValue(self, text):
        return text


class FloatValidator(QRegExpValidator):
    VALIDATION = 'float'

    def __init__(self, parent=None):
        super(FloatValidator, self).__init__(parent)
        # pylint: disable=anomalous-backslash-in-string
        self.setRegExp(QRegExp('[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?',
                               Qt.CaseInsensitive))

    def getValue(self, text):
        return float(text)

class FloatValidator64(QRegExpValidator):
    VALIDATION = 'double'

    def __init__(self, parent=None):
        super(FloatValidator64, self).__init__(parent)
        # pylint: disable=anomalous-backslash-in-string
        self.setRegExp(QRegExp('[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?',
                               Qt.CaseInsensitive))

    def getValue(self, text):
        return double(text)

class HexValidator(QRegExpValidator):
    VALIDATION = 'hex'

    def __init__(self, parent=None):
        super(HexValidator, self).__init__(parent)
        self.setRegExp(QRegExp('0x[0-9A-F]{,4}', Qt.CaseInsensitive))

    def getValue(self, text):
        return text


class Int16Validator(QIntValidator):
    VALIDATION = 'int16'

    def __init__(self, parent=None):
        super(Int16Validator, self).__init__(parent)
        self.setRange(-0x8000, 0x7FFF)  # 16Bit

    def getValue(self, text):
        return int(text) & 0xFFFF


class UInt16Validator(QIntValidator):
    VALIDATION = 'uint16'

    def __init__(self, parent=None):
        super(UInt16Validator, self).__init__(parent)
        self.setRange(0, 0xFFFF)  # 16Bit

    def getValue(self, text):
        return int(text)


class Int32Validator(QValidator):
    VALIDATION = 'int32'

    def validate(self, text, pos=0):
        try:
            if text == '':
                return (QValidator.Intermediate, text, pos)
            elif -0x80000000 <= int(text) <= 0x7FFFFFFF:
                return (QValidator.Acceptable, text, pos)
            return (QValidator.Invalid, text, pos)
        except ValueError:
            return (QValidator.Invalid, text, pos)

    def getValue(self, text):
        return int(float(text))


class UInt32Validator(QValidator):
    VALIDATION = 'uint32'

    def validate(self, text, pos=0):
        try:
            if text == '':
                return (QValidator.Intermediate, text, pos)
            elif 0 <= int(text) <= 0xFFFFFFFF:
                return (QValidator.Acceptable, text, pos)
            return (QValidator.Invalid, text, pos)
        except ValueError:
            return (QValidator.Invalid, text, pos)

    def getValue(self, text):
        return int(float(text))

class Int64Validator(QValidator):
    VALIDATION = 'int64'

    def validate(self, text, pos=0):
        try:
            if text == '':
                return (QValidator.Intermediate, text, pos)
            elif -0x8000000000000000 <= int(text) <= 0x7FFFFFFFFFFFFFFF:
                return (QValidator.Acceptable, text, pos)
            return (QValidator.Invalid, text, pos)
        except ValueError:
            return (QValidator.Invalid, text, pos)

    def getValue(self, text):
        return int(float(text))


class UInt64Validator(QValidator):
    VALIDATION = 'uint64'

    def validate(self, text, pos=0):
        try:
            if text == '':
                return (QValidator.Intermediate, text, pos)
            elif 0 <= int(text) <= 0xFFFFFFFFFFFFFFFF:
                return (QValidator.Acceptable, text, pos)
            return (QValidator.Invalid, text, pos)
        except ValueError:
            return (QValidator.Invalid, text, pos)

    def getValue(self, text):
        return int(float(text))


class DeviceNameValidator(QRegExpValidator):
    VALIDATION = 'device'

    def __init__(self, parent=None):
        super(DeviceNameValidator, self).__init__(parent)
        self.setRegExp(QRegExp(r'[a-zA-Z_][0-9a-zA-Z_]*'))


class AsciiValidator(QRegExpValidator):
    VALIDATION = 'ascii'

    def __init__(self, parent=None):
        super(AsciiValidator, self).__init__(parent)
        self.setRegExp(QRegExp(r'[ -~]+'))
