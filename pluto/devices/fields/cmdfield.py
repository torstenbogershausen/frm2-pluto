# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2015 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA


import logging

from pluto.qt import uic

from pluto import getUiPath
from pluto.devices.fields import BaseField
from pluto.devices.fields.validators import FloatValidator, UInt32Validator, \
    ValidationError
from pluto.gui.dialogs.statemachinedebugdialog import StatemachineDebugDialog


class CmdField(BaseField):
    NOT_INITIALIZED = 0
    DO_READ = 1
    DO_WRITE = 2
    BUSY = 3
    DONE = 4
    ERROR_NO_INDEX = 5
    ERROR_READONLY = 6
    ERROR_RETRY_LATER = 7

    def __init__(self, register, subs, model, params=None, parent=None, is64bit=False):
        super(CmdField, self).__init__(register, parent)
        uic.loadUi(getUiPath('CmdField.ui'), self)
        self.model = model
        self.is64bit = is64bit
        self.indices = model.verConstants.paramIndices
        self.parameters = model.verConstants.parameters
        self.deviceParameters = {}
        self.currentParams = []
        self.lastRequest = 0

        self.floatValidator = FloatValidator()
        self.int32Validator = UInt32Validator()

        if not subs:
            subs = ['Default']
            self.comboBoxDev.hide()

        self.comboBoxDev.addItems(subs)
        for sub in subs:
            self.deviceParameters[sub] = [self.generateParamDict(param) for
                                          param in self.parameters]

        self.debugButton.hide()

        self.updateParamBox()
        self.adjustComponents()

        self.comboBoxDev.currentIndexChanged.connect(self.updateParamBox)
        self.comboBoxParam.currentIndexChanged.connect(self.adjustComponents)
        self.comboBoxParam.currentIndexChanged.connect(self.requestValue)
        self.setButton.clicked.connect(self.writeRequest)
        self.refreshButton.clicked.connect(self.requestValue)
        self.debugButton.clicked.connect(self.openDebugDialog)

    def setParams(self, params):
        '''
            Generates a paramDict for every Device in the comboBoxDev,
            if the same amount of parameter lists is provided.
        '''

        if not len(params) == self.comboBoxDev.count():
            logging.error('Paramlists amount does not match number of devices')
            return

        for i, paramlist in enumerate(params):
            device = self.comboBoxDev.itemText(i)
            self.deviceParameters[device] = [self.generateParamDict(param) for
                                             param in paramlist]

        self.updateParamBox()
        self.requestValue()

    def getParams(self):
        devices = [self.comboBoxDev.itemText(i)
                   for i in range(self.comboBoxDev.count())]
        paramList = []
        for device in devices:
            paramList.append([param['value'] for param in self.deviceParameters[device]])
        return paramList

    def adjustNames(self, names):
        newDevParams = {}
        for i, name in enumerate(names):
            newDevParams[name] = self.deviceParameters[self.comboBoxDev.itemText(i)]

        self.deviceParameters = newDevParams
        self.comboBoxDev.clear()
        self.comboBoxDev.addItems(names)

    def updateParamBox(self):
        '''
            Sets the contents of the comboBoxParams to the parameters of the
            currently selected device.
        '''

        if self.comboBoxDev.count() == 0:
            return
        curDev = self.comboBoxDev.currentText()
        self.currentParams = self.deviceParameters[curDev]
        parameters = [param['name'] for param in self.deviceParameters[curDev]]
        self.comboBoxParam.clear()
        self.comboBoxParam.addItems(parameters)

    def adjustComponents(self):
        '''
            Enables and disables the buttons and valueEdit of the cmd-field
            to match the requirements/permissions for the currently displayed
            parameter.
        '''

        if self.comboBoxParam.count() == 0:
            return

        parameter = self.currentParams[self.comboBoxParam.currentIndex()]

        self.setButton.setEnabled(parameter['rw'])
        self.valueEdit.setReadOnly(parameter['rw'])

        self.setValidator(parameter['type'])

    def setValidator(self, valType):
        if valType == 'float':
            self.valueEdit.setValidator(self.floatValidator)
        elif valType == 'uint':
            self.valueEdit.setValidator(self.int32Validator)

    def requestValue(self):
        '''
            Write a DOREAD command to the PLC.
        '''

        self.valueEdit.enableUpdates()

        device = self.comboBoxDev.currentIndex()
        paramIndex = self.comboBoxParam.currentIndex()
        if device == -1 or paramIndex == -1:
            return
        param = self.currentParams[paramIndex]['value']

        # don't try to read if it's a special function
        if self.model.verConstants.isFunctionNoWriteValue(param):
            return

        try:
            self.lastRequest = (device << 8) + param
            self.model.writeWord((self.DO_READ << 13) + self.lastRequest,
                                 self.register)
        except IndexError:
            pass

    def writeRequest(self):
        '''
            Write a DOWRITE command to the PLC
        '''
        device = self.comboBoxDev.currentIndex()
        paramIndex = self.comboBoxParam.currentIndex()
        if device == -1 or paramIndex == -1:
            return

        req = (device << 8) + (self.currentParams[paramIndex]['value'])
        cmd = (self.DO_WRITE << 13) + req
        value = self.valueEdit.getValue()
        self.valueEdit.enableUpdates()

        validation = self.valueEdit.validator().VALIDATION

        if validation == 'uint32':
            if self.is64bit:
                self.model.writeWordQWord(cmd, value, self.register)
            else:
                self.model.writeWordDWord(cmd, value, self.register)
        elif validation == 'float':
            if self.is64bit:
                self.model.writeWordDouble(cmd, value, self.register)
            else:
                self.model.writeWordFloat(cmd, value, self.register)
        else:
            raise ValidationError(validation)

    def updateValue(self, model=None):
        '''
            update the lineEdit to display the current value of the Parameterregister
        '''

        if not model:
            model = self.model

        try:
            validation = self.valueEdit.validator().VALIDATION
        except AttributeError:
            return

        if validation == 'uint32':
            if self.is64bit:
                rawValues = model.getWordQWord(self.register)
            else:
                rawValues = model.getWordDWord(self.register)
        elif validation == 'float':
            if self.is64bit:
                rawValues = model.getWordDouble(self.register)
            else:
                rawValues = model.getWordFloat(self.register)
        else:
            raise ValidationError(validation)

        if not rawValues[0]:
            return

        status = rawValues[0] >> 13
        question = rawValues[0] & 0b1111111111111
        parameter = rawValues[0] & 0b11111111
        paramIndex = self.comboBoxParam.currentIndex()
        if paramIndex != -1:
            paramNoCombo = self.currentParams[paramIndex]['value']
            if parameter != paramNoCombo:
                return

        if status == self.NOT_INITIALIZED:
            self.valueEdit.setToolTip('NOT_INITIALIZED')
            self.valueEdit.setReadOnly(True)
            self.setButton.setEnabled(False)
        elif status == self.DO_READ:
            self.valueEdit.setToolTip('DO_READ')
            self.valueEdit.setReadOnly(True)
            self.setButton.setEnabled(False)
        elif status == self.DO_WRITE:
            self.valueEdit.setToolTip('DO_WRITE')
            self.valueEdit.setReadOnly(True)
            self.setButton.setEnabled(False)
        elif status == self.BUSY:
            self.valueEdit.setToolTip('BUSY')
            self.valueEdit.setReadOnly(True)
            self.setButton.setEnabled(False)
        elif status == self.DONE:
            self.valueEdit.setToolTip('DONE')
            self.valueEdit.setReadOnly(False)
            self.setButton.setEnabled(True)
        elif status == self.ERROR_NO_INDEX:
            self.valueEdit.setToolTip('ERROR_NO_INDEX')
            self.setButton.setEnabled(True)
            self.valueEdit.setDisplayState(self.valueEdit.INVALID)
            self.valueEdit.setText('-')
            return
        elif status == self.ERROR_READONLY:
            self.valueEdit.setToolTip('ERROR_READONLY')
            self.valueEdit.setReadOnly(True)
            self.setButton.setEnabled(False)
            if (self.currentParams[self.comboBoxParam.currentIndex()]['value']
                    != parameter):
                return
        elif status == self.ERROR_RETRY_LATER:
            self.valueEdit.setToolTip('ERROR_RETRY_LATER')
            self.valueEdit.setDisplayState(self.valueEdit.INVALID)
            self.valueEdit.setText('-')
            self.setButton.setEnabled(True)
            return

        if self.lastRequest and not self.model.verConstants.isFunctionNoWriteValue(question):
            if question != self.lastRequest:
                self.valueEdit.setDisplayState(self.valueEdit.INVALID)
        else:
            self.valueEdit.setDisplayState(self.valueEdit.DEFAULT)

        self.valueEdit.updateValue(rawValues[1])

    # Currently unused.
    def removeParameter(self, device, parameter):
        i = 0
        paramList = self.deviceParameters[device]
        for i, param in enumerate(paramList):
            if param['value'] == parameter:
                paramList.pop(i)
                break
        else:
            return

        if self.comboBoxDev.currentText() == device:
            self.comboBoxParam.removeItem(i)

    # Currently unused.
    def makeParamReadOnly(self, device, parameter):
        paramList = self.deviceParameters[device]
        for param in paramList:
            if param['value'] == parameter:
                param['rw'] = False
                if param['name'] == self.comboBoxParam.currentText():
                    self.adjustComponents()
                break

    def generateParamDict(self, param):
        try:
            name = self.parameters[param]
            return {
                'name': name,
                'type': self.indices[name][1],
                'rw': True,
                'value': self.indices[name][0]
            }

        except KeyError:
            return {
                'name': str(param),
                'type': 'float' if param >= 30 else 'uint',
                'rw': True,
                'value': param
            }

    def toggleDebugComponents(self):
        self.debugButton.setVisible(not self.debugButton.isVisible())

    def openDebugDialog(self):
        debugDialog = StatemachineDebugDialog(self.model, self.register, self)
        # TODO: show device name in window title
        # debugDialog.setWindowTitle(self.name)
        debugDialog.show()
