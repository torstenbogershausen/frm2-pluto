# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

"""v2014.0701 only!"""

from pluto.qt import Qt

from pluto.devices import BaseDevice
from pluto.devices.fields.cmdfield import CmdField
from pluto.devices.fields.compoundstatusfield import CompoundStatusField
from pluto.devices.fields.currentcompoundfield import CurrentCompoundField


class BaseCompoundDevice(BaseDevice):
    def __init__(self, paramDict, model, parent=None):
        super(BaseCompoundDevice, self).__init__(paramDict, model, parent)

        self.secondaryToggled.connect(self.fields['status'].setExtVisible)
        self.fields['status'].currentMode.connect(self.fields['current'].updateMode)
        self.statusOffset = 0

    def _generateCompoundFields(self, register, subdevices):
        self.statusOffset = len(subdevices) * 2

        self.fields['current'] = CurrentCompoundField(register, subdevices)
        self.fields['status'] = CompoundStatusField(register + self.statusOffset)
        self.fields['cmd'] = CmdField(self.fields['status'].register + 1,
                                      subdevices, self.model)

    def _insertFields(self):
        self.statusLayout.addWidget(self.fields['status'], 0, Qt.AlignTop)
        self.valueLayout.addWidget(self.fields['current'], 0, 0)
        self.valueLayout.addWidget(self.fields['cmd'], 1, 0)

    def updateRegisters(self, register):
        self.fields['current'].updateRegister(register)
        self.fields['status'].updateRegister(register + self.statusOffset)
        self.fields['cmd'].updateRegister(register + self.statusOffset + 1)

    def getParams(self):
        return self.fields['cmd'].getParams()


class PIDController(BaseCompoundDevice):
    '''
        N    Current Heater(float32), low Word
        N+1  Current Heater(float32), high Word
        N+2  Current Temp(float32), low Word
        N+3  Current Temp(float32), high Word
        N+4  Status
        N+5  CMD(uint16)
        N+6  Answer(float32/uint32), lowWord
        N+7  Answer(float32/uint32), highWord
    '''

    DISPLAYNAME = 'PIDController'

    def _generateFields(self, register):
        BaseCompoundDevice._generateCompoundFields(self, register,
                                                   ['Heizer', 'Temperatur'])

    def getNames(self):
        return ['Heizer', 'Temperatur']


class PIDControllerRamped(BaseCompoundDevice):
    '''
        N    Current Heater(float32), low Word
        N+1  Current Heater(float32), high Word
        N+2  Current Temp(float32), low Word
        N+3  Current Temp(float32), high Word
        N+4  Current Setpoint(float32), low Word
        N+5  Current Setpoint(float32), high Word
        N+6  Status
        N+7  CMD(uint16)
        N+8  Answer(float32/uint32), lowWord
        N+9  Answer(float32/uint32), highWord
    '''

    DISPLAYNAME = 'PIDControllerRamped'

    def _generateFields(self, register):
        BaseCompoundDevice._generateCompoundFields(self, register,
                                                   ['Heizer', 'Temperatur',
                                                    'Setpoint'])

    def getNames(self):
        return['Heizer', 'Temperatur', 'Setpoint']
