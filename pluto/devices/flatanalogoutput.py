# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.qt import pyqtSlot, Qt

from pluto.devices import BaseDevice
from pluto.devices.fields.currentvaluefield import CurrentValueFloatField
from pluto.devices.fields.multiparamfield import MultiParamField
from pluto.devices.fields.statusfield import StatusField
from pluto.devices.fields.targetvaluefield import TargetValueFloatField
from pluto.devices.fields.validators import ValidationError


class FlatAnalogOutput(BaseDevice):
    '''
        N    Current(flaot32), low World
        N+1  Current(float32), high Word
        N+2  Target(float32), low Word
        N+3  Target(float32), high Word
        N+4  Status
        N+5  Parameter1, low World
        N+6  Parameter1, high Word
        ...
        N+2M+3 ParameterM, low World
        N+2M+4 ParameterM, high Word
    '''

    DISPLAYNAME = 'FlatAnalogOutput'

    def __init__(self, paramDict, model, parent=None):
        super(FlatAnalogOutput, self).__init__(paramDict, model, parent)

        self.secondaryToggled.connect(self.fields['status'].setExtVisible)
        self.fields['target'].buttonGo.clicked.connect(self._go)
        self.fields['target'].buttonStop.clicked.connect(self._stop)
        self.fields['multiparam'].setButton.clicked.connect(self.setParameter)
        self.fields['multiparam'].comboBox.currentIndexChanged.connect(self.updateValues)
        self.fields['status'].buttonReset.clicked.connect(self._reset)
        self.fields['status'].goEnable.connect(self.fields['target'].setGoButtonState)
        self.fields['status'].stopEnable.connect(self.fields['target'].setStopButtonState)

    def _generateFields(self, register):
        self.fields['current'] = CurrentValueFloatField(register)
        self.fields['target'] = TargetValueFloatField(register + 2)
        self.fields['status'] = StatusField(register + 4,
                                            self.hasExtendedStatus())
        self.fields['multiparam'] = MultiParamField(register + 5,
                                                    self.model.verConstants)

    def _insertFields(self):
        self.statusLayout.addWidget(self.fields['status'], 0, Qt.AlignTop)
        self.valueLayout.addWidget(self.fields['current'], 0, 0)
        self.valueLayout.addWidget(self.fields['target'], 0, 1)
        self.valueLayout.addWidget(self.fields['multiparam'], 1, 0, 1, 2)

    def updateRegisters(self, register):
        self.fields['current'].updateRegister(register)
        self.fields['target'].updateRegister(register + 2)
        self.fields['status'].updateRegister(register + 4)
        self.fields['multiparam'].updateRegister(register + 5)

    def getParams(self):
        return self.params

    def initiateDeviceParams(self):
        self.fields['multiparam'].setParams(self.params)

    @pyqtSlot()
    def setParameter(self):
        validation, value = self.fields['multiparam'].getValue()
        register = self.fields['multiparam'].getSetRegister()
        # Interpret as float unless one of the following occures.
        if validation == 'uint32':
            self.model.writeDWord(value, register)
        elif validation == 'float':
            self.model.writeFloat(value, register)
        else:
            raise ValidationError(validation)

    @pyqtSlot()
    def _go(self):
        self.model.writeFloatWord(self.fields['target'].getValue(),
                                  self.model.verConstants.GO_CMD,
                                  self.fields['target'].register)

    @pyqtSlot()
    def _stop(self):
        self.model.writeWord(self.model.verConstants.STOP_CMD,
                             self.fields['status'].register)

    @pyqtSlot()
    def _reset(self):
        self.model.writeWord(self.model.verConstants.RESET_CMD,
                             self.fields['status'].register)

    def getStatus(self):
        return self.fields['status'].status

    def syncParams(self):
        self.fields['multiparam'].setParams(self.params)


class FlatOutput_201502(FlatAnalogOutput):
    '''
        N    Current(flaot32), low World
        N+1  Current(float32), high Word
        N+2  Target(float32), low Word
        N+3  Target(float32), high Word
        N+4  Status
        N+5  AuxStatus
        N+6  Parameter1, low World
        N+7  Parameter1, high Word
        ...
        N+2M+4 ParameterM, low World
        N+2M+5 ParameterM, high Word
    '''

    DISPLAYNAME = 'FlatOutput'

    def _generateFields(self, register):
        '''
            Register order for v2015.02.
        '''

        self.fields['current'] = CurrentValueFloatField(register)
        self.fields['target'] = TargetValueFloatField(register + 2)
        self.fields['status'] = StatusField(register + 4,
                                            self.hasExtendedStatus())
        self.fields['multiparam'] = MultiParamField(register + 6,
                                                    self.model.verConstants)

    def updateRegisters(self, register):
        self.fields['current'].updateRegister(register)
        self.fields['target'].updateRegister(register + 2)
        self.fields['status'].updateRegister(register + 4)
        self.fields['multiparam'].updateRegister(register + 6)
