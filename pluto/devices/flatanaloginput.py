# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.qt import pyqtSlot, Qt

from pluto.devices import BaseDevice
from pluto.devices.fields.currentvaluefield import CurrentValueFloatField
from pluto.devices.fields.multiparamfield import MultiParamField
from pluto.devices.fields.statusfield import StatusField
from pluto.devices.fields.validators import ValidationError


class FlatAnalogInput(BaseDevice):
    '''
        N    Current(flaot32), low World
        N+1  Current(float32), high Word
        N+2  Status
        N+3  Extended Status
        N+4  Parameter1, low World
        N+5  Parameter1, high Word
        ...
        N+2M+2 ParameterM, low World
        N+2M+2 ParameterM, high Word
    '''

    DISPLAYNAME = 'FlatAnalogInput'

    def __init__(self, paramDict, model, parent=None):
        super(FlatAnalogInput, self).__init__(paramDict, model, parent)

        self.secondaryToggled.connect(self.fields['status'].setExtVisible)
        self.fields['multiparam'].setButton.clicked.connect(self.setParameter)
        self.fields['multiparam'].comboBox.currentIndexChanged.connect(self.updateValues)
        self.fields['status'].buttonReset.clicked.connect(self._reset)

    def _generateFields(self, register):
        self.fields['current'] = CurrentValueFloatField(register)
        self.fields['status'] = StatusField(register + 2,
                                            self.hasExtendedStatus())
        self.fields['multiparam'] = MultiParamField(register + 3,
                                                    self.model.verConstants)

    def _insertFields(self):
        self.statusLayout.addWidget(self.fields['status'], 0, Qt.AlignTop)
        self.valueLayout.addWidget(self.fields['current'], 0, 0)
        self.valueLayout.addWidget(self.fields['multiparam'], 1, 0)

    def updateRegisters(self, register):
        self.fields['current'].updateRegister(register)
        self.fields['status'].updateRegister(register + 2)
        self.fields['multiparam'].updateRegister(register + 3)

    def getParams(self):
        return self.params

    def initiateDeviceParams(self):
        self.fields['multiparam'].setParams(self.params)

    @pyqtSlot()
    def setParameter(self):
        validation, value = self.fields['multiparam'].getValue()

        if validation == 'uint32':
            self.model.writeDWord(value,
                                  self.fields['multiparam'].getSetRegister())
        elif validation == 'float':
            self.model.writeFloat(value,
                                  self.fields['multiparam'].getSetRegister())
        else:
            raise ValidationError(validation)

    @pyqtSlot()
    def _reset(self):
        self.model.writeWord(self.model.verConstants.RESET_CMD,
                             self.fields['status'].register)

    def syncParams(self):
        self.fields['multiparam'].setParams(self.params)


class FlatInput_201502(FlatAnalogInput):
    '''
        N    Current(flaot32), low World
        N+1  Current(float32), high Word
        N+2  Status
        N+3  Aux Status
        N+4  Parameter1, low World
        N+5  Parameter1, high Word
        ...
        N+2M+2 ParameterM, low World
        N+2M+2 ParameterM, high Word
    '''

    DISPLAYNAME = 'FlatInput'

    def _generateFields(self, register):
        '''
            Register order for v2015.02.
        '''

        self.fields['current'] = CurrentValueFloatField(register)
        self.fields['status'] = StatusField(register + 2,
                                            self.hasExtendedStatus())
        self.fields['multiparam'] = MultiParamField(register + 4,
                                                    self.model.verConstants)

    def updateRegisters(self, register):
        self.fields['current'].updateRegister(register)
        self.fields['status'].updateRegister(register + 2)
        self.fields['multiparam'].updateRegister(register + 4)
