# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.qt import pyqtSlot, Qt

from pluto.devices import BaseDevice
from pluto.devices.fields.cmdfield import CmdField
from pluto.devices.fields.currentvaluefield import CurrentValueFloatField
from pluto.devices.fields.statusfield import StatusField


class ParamInput(BaseDevice):
    # TODO: adjust editing of the parameters as soon as it is added to the spec
    '''
        N    Current(float32), low Word
        N+1  Current(float32), high Word
        N+2  Status
        N+3  CMD(uint16)
        N+4  Answer(float32/uint32), lowWord
        N+5  Answer(float32/uint32), highWord
    '''

    DISPLAYNAME = 'ParamInput'

    def __init__(self, paramDict, model, parent=None):
        super(ParamInput, self).__init__(paramDict, model, parent)
        self.secondaryToggled.connect(self.fields['status'].setExtVisible)
        self.fields['status'].buttonReset.clicked.connect(self._reset)

    def _generateFields(self, register):
        self.fields['current'] = CurrentValueFloatField(register)
        self.fields['status'] = StatusField(register + 2,
                                            self.hasExtendedStatus())
        self.fields['cmd'] = CmdField(register + 3, [], self.model)

    def _insertFields(self):
        self.statusLayout.addWidget(self.fields['status'], 0, Qt.AlignTop)
        self.valueLayout.addWidget(self.fields['current'], 0, 0)
        self.valueLayout.addWidget(self.fields['cmd'], 1, 0)

    def updateRegisters(self, register):
        self.fields['current'].updateRegister(register)
        self.fields['status'].updateRegister(register + 2)
        self.fields['cmd'].updateRegister(register + 3)

    def getParams(self):
        return self.fields['cmd'].getParams()

    def initiateDeviceParams(self):
        self.fields['cmd'].setParams(self.params)

    @pyqtSlot()
    def _reset(self):
        self.model.writeWord(self.model.verConstants.RESET_CMD,
                             self.fields['status'].register)


class ReadableInterface(ParamInput):
    '''
        ParamInput of 2014.0701
    '''

    DISPLAYNAME = 'ReadableInterface'
