# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.qt import pyqtSlot, Qt

from pluto.devices import BaseDevice
from pluto.devices.fields.cmdfield import CmdField
from pluto.devices.fields.vectorfields import VectorInputField, \
    VectorOutputField

from pluto.devices.fields.statusfield import StatusField
from pluto.devices.fields.targetvaluefield import TargetButtons


class DataBytes(BaseDevice):
    '''
        N    	BIT 15 = 1 : PLC has written data
                       = 0 : Host has written data
                BIT  14..9 : reserved, always 0
                BIT   8..0 : Number of valid bytes (up to 510 in a 0x05FF)
                State machine:
                             N = 0 : Idle
                                     Host writes data into N+1....
                                     (example: Host has written 32 bytes
                                     Host updates N and need to wait.
                             N = 0000 0000 0010 0000b
                                     PLC reads and process the data
                                     After that, 2 possible state transitions:
                                     a) PLC sets N to 0, host may send next data
                                     b) PLC answers with e.g. 4 bytes
                                       PLC writes answer into N+1, N+2
                                       PLC updates N
                             N = 1000 0000 0000 0100b
                                     PLC waits
                                     Hosts reads and processes the data
                                     When done, hosts sets N = Idle
                             N = 0: Idle

 
        N+1  data byte     0,1
        N+2  data byte     2,3
        N+3  data byte     4,5
        ...
    '''

    DISPLAYNAME = 'DataBytes'

    def __init__(self, paramDict, model, parent=None):
        super(DataBytes, self).__init__(paramDict, model, parent)

        #self.secondaryToggled.connect(self.fields['status'].setExtVisible)
        #self.fields['status'].buttonReset.clicked.connect(self._reset)

    def _generateFields(self, register):
        devices = (self.size - 4) // 2

        # when reading the indexer the names are not provided.
        #if not self.names:
        #    self.names = ['Ch%d' % i for i in range(1, devices + 1)]

        #self.fields['devices'] = VectorInputField(register,
        #                                          devices, self.names)
        #self.fields['cmd'] = CmdField(register + devices * 2 + 1,
        #                              ['ALL'] + self.names, self.model)
        self.fields['status'] = StatusField(register, 0)
        #                                    # self.hasExtendedStatus())
        #self.numberOfDevices = devices

    def _insertFields(self):
        '''
            Adds a MultiDeviceField, containing the subdevices
            and a CmdField to edit the devices' parameters.
        '''
        self.statusLayout.addWidget(self.fields['status'], 0, Qt.AlignTop)
        #self.valueLayout.addWidget(self.fields['devices'], 0, 0)
        #self.valueLayout.addWidget(self.fields['cmd'], 1, 0)

    def updateRegisters(self, register):
        self.fields['status'].updateRegister(register)

    def getParams(self):
        return [] #self.fields['cmd'].getParams()

    def getNames(self):
        return [] #return self.fields['devices'].getNames()

    def adjustDeviceNames(self, names):
        '''
            Sets the names parameter to the provided list and updates
            the subdevices accordingly.
        '''

        self.names = names
        self.fields['devices'].adjustNames(names)
        self.fields['cmd'].setParams(self.params * (1 + self.numberOfDevices))
        self.fields['cmd'].adjustNames(['ALL'] + names)

    def syncSubs(self, nameList):
        currentAmount = len(nameList)
        expectedAmount = len(self.names)

        if currentAmount < expectedAmount:
            for i in range(currentAmount, expectedAmount):
                self.addDevice(i, self.names[i])
        elif currentAmount > expectedAmount:
            for i in range(currentAmount, expectedAmount, -1):
                self.removeDevices(i - 1)

        self.adjustDeviceNames(self.names)

    def addDevice(self, pos, name):
        self.fields['devices'].addDevice(pos, name)

    def removeDevices(self, pos):
        self.fields['devices'].removeDevices(pos)

    @pyqtSlot()
    def _reset(self):
        dummy = 1
        #self.model.writeWord(self.model.verConstants.RESET_CMD,
        #                     self.fields['status'].register)

    def getStatus(self):
        return "" ##return self.fields['status'].status

