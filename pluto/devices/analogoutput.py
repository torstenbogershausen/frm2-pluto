# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.qt import pyqtSlot, Qt

from pluto.devices import BaseDevice
from pluto.devices.fields.currentvaluefield import CurrentValueFloatField
from pluto.devices.fields.statusfield import StatusField
from pluto.devices.fields.targetvaluefield import TargetValueFloatField


class AnalogOutput(BaseDevice):
    '''
        N    Current(float32), low Word
        N+1  Current(float32), high Word
        N+2  Target(float32), low Word
        N+3  Target(float32), high Word
        N+4  Status
    '''

    DISPLAYNAME = 'AnalogOutput'

    def __init__(self, paramDict, model, parent=None):
        super(AnalogOutput, self).__init__(paramDict, model, parent)

        self.secondaryToggled.connect(self.fields['status'].setExtVisible)
        self.fields['target'].buttonGo.clicked.connect(self._go)
        self.fields['target'].buttonStop.clicked.connect(self._stop)
        self.fields['status'].buttonReset.clicked.connect(self._reset)
        self.fields['status'].goEnable.connect(self.fields['target'].setGoButtonState)
        self.fields['status'].stopEnable.connect(self.fields['target'].setStopButtonState)

    def _generateFields(self, register):
        self.fields['current'] = CurrentValueFloatField(register)
        self.fields['target'] = TargetValueFloatField(register + 2)
        self.fields['status'] = StatusField(register + 4,
                                            self.hasExtendedStatus())

    def _insertFields(self):
        self.statusLayout.addWidget(self.fields['status'], 0, Qt.AlignTop)
        self.valueLayout.addWidget(self.fields['current'], 0, 0)
        self.valueLayout.addWidget(self.fields['target'], 0, 1)

    def updateRegisters(self, register):
        self.fields['current'].updateRegister(register)
        self.fields['target'].updateRegister(register + 2)
        self.fields['status'].updateRegister(register + 4)

    @pyqtSlot()
    def _go(self):
        self.model.writeFloatWord(self.fields['target'].getValue(),
                                  self.model.verConstants.GO_CMD,
                                  self.fields['target'].register)

    @pyqtSlot()
    def _stop(self):
        self.model.writeWord(self.model.verConstants.STOP_CMD,
                             self.fields['status'].register)

    @pyqtSlot()
    def _reset(self):
        self.model.writeWord(self.model.verConstants.RESET_CMD,
                             self.fields['status'].register)

    def getStatus(self):
        return self.fields['status'].status
