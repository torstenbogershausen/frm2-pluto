# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
# Copyright (C) 2016 Enrico Faulhaber
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.qt import pyqtSlot, Qt

from pluto.devices import BaseDevice
from pluto.devices.fields.cmdfield import CmdField
from pluto.devices.fields.vectorfields import VectorInputField, \
    VectorOutputField

from pluto.devices.fields.statusfield import StatusField
from pluto.devices.fields.targetvaluefield import TargetButtons


class VectorInput(BaseDevice):
    '''
        N    CurrentD1(float32), low Word
        N+1  CurrentD1(float32), high Word
        N+2  CurrentD2(float32), low Word
        N+3  CurrentD2(float32), high Word
        ...
        N+2n    Status
        N+2n+1  CMD(uint16)
        N+2n+2  Answer(float32/uint32), lowWord
        N+2n+3  Answer(float32/uint32), highWord
    '''

    DISPLAYNAME = 'VectorInput'

    def __init__(self, paramDict, model, parent=None):
        super(VectorInput, self).__init__(paramDict, model, parent)

        self.secondaryToggled.connect(self.fields['status'].setExtVisible)
        self.fields['status'].buttonReset.clicked.connect(self._reset)

    def _generateFields(self, register):
        devices = (self.size - 4) // 2

        # when reading the indexer the names are not provided.
        if not self.names:
            self.names = ['Ch%d' % i for i in range(1, devices + 1)]

        self.fields['devices'] = VectorInputField(register,
                                                  devices, self.names)
        self.fields['cmd'] = CmdField(register + devices * 2 + 1,
                                      ['ALL'] + self.names, self.model)
        self.fields['status'] = StatusField(register + devices * 2,
                                            self.hasExtendedStatus())
        self.numberOfDevices = devices

    def _insertFields(self):
        '''
            Adds a MultiDeviceField, containing the subdevices
            and a CmdField to edit the devices' parameters.
        '''
        self.statusLayout.addWidget(self.fields['status'], 0, Qt.AlignTop)
        self.valueLayout.addWidget(self.fields['devices'], 0, 0)
        self.valueLayout.addWidget(self.fields['cmd'], 1, 0)

    def updateRegisters(self, register):
        self.fields['devices'].updateRegister(register)
        self.fields['cmd'].updateRegister(register +
                                          self.numberOfDevices * 2 + 1)
        self.fields['status'].updateRegister(register +
                                             self.numberOfDevices * 2)

    def getParams(self):
        return self.fields['cmd'].getParams()

    def getNames(self):
        return self.fields['devices'].getNames()

    def adjustDeviceNames(self, names):
        '''
            Sets the names parameter to the provided list and updates
            the subdevices accordingly.
        '''

        self.names = names
        self.fields['devices'].adjustNames(names)
        self.fields['cmd'].setParams(self.params * (1 + self.numberOfDevices))
        self.fields['cmd'].adjustNames(['ALL'] + names)

    def syncSubs(self, nameList):
        currentAmount = len(nameList)
        expectedAmount = len(self.names)

        if currentAmount < expectedAmount:
            for i in range(currentAmount, expectedAmount):
                self.addDevice(i, self.names[i])
        elif currentAmount > expectedAmount:
            for i in range(currentAmount, expectedAmount, -1):
                self.removeDevices(i - 1)

        self.adjustDeviceNames(self.names)

    def addDevice(self, pos, name):
        self.fields['devices'].addDevice(pos, name)

    def removeDevices(self, pos):
        self.fields['devices'].removeDevices(pos)

    @pyqtSlot()
    def _reset(self):
        self.model.writeWord(self.model.verConstants.RESET_CMD,
                             self.fields['status'].register)

    def getStatus(self):
        return self.fields['status'].status


class VectorOutput(BaseDevice):
    # TODO: fix editing as soon as it is implemented into the spec
    '''
        N    CurrentD1(float32), low Word
        N+1  CurrentD1(float32), high Word
        N+2  CurrentD2(float32), low Word
        N+3  CurrentD2(float32), high Word
        ...
        N+2n    TargetD1(float32), low Word
        N+2n+1  TargetD1(float32), high Word
        N+2n+2  TargetD2(float32), low Word
        N+2n+3  TargetD2(float32), high Word
        ...
        N+4n    Status
        N+4n+1  CMD(uint16)
        N+4n+2  Answer(float32/uint32), lowWord
        N+4n+3  Answer(float32/uint32), highWord
    '''

    DISPLAYNAME = 'VectorOutput'

    def __init__(self, paramDict, model, parent=None):
        super(VectorOutput, self).__init__(paramDict, model, parent)
        self.secondaryToggled.connect(self.fields['status'].setExtVisible)

        # button presses
        self.fields['status'].buttonReset.clicked.connect(self._reset)
        self.fields['buttons'].buttonGo.clicked.connect(self._go)
        self.fields['buttons'].buttonStop.clicked.connect(self._stop)

        # enable/disable signals for go/stop
        self.fields['status'].goEnable.connect(
            self.fields['buttons'].setGoButtonState)
        self.fields['status'].stopEnable.connect(
            self.fields['buttons'].setStopButtonState)

    @pyqtSlot()
    def _go(self):
        # get targets
        targets = [dev._targetField.getValue()
                   for dev in self.fields['devices'].devices]
        # send to PLC
        self.model.writeMultipleFloat(targets,
                                      self.register + self.numberOfDevices * 2)
        self.model.writeWord(self.model.verConstants.GO_CMD,
                             self.fields['status'].register)

    @pyqtSlot()
    def _stop(self):
        self.model.writeWord(self.model.verConstants.STOP_CMD,
                             self.fields['status'].register)

    @pyqtSlot()
    def _reset(self):
        self.model.writeWord(self.model.verConstants.RESET_CMD,
                             self.fields['status'].register)

    def _generateFields(self, register):
        devices = (self.size - 4) // 4

        # when reading the indexer the names are not provided.
        if not self.names:
            self.names = ['Ch%d' % i for i in range(1, devices + 1)]

        self.fields['devices'] = VectorOutputField(register,
                                                   devices, self.names)
        self.fields['cmd'] = CmdField(register + devices * 4 + 1,
                                      ['ALL'] + self.names, self.model)
        self.fields['buttons'] = TargetButtons(register + devices * 4)
        self.fields['status'] = StatusField(register + devices * 4,
                                            self.hasExtendedStatus())

        self.numberOfDevices = devices

    def _insertFields(self):
        '''
            Adds a MultiDeviceField, containing the subdevices
            and a CmdField to edit the devices' parameters.
        '''
        self.statusLayout.addWidget(self.fields['status'], 0, Qt.AlignTop)
        self.valueLayout.addWidget(self.fields['devices'], 0, 0)
        self.valueLayout.addWidget(self.fields['cmd'], 1, 0)
        self.valueLayout.addWidget(self.fields['buttons'], 0, 1)

    def updateRegisters(self, register):
        self.fields['devices'].updateRegister(register)
        self.fields['cmd'].updateRegister(register +
                                          self.numberOfDevices * 4 + 1)
        self.fields['status'].updateRegister(register +
                                             self.numberOfDevices * 4)

    def getParams(self):
        return self.fields['cmd'].getParams()

    def getNames(self):
        return self.fields['devices'].getNames()

    def adjustDeviceNames(self, names):
        '''
            Sets the names parameter to the provided list and updates
            the subdevices accordingly.
        '''

        self.names = names
        self.fields['devices'].adjustNames(names)
        # first entry is for the whole device, further are per channel
        self.fields['cmd'].adjustNames(['ALL'] + names)
        self.fields['cmd'].setParams(self.params * (1 + self.numberOfDevices))

    def syncSubs(self, nameList):
        currentAmount = len(nameList)
        expectedAmount = len(self.names)

        if currentAmount < expectedAmount:
            for i in range(currentAmount, expectedAmount):
                self.addDevice(i, self.names[i])
        elif currentAmount > expectedAmount:
            for i in range(currentAmount, expectedAmount, -1):
                self.removeDevices(i - 1)

        self.adjustDeviceNames(self.names)

    def addDevice(self, pos, name):
        self.fields['devices'].addDevice(pos, name)

    def removeDevices(self, pos):
        self.fields['devices'].removeDevices(pos)

    def getStatus(self):
        return self.fields['status'].status
