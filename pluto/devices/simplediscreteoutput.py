# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.qt import pyqtSlot

from pluto.devices import BaseDevice
from pluto.devices.fields.currentvaluefield import CurrentValueIntField
from pluto.devices.fields.targetvaluefield import TargetValueIntField


class SimpleDiscreteOutput(BaseDevice):
    '''
        N    Current((u)int16)
        N+1  Target((u)int16)
    '''

    DISPLAYNAME = 'SimpleDiscreteOutput'

    def __init__(self, paramDict, model, signed=True, parent=None):
        super(SimpleDiscreteOutput, self).__init__(paramDict, model, parent)

        self.signed = signed
        if signed:
            self.fields['current'].setLineEditSigned()
            self.fields['target'].setLineEditSigned()

        self.fields['target'].buttonGo.clicked.connect(self._go)
        self.fields['target'].buttonStop.clicked.connect(self._stop)

    @pyqtSlot()
    def updateValues(self):
        current = self.model.getWord(self.fields['current'].register,
                                     self.signed)
        target = self.model.getWord(self.fields['target'].register,
                                    self.signed)

        if current != target:
            self._setGoEnabled(False)
            self._setStopEnabled()
        else:
            self._setGoEnabled()
            self._setStopEnabled(False)

        BaseDevice.updateValues(self)

    def _generateFields(self, register):
        self.fields['current'] = CurrentValueIntField(register)
        self.fields['target'] = TargetValueIntField(register + 1)

    def _insertFields(self):
        self.valueLayout.addWidget(self.fields['current'], 0, 0)
        self.valueLayout.addWidget(self.fields['target'], 0, 1)

    def updateRegisters(self, register):
        self.fields['current'].updateRegister(register)
        self.fields['target'].updateRegister(register + 1)

    @pyqtSlot()
    def _go(self):
        self.model.writeWord(self.fields['target'].getValue(),
                             self.fields['target'].register,
                             self.signed)

    @pyqtSlot()
    def _stop(self):
        self.model.writeWord(self.fields['current'].getValue(),
                             self.fields['target'].register,
                             self.signed)

    def _setGoEnabled(self, state=True):
        if state in (True, False):
            self.fields['target'].buttonGo.setEnabled(state)

    def _setStopEnabled(self, state=True):
        if state in (True, False):
            self.fields['target'].buttonStop.setEnabled(state)
