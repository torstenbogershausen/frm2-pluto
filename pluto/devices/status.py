# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.qt import Qt, QSpacerItem, QSizePolicy

from pluto.devices import BaseDevice
from pluto.devices.fields.statusfield import StatusField


class Status(BaseDevice):
    '''
        N    Status
    '''

    DISPLAYNAME = 'Status'

    def __init__(self, paramDict, model, parent=None):
        super(Status, self).__init__(paramDict, model, parent)

        self.secondaryToggled.connect(self.fields['status'].setExtVisible)
        self.fields['status'].buttonReset.clicked.connect(self._reset)

    def _generateFields(self, register):
        self.fields['status'] = StatusField(register,
                                            self.hasExtendedStatus())

    def _insertFields(self):
        self.statusLayout.addWidget(self.fields['status'], 0, Qt.AlignTop)
        self.valueLayout.addItem(QSpacerItem(0, 0, QSizePolicy.Expanding,
                                             QSizePolicy.Minimum), 0, 0)

    def updateRegisters(self, register):
        self.fields['status'].updateRegister(register)

    def _reset(self):
        self.model.writeWord(0, self.fields['status'].register)

    def getStatus(self):
        return self.fields['status'].status
