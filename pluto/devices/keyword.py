# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.qt import pyqtSlot

from pluto.devices import BaseDevice
from pluto.devices.fields.checkablebinhexfield import CheckableBinHexField
from pluto.devices.fields.components.buttons import ResetButton


class Keyword(BaseDevice):
    '''
        N    Keyword
    '''

    DISPLAYNAME = 'Keyword'

    def __init__(self, paramDict, model, parent=None):
        super(Keyword, self).__init__(paramDict, model, parent)
        self.fields['binhex'].buttonSet.clicked.connect(self.executeAction)

    @pyqtSlot()
    def executeAction(self):
        if self.sender() == self._resetButton:
            self.model.writeWord(0, self.register)
        else:
            self.model.writeWord(self.fields['binhex'].getValue(),
                                 self.fields['binhex'].register)

        self.fields['binhex'].checkBox.setChecked(False)

    def _generateFields(self, register):
        self.fields['binhex'] = CheckableBinHexField(register)

    def _insertFields(self):
        self._resetButton = ResetButton()
        self._resetButton.setToolTip('Reset value to 0')
        self._resetButton.clicked.connect(self.executeAction)
        self.fields['binhex'].layout().addWidget(self._resetButton)

        self.valueLayout.addWidget(self.fields['binhex'], 0, 0)

    def updateRegisters(self, register):
        self.fields['binhex'].updateRegister(register)
