# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.qt import pyqtSlot, Qt

from pluto.devices import BaseDevice
from pluto.devices.fields.cmdfield import CmdField
from pluto.devices.fields.currentvaluefield import CurrentValueFloatField, CurrentValueFloatField64
from pluto.devices.fields.currentvaluefield import CurrentValueIntField, CurrentValueInt32Field
from pluto.devices.fields.statusfield import StatusField
from pluto.devices.fields.statusfield64 import StatusField64
from pluto.devices.fields.targetvaluefield import TargetValueFloatField, TargetValueFloatField64


class ParamOutput(BaseDevice):
    # TODO: adjust editing of the parameters as soon as it is added to the spec
    '''
        N    Current(float32), low Word
        N+1  Current(float32), high Word
        N+2  Target(float32), low Word
        N+3  Target(float32), high Word
        N+4  Status
        N+5  CMD(uint16)
        N+6  Answer(float32/uint32), lowWord
        N+7  Answer(float32/uint32), highWord
    '''

    DISPLAYNAME = 'ParamOutput'

    def __init__(self, paramDict, model, parent=None):
        super(ParamOutput, self).__init__(paramDict, model, parent)

        self.secondaryToggled.connect(self.fields['status'].setExtVisible)
        self.fields['target'].buttonGo.clicked.connect(self._go)
        self.fields['target'].buttonStop.clicked.connect(self._stop)
        self.fields['status'].buttonReset.clicked.connect(self._reset)
        self.fields['status'].goEnable.connect(self.fields['target'].setGoButtonState)
        self.fields['status'].stopEnable.connect(self.fields['target'].setStopButtonState)

    def _generateFields(self, register):
        self.fields['current'] = CurrentValueFloatField(register)
        self.fields['target'] = TargetValueFloatField(register + 2)
        self.fields['status'] = StatusField(register + 4,
                                            self.hasExtendedStatus())
        self.fields['cmd'] = CmdField(register + 5, [], self.model)

    def _insertFields(self):
        self.statusLayout.addWidget(self.fields['status'], 0, Qt.AlignTop)
        self.valueLayout.addWidget(self.fields['current'], 0, 0)
        self.valueLayout.addWidget(self.fields['target'], 0, 1)
        self.valueLayout.addWidget(self.fields['cmd'], 1, 0, 1, 2)

    def updateRegisters(self, register):
        self.fields['current'].updateRegister(register)
        self.fields['target'].updateRegister(register + 2)
        self.fields['status'].updateRegister(register + 4)
        self.fields['cmd'].updateRegister(register + 5)

    def getParams(self):
        return self.fields['cmd'].getParams()

    def initiateDeviceParams(self):
        self.fields['cmd'].setParams(self.params)

    @pyqtSlot()
    def _go(self):
        self.model.writeFloatWord(self.fields['target'].getValue(),
                                  self.model.verConstants.GO_CMD,
                                  self.fields['target'].register)

    @pyqtSlot()
    def _stop(self):
        self.model.writeWord(self.model.verConstants.STOP_CMD,
                             self.fields['status'].register)

    @pyqtSlot()
    def _reset(self):
        self.model.writeWord(self.model.verConstants.RESET_CMD,
                             self.fields['status'].register)

    def getStatus(self):
        return self.fields['status'].status

class ParamOutput64(BaseDevice):
    '''
        Offset       RegNo
        0.. 7          0            Current(double64)
        8..15          4            Target(double64)
       16              8            AUX bits 7..0
       17                           AUX bits 15..8
       18              9            AUX bits 23..16
       19                           Status/Reason
       20..21        0xA            ErrorID
       22..23        0xB            ParamControl
       24..31                       ParamValue double64/uint64
    '''

    DISPLAYNAME = 'ParamOutput64'

    def __init__(self, paramDict, model, parent=None):
        super(ParamOutput64, self).__init__(paramDict, model, parent)

        self.secondaryToggled.connect(self.fields['status'].setExtVisible)
        self.fields['target'].buttonGo.clicked.connect(self._go)
        self.fields['target'].buttonStop.clicked.connect(self._stop)
        self.fields['status'].buttonReset.clicked.connect(self._reset)
        self.fields['status'].goEnable.connect(self.fields['target'].setGoButtonState)
        self.fields['status'].stopEnable.connect(self.fields['target'].setStopButtonState)

    def _generateFields(self, register):
        self.fields['current'] = CurrentValueFloatField64(register)
        self.fields['target'] = TargetValueFloatField64(register + 4)
        self.fields['status'] = StatusField64(register + 8)
        self.fields['cmd'] = CmdField(register + 0xB, [], self.model, is64bit=True)
        self.fields['nErrId'] = CurrentValueIntField(register + 0xA, 'nErrId', signed=False, fmtstr='0x%04x')

    def _insertFields(self):
        self.statusLayout.addWidget(self.fields['status'], 0, Qt.AlignTop)
        self.valueLayout.addWidget(self.fields['current'], 0, 0)
        self.valueLayout.addWidget(self.fields['target'], 0, 1)
        self.valueLayout.addWidget(self.fields['nErrId'], 1, 0, 1, 2)
        self.valueLayout.addWidget(self.fields['cmd'], 2, 0, 1, 2)

    def updateRegisters(self, register):
        self.fields['current'].updateRegister(register)
        self.fields['target'].updateRegister(register + 4)
        self.fields['status'].updateRegister(register + 8)
        self.fields['cmd'].updateRegister(register + 0xB)

    def getParams(self):
        return self.fields['cmd'].getParams()

    def initiateDeviceParams(self):
        self.fields['cmd'].setParams(self.params)

    @pyqtSlot()
    def _go(self):
        self.model.writeDoubleDWord(self.fields['target'].getValue(),
                                    self.model.verConstants.GO_CMD << 16,
                                    self.fields['target'].register)

    @pyqtSlot()
    def _stop(self):
        self.model.writeDWord(self.model.verConstants.STOP_CMD << 16,
                              self.fields['status'].register)

    @pyqtSlot()
    def _reset(self):
        self.model.writeDWord(self.model.verConstants.RESET_CMD << 16,
                              self.fields['status'].register)

    def getStatus(self):
        return self.fields['status'].status


class MovableInterface(ParamOutput):
    '''
        ParamOutput of 2014.0701
    '''

    DISPLAYNAME = 'MovableInterface'


class ESSMotor(ParamOutput):
    '''
        [ParamOutput]
        nErrid (uint32)
        actSpeed (float32)
    '''

    DISPLAYNAME = 'ESSMotor'

    def _generateFields(self, register):
        super(ESSMotor, self)._generateFields(register)

        self.fields['nErrId'] = CurrentValueIntField(register + 8, 'nErrId', signed=False, fmtstr='0x%04x')
        self.fields['actSpeed'] = CurrentValueFloatField(register + 10, 'ActSpeed')

    def _insertFields(self):
        self.statusLayout.addWidget(self.fields['status'], 0, Qt.AlignTop)
        self.valueLayout.addWidget(self.fields['current'], 0, 0)
        self.valueLayout.addWidget(self.fields['target'], 0, 1)
        self.valueLayout.addWidget(self.fields['nErrId'], 1, 0, 1, 2)
        self.valueLayout.addWidget(self.fields['actSpeed'], 2, 0, 1, 2)
        self.valueLayout.addWidget(self.fields['cmd'], 3, 0, 1, 2)

    def updateRegisters(self, register):
        super(ESSMotor, self).updateRegisters(register)
        self.fields['nErrId'].updateRegister(register + 8)
        self.fields['actSpeed'].updateRegister(register + 10)
