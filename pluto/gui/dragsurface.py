# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.qt import uic, pyqtSignal, pyqtSlot, Qt, QMimeData, QWidget, \
    QFrame, QDrag, QApplication, QMenu, QAction

from pluto import getUiPath
from pluto.devices import BaseDevice
from pluto.gui.dialogs.editdevicedialogs import DeleteDeviceDialog


class Surface(QWidget):
    draggedChild = pyqtSignal(QWidget)
    editContent = pyqtSignal(BaseDevice)
    resized = pyqtSignal()

    def __init__(self, parent=None):
        super(Surface, self).__init__(parent)
        uic.loadUi(getUiPath('Surface.ui'), self)
        self.setAcceptDrops(True)
        self.columns = []

    def addColumn(self, column, side=None):
        column.enteredNewColumn.connect(self.removeEmptyColumns)
#         column.draggedChild.connect(self.draggedChild)
        column.editContent.connect(self.editContent)
        if not self.columns:
            self.mainLayout.addWidget(column)
            self.columns.append(column)
        elif side == 'right':
            self.mainLayout.addWidget(Separator())
            self.mainLayout.addWidget(column)
            self.columns.append(column)
        elif side == 'left':
            self.mainLayout.insertWidget(0, Separator())
            self.mainLayout.insertWidget(0, column)
            self.columns.insert(0, column)

        column.resized.connect(self.resized)

    def addChild(self, child):
        column = self.findFirstColumn()

        column.addChild(child)

        child.deleteChild.connect(column.deleteChild)

    def findFirstColumn(self):
        for i in range(self.mainLayout.count()):
            widget = self.mainLayout.itemAt(i).widget()
            if isinstance(widget, Column):
                return widget
        # if no column was found
        column = Column()
        self.addColumn(column)

        return column

    def dragEnterEvent(self, event):
        if isinstance(event.mimeData(), ChildMimeData):
            child = event.mimeData().getChild()
            if event.pos().x() < self.width() * 0.1 \
                    and len(self.columns[0].getChildren()) > 1:
                self.addColumn(Column([child], self), 'left')
            elif event.pos().x() > self.width() * 0.9 \
                    and len(self.columns[-1].getChildren()) > 1:
                self.addColumn(Column([child]), 'right')

            self.removeEmptyColumns()

    def removeEmptyColumns(self):
        for column in reversed(self.columns):
            if column.isEmpty():
                self.removeColumn(column)

    def removeColumn(self, column):
        self.columns.remove(column)
        layoutIndex = self.mainLayout.indexOf(column)

        self.mainLayout.takeAt(layoutIndex).widget().deleteLater()
        try:
            self.mainLayout.takeAt(layoutIndex).widget().deleteLater()
        except AttributeError:
            # If the rightmost column was removed there is no widget at
            # layoutIndex!
            layoutItem = self.mainLayout.takeAt(layoutIndex - 1)

            if layoutItem:
                layoutItem.widget().deleteLater()

    def adjustColumnWidths(self):
        for column in self.columns:
            column.resizeDevices()


class Column(QWidget):
    enteredNewColumn = pyqtSignal()
#     draggedChild = pyqtSignal(QWidget)
    editContent = pyqtSignal(BaseDevice)
    resized = pyqtSignal()

    def __init__(self, children=None, parent=None):
        super(Column, self).__init__(parent)
        uic.loadUi(getUiPath('Column.ui'), self)

        if children:
            for child in children:
                self.addChild(child)

        self.setAcceptDrops(True)
        self.relocatedChild = None

    def isEmpty(self):
        return self.childLayout.isEmpty()

    def addChild(self, child, pos=None):
        if self.childLayout.indexOf(child) >= 0:
            return
        if pos is None:
            pos = self.childLayout.count() - 1

        self.childLayout.insertWidget(pos, child)
        try:
            child.editContent.disconnect()
        except TypeError:
            pass
        child.editContent.connect(self.editContent)
#         child.editContent.connect(self.parent.editContent)
        self.resizeDevices()

    @pyqtSlot()
    def deleteChild(self, child=None):
        if not child:
            child = self.sender()
        self.childLayout.takeAt(self.childLayout.indexOf(child))
        child.deleteLater()

    def getChildren(self):
        children = []
        for i in range(self.childLayout.count()):
            child = self.childLayout.itemAt(i).widget()
            if child:
                children.append(child)
        return children

    def getChildIndex(self, child):
        return self.childLayout.indexOf(child)

    def getChildAtPos(self, pos):
        child = self.childAt(pos)
        while True:
            if child is None:
                return
            elif isinstance(child, Child):
                return child
            else:
                child = child.parent()

    def dragEnterEvent(self, event):
        if isinstance(event.mimeData(), ChildMimeData):
            child = event.mimeData().getChild()
            if self.getChildAtPos(event.pos()):
                pos = self.getChildIndex(self.getChildAtPos(event.pos()))
            else:
                pos = None
            self.addChild(child, pos)
            event.accept()
        self.enteredNewColumn.emit()


    def dragMoveEvent(self, event):
        childAtPos = self.getChildAtPos(event.pos())
        child = event.mimeData().getChild()
        if childAtPos is None:
            return

        if self.relocationExpected(child, childAtPos, event.pos()):
            self.relocateChild(child, self.getChildIndex(childAtPos))
            self.relocatedChild = childAtPos

    def relocationExpected(self, dragChild, childAtPos, eventPos):
        if childAtPos and childAtPos != dragChild \
                and childAtPos != self.relocatedChild:
            return True
        elif self.isSouth(dragChild, eventPos) \
                and eventPos.y() < childAtPos.pos().y() + dragChild.height() // 2:
                # dragChild is south of childAtPos and cursor is in the upper
                # part of the childAtPos (1/2 dragChild height to the edge)
            return True
        elif self.isNorth(dragChild, eventPos) \
                and eventPos.y() > childAtPos.pos().y() + \
                childAtPos.height() - dragChild.height() // 2:
                # dragChild is north of childAtPos and cursor is in the lower
                # part of the childAtPos (1/2 dragChild height to the edge)
            return True

        return False

    def isSouth(self, child, pos):
        return child.pos().y() > pos.y()

    def isNorth(self, child, pos):
        return child.pos().y() < pos.y()

    def relocateChild(self, child, index):
        self.childLayout.removeWidget(child)
        self.childLayout.insertWidget(index, child)

    def hasChildWithField(self, field):
        for child in self.childLayout.findChildren(Child):
            if field in child.content.fields.values():
                return True
        return

    def resizeDevices(self):
        children = self.getChildren()
        maxWidth = max(child.content.nameField.sizeHint().width()
                       for child in children)
        if children[0].content.infoField.isVisible():
            maxInfoWidth = max(child.content.infoField.sizeHint().width()
                               for child in children)
            maxWidth = max(maxWidth, maxInfoWidth)
            for child in children:
                child.content.infoField.setMinimumWidth(maxWidth)

        for child in children:
            child.content.nameField.setMinimumWidth(maxWidth)

        self.resized.emit()

class Separator(QFrame):
    def __init__(self, parent=None):
        super(Separator, self).__init__(parent)
        uic.loadUi(getUiPath('Separator.ui'), self)


class Child(QWidget):
    deleteChild = pyqtSignal()
    editContent = pyqtSignal(BaseDevice)

    def __init__(self, content=None, parent=None):
        super(Child, self).__init__(parent)
        uic.loadUi(getUiPath('ChildWidget.ui'), self)
        if content:
            self.mainLayout.insertWidget(1, content)
        self.content = content
        self.dragStartPos = None
        self.anchorLeft.customContextMenuRequested.connect(self.showCustomContextMenu)
        self.actions = self.createActions()

    def createActions(self):
        self.editAction = QAction('Edit', self)
        self.editAction.triggered.connect(self.startEditContent)
        self.deleteAction = QAction('Delete', self)
        self.deleteAction.triggered.connect(self.showDeleteDeviceDialog)

        return [self.editAction, self.deleteAction]

    @pyqtSlot('QPoint')
    def showCustomContextMenu(self, pos):
        if not self.content:
            return

        menu = QMenu(self)
        for action in self.actions:
            menu.addAction(action)

        menu.popup(self.mapToGlobal(pos))

    @pyqtSlot()
    def showDeleteDeviceDialog(self):
        self.mainLayout.removeWidget(self.content)

        dialog = DeleteDeviceDialog(self.content)

        if dialog.exec_():
            self.deleteChild.emit()
        else:
            self.mainLayout.addWidget(self.content)

    @pyqtSlot()
    def startEditContent(self):
        self.editContent.emit(self.content)

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton \
                and (self.anchorLeft.geometry().contains(event.pos())):
            self.dragStartPos = event.pos()

    def mouseMoveEvent(self, event):
        if not event.buttons and Qt.LeftButton and self.dragStartPos:
            return
        if self.dragStartPos \
                and (event.pos() - self.dragStartPos).manhattanLength() \
                > QApplication.startDragDistance():
            self.startDragEvent()

    def startDragEvent(self):
        drag = QDrag(self)
        drag.destroyed.connect(self.restoreDefaults)
        mimeData = ChildMimeData(self)
        mimeData.draggedChild.connect(self.findSurface().draggedChild)
        drag.setMimeData(mimeData)
        self.frame.setFrameShadow(QFrame.Raised)

        drag.exec_(Qt.MoveAction)

    def restoreDefaults(self):
        self.frame.setFrameShadow(QFrame.Sunken)
        self.dragStartPos = None

    def findSurface(self):
        parent = self.parent()
        while True:
            if isinstance(parent, Surface):
                return parent
            else:
                parent = parent.parent()


class ChildMimeData(QMimeData):
    draggedChild = pyqtSignal(QWidget)

    def __init__(self, child, parent=None):
        super(ChildMimeData, self).__init__()
        self.child = child
        self.setText('custom/child')
        self.startTimer(0.2)

    def formats(self):
        return ['custom/child']

    def getChild(self):
        return self.child

    def timerEvent(self, event):
        self.draggedChild.emit(self.child)
