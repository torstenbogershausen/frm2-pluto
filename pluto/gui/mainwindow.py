# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from os.path import join

from pluto.qt import uic, pyqtSignal, Qt, QMainWindow, \
    QListWidgetItem, QFileDialog, QStyle, qApp

from pluto import getUiPath, loadFromIni
from pluto.gui.dialogs.errordialog import ErrorDialog
from pluto.gui.dialogs.newplcdialog import NewPLCDialog
from pluto.gui.loadingscreen import LoadingScreen
from pluto.gui.plc import PLC
from pluto.model.model import Model
from pluto.gui.dialogs.plccodedialog import generatePLCCode


class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        uic.loadUi(getUiPath('MainWindow.ui'), self)
        self.plcs = []
        self.createConnections()
        self.newDialog = None

    def createConnections(self):
        self.actionNew.triggered.connect(self.showNewDialog)
        self.listWidget.itemActivated.connect(showPLC)
        self.actionSaveAll.triggered.connect(self.saveAll)
        self.actionLoad.triggered.connect(self.loadPLC)
        self.actionPlcCode.triggered.connect(self.createPlcCode)

    def saveAll(self):
        for plcWindow in self.plcs:
            plcWindow.plc.saveDevices()

    def loadPLC(self):
        fileNames, _ = QFileDialog.getOpenFileNames(caption='Open',
                                                    directory=join('saves'),
                                                    filter='Plutosaves (*.pluto)')
        if not fileNames:
            return

        for fileName in fileNames:
            data = loadFromIni(fileName)

            globs = data['global']

            loadData = dict(
                rawmode=None,
                manual=dict(
                    conversion=globs['conversion'],
                    version=globs['version'],
                    versionString=globs['versionstring'],
                    author1=globs['author1'],
                    author2=globs['author2']
                ),
                communication=globs['communication'],
            )

            loadData['stats'] = globs

            self.createNewPLC(loadData).loadDevices(fileName)

    def showNewDialog(self):
        if not self.newDialog:
            self.newDialog = NewPLCDialog()
            self.newDialog.accepted.connect(self.createNewPLC)

        self.newDialog.show()

    def extractDialogData(self):
        dialog = self.newDialog

        dialogData = {}
        dialogData['name'] = dialog.getName()
        dialogData['stats'] = dialog.getStats()
        dialogData['rawmode'] = dialog.getRawModeData()
        dialogData['manual'] = dialog.getManualModeData()
        dialogData['communication'] = dialog.getCommunicationMode()

        self.newDialog = None

        return dialogData

    def createNewPLC(self, loadData=None):
        if loadData is None:
            dialogData = self.extractDialogData()
        else:
            dialogData = loadData

        loadingScreen = LoadingScreen()
        name = dialogData['stats']['name']
        plcWindow = PLCWindow(name)
        plcWindow.centerOnDesktop()
        plcWindow.actionNew.triggered.connect(self.showNewDialog)
        plcWindow.closing.connect(self.removePLC)

        plcWindow.setCentralWidget(loadingScreen)

        plcListWidgetItem = PLCListWidgetItem(plcWindow, name)

        self.listWidget.addItem(plcListWidgetItem)

        plc = PLC(dialogData['stats'],
                  dialogData['rawmode'],
                  dialogData['manual'],
                  dialogData['communication'])

        plcWindow.plc = plc
        plc.surface.resized.connect(plcWindow.adjustWindowSize)

        plcWindow.actionAdd.triggered.connect(plc.addNonIndexedDevices)
        plcWindow.actionPollEdit.triggered.connect(plc.updateRefreshTimer)
        plcWindow.closing.connect(plc.quitModelThreads)

        plc.model.progress.connect(loadingScreen.updateStatus)
        plc.model.status.connect(loadingScreen.updateMessage)
        plc.model.numberFound.connect(loadingScreen.setDevicesAmount)
        plc.model.netStatsExtracted.connect(plcWindow.adjustWindowTitle)
        plc.model.plcStatsExtracted.connect(self.updateNames)
        plc.model.errorsOccurred.connect(plcWindow.buildErrorDialog)
        plcWindow.actionSave.triggered.connect(plc.saveDevices)
        plcWindow.actionLoad.triggered.connect(plc.loadDevices)
        plcWindow.actionExport.triggered.connect(plc.exportToIni)
        plcWindow.actionInfo.toggled.connect(plc.toggleSecondaryInformation)
        plc.filling.connect(loadingScreen.updateStatus)
        plc.filled.connect(plcWindow.updateCentralWidget)

        self.plcs.append(plcWindow)

        plcWindow.show()

        qApp.processEvents()

        plc.start()

        return plc

    def removePLC(self):
        plcWindow = self.sender()
        for i in range(self.listWidget.count()):
            item = self.listWidget.item(i)
            if item.plcWindow == plcWindow:
                self.listWidget.takeItem(i)
                break
        self.plcs.remove(plcWindow)

    def createPlcCode(self):
        devices = []
        plcSpec = dict(
            version='',
            indexersize=36,
            indexeroffset=64)

        generatePLCCode(devices, plcSpec)

    def updateNames(self):
        for i in range(self.listWidget.count()):
            self.listWidget.item(i).updateName()

    def closeEvent(self, event):
        for plc in self.plcs[::-1]:
            plc.close()
        if self.newDialog:
            self.newDialog.close()
        event.accept()


class PLCWindow(QMainWindow):
    closing = pyqtSignal()

    def __init__(self, name, parent=None):
        super(PLCWindow, self).__init__(parent)
        uic.loadUi(getUiPath('PLCWindow.ui'), self)
        self.setWindowTitle(name)
        self._baseSize = self.sizeHint()
        self.name = name
        self.plc = None
        self.errorDialog = None

        self.actionResize.triggered.connect(self.adjustWindowSize)
        self.actionDebugComponents.triggered.connect(self.toggleDebugComponents)
        self.actionPlcCode.triggered.connect(self.createPLCCode)

    def buildErrorDialog(self):
        errors = self.plc.model.errors
        self.errorDialog = ErrorDialog()

        for error in errors:
            self.errorDialog.addError(error)
            if error[0] == Model.ERROR_FATAL:
                self.errorDialog.accepted.connect(self.close)
                self.plc.quitModelThreads()
                self.errorDialog.show()

    def closeEvent(self, event):
        self.closing.emit()
        event.accept()

    def adjustWindowTitle(self):
        if self.plc:
            self.name = self.plc.model.name

        title = '%s ' % (self.name)

        if self.plc.model._version:
            title += 'running on v%s ' % self.plc.model._version

        if self.plc.model.ip:
            title += '(%s @ %s)' % (self.plc.model.fqdn, self.plc.model.ip)
        else:
            title += '(%s)' % self.plc.model.address

        self.setWindowTitle(title)

    def centerOnDesktop(self):
        self.setGeometry(QStyle.alignedRect(Qt.LeftToRight, Qt.AlignCenter,
                                            self.size(),
                                            qApp.desktop().availableGeometry()))

    def adjustWindowSize(self):
        qApp.processEvents()
        if self.centralWidget() == self.plc:
            additionalHeight = 0
            if self.plc.widgetReconnect.isVisible():
                additionalHeight += self.plc.widgetReconnect.height()
            if self.plc.widgetLoading.isVisible():
                additionalHeight += self.plc.widgetLoading.height()

            width = max(self.plc.surface.sizeHint().width() + 25,
                        600)
            height = max(self.plc.surface.sizeHint().height() +
                         self._baseSize.height() +
                         self.plc.scrollArea.sizeHint().height() +
                         additionalHeight,
                         100)

            if self.height() != height or self.width() != width:
                self.resize(width, height)

    def updateCentralWidget(self):
        self.setCentralWidget(self.plc)

        self.adjustWindowTitle()

        if self.errorDialog:
            self.errorDialog.open()

        self.adjustWindowSize()

    def toggleDebugComponents(self):
        self.plc.toggleDebugComponents()

    def createPLCCode(self):
        self.plc.createPLCCode()


class PLCListWidgetItem(QListWidgetItem):
    def __init__(self, plcWindow, text=None, parent=None):
        super(PLCListWidgetItem, self).__init__(text, parent)
        self.plcWindow = plcWindow

    def raisePlcWindow(self):
        self.plcWindow.raise_()

    def updateName(self):
        self.plcWindow.adjustWindowTitle()
        self.setText(self.plcWindow.name)


def showPLC(plcListWidgetItem):
    plcListWidgetItem.raisePlcWindow()
