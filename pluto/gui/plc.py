# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

import pickle
from collections import OrderedDict
from os import makedirs
from os.path import exists

from pluto.qt import uic, pyqtSignal, pyqtSlot, QThread, QWidget, QMovie, \
    QFileDialog, QMessageBox

from pluto import convertFilename, getUiPath, loadFromIni, saveAsIni
from pluto.devices import BaseDevice
from pluto.gui.dialogs.adddevicesdialog import AddDevicesDialog
from pluto.gui.dialogs.editdevicedialogs import EditDeviceDialog
from pluto.gui.dialogs.editpolldialog import EditPollDialog
from pluto.gui.dialogs.plccodedialog import generatePLCCode
from pluto.gui.dragsurface import Child, Column
from pluto.model.model import Model
from pluto.save2ini import createIni


class PLC(QWidget):
    filling = pyqtSignal(dict)
    filled = pyqtSignal()
    progress = pyqtSignal(dict)

    def __init__(self, stats, raw, manual, communication, parent=None):
        super(PLC, self).__init__(parent)
        uic.loadUi(getUiPath('PLC.ui'), self)
        self._name = stats['name'] if stats['name'] else 'PLC'

        self.model = Model(stats, raw, manual, communication)
        self.modelThread = QThread()
        self.model.moveToThread(self.modelThread)
        self.modelThread.started.connect(self.model.start)

        self.model.manualSignal.connect(self.filled)
        self.model.rawSignal.connect(self.fillPlc)
        self.model.primaryInfoExtracted.connect(self.fillPlc)
        self.model.secondaryInfoExtracted.connect(self.applySecondaryInfo)

        self.devices = []

        movie = QMovie(':/animations/loading')
        self.labelLoadingIcon.setMovie(movie)
        movie.start()

        if raw or manual:
            self.widgetLoading.hide()

        self.widgetReconnect.hide()

        self.surface.draggedChild.connect(self.scrollArea.ensureWidgetVisible)
        self.surface.editContent.connect(self.editDevice)

    def start(self):
        self.modelThread.start()

    def reconnectionHandling(self, state):
        if state == Model.RECONNECTING:
            self.widgetReconnect.show()
        elif state == Model.RECONNECT_SUCCESS:
            self.widgetReconnect.hide()

    def ensureWidgetVisible(self, widget):
        self.scrollArea.ensureWidgetVisible(widget)

    def getStats(self):
        stats = self.model.getStats()

        return stats

    def createPLCCode(self):
        version = self.model.getVString()[1:]
        if version in ['2014_0701']:
            QMessageBox.critical(self, 'Unsupported Version',
                                 'The version %s is not supported by the Codegenerator'
                                 % (version.replace('_', '.')),
                                 QMessageBox.Ok)
            return

        plcSpec = dict(
            name=self.model.name,
            versionString=self.model.versionString,
            author1=self.model.author1,
            author2=self.model.author2,
            version=version,
            indexersize=self.model.getIndexerSize() * 2,
            indexeroffset=self.model.getIndexerOffset() * 2)

        devices = self.extractDeviceData()

        generatePLCCode(devices, plcSpec)

    def extractDeviceData(self):
        devData = []
        for device in self.devices:
            devData.append(dict(name=device.name,
                                typecode=device.getDeviceCode(),
                                params=device.getParams(),
                                aux=device.getAuxReasons(),
                                unit=device.unit[0] if device.unit else ''))

        return devData

    def addDevice(self, deviceDict):
        '''
            Adds a device generated from deviceDict and wrapped in a 'Child'
            to the first 'Column' of the plc's 'Surface'.
            Returns said device.
        '''

        child = self.createChild(deviceDict)
        self.surface.addChild(child)

        return child.content

    def createChild(self, deviceDict):
        '''
            Creates a 'Child', containing a device generated from deviceDict and
            returns said 'Child'
        '''

        device = deviceDict['subType'][0](deviceDict, self.model, parent=self)
        self.devices.append(device)
        child = Child(device)

        self.model.dataUpdated.connect(device.updateValues)
        child.deleteChild.connect(self.removeDevices)

        return child

    def createDevice(self, deviceDict):
        return deviceDict['subType'][0](deviceDict, self.model, parent=self)

    @pyqtSlot()
    def removeDevices(self):
        child = self.sender()
        self.devices.remove(child.content)
        for column in self.surface.columns:
            if child in column.getChildren():
                column.deleteChild(child)
                self.surface.removeEmptyColumns()
                return

    def addNonIndexedDevices(self):
        deviceDialog = AddDevicesDialog(self.model.verConstants)
        if deviceDialog.exec_():
            for deviceDict in deviceDialog.generateDeviceDicts():
                self.addDevice(deviceDict).setSecondaryInformation(deviceDict)


    def fillPlc(self):
        self.filling.emit({'message': 'Building devices',
                           'step': 'last'})
        for deviceDict in self.model.getDeviceDicts():
            self.addDevice(deviceDict)

        self.model.devices = self.devices
        self.filled.emit()

    def toggleDebugComponents(self):
        for device in self.devices:
            device.toggleDebugComponents()

    def saveDevices(self):
        if not exists('saves'):
            makedirs('saves')

        fileName, _ = QFileDialog.getSaveFileName(caption='Save %s' % self._name,
                                                  directory='saves',
                                                  filter='Plutosaves (*.pluto)')

        columns = []
        for column in self.surface.columns:
            widgetList = []
            for child in column.getChildren():
                widgetList.append(child.content.getParamDict()['number'])
            columns.append(widgetList)

        if not fileName:
            return
        else:
            fileName = convertFilename(fileName, '.pluto')


        data = OrderedDict()

        # stats: conversion, address, name, refresh, _offset
        stats = self.getStats()
        globs = OrderedDict()
        data['global'] = globs
        for entry in stats:
            globs [entry] = stats[entry]

        # save widgets according to their order in the columns
        globs['columns'] = []

        for column in self.surface.columns:
            widgetList = []
            for child in column.getChildren():
                paramDict = child.content.getParamDict()
                devNumber = str(self.devices.index(child.content))
                widgetList.append(devNumber)

                data[devNumber] = {'paramdict': pickle.dumps(paramDict, 0)}

            globs['columns'].append(widgetList)

        saveAsIni(data, fileName)

    @pyqtSlot()
    def exportToIni(self):
        fileName, _ = QFileDialog.getSaveFileName(caption='Export %s' % self._name,
                                                  directory='saves',
                                                  filter='Inifiles (*.ini)')

        if not fileName:
            return
        else:
            fileName = convertFilename(fileName, '.ini')

        paramDicts = {str(i): device.getParamDict()
                      for i, device in enumerate(self.devices)}
        globalDict = self.getStats()

        createIni(paramDicts, globalDict, fileName, self.model.verConstants)

    def loadDevices(self, fileName):
        if fileName is False:
            fileName, _ = QFileDialog.getOpenFileName(caption='Open',
                                                      directory='saves',
                                                      filter='Plutosaves (*.pluto)')

        if not fileName:
            return

        data = loadFromIni(fileName)

        columns = data['global']['columns']

        for column in columns:
            childColumn = Column()

            # fill the Column
            for devNumber in column:
                deviceDict = pickle.loads(data[devNumber]['paramdict'])
                child = self.createChild(deviceDict)
                child.content.setSecondaryInformation(deviceDict)
                childColumn.addChild(child)
            # insert column into plc
            self.surface.addColumn(childColumn, 'right')

    def applySecondaryInfo(self, index, info):
        if index == -1:
            self.widgetLoading.hide()
            self.surface.adjustColumnWidths()
        else:
            self.devices[index].setSecondaryInformation(info)

    @pyqtSlot(bool)
    def toggleSecondaryInformation(self, visible):
        for device in self.devices:
            device.toggleSecondaryInformation(visible)
        self.resizeNameFields()

    def updateRefreshTimer(self):
        dialog = EditPollDialog('%d ms' % self.model.getRefreshInterval())

        if dialog.exec_():
            self.model.setRefreshInterval(dialog.getNewRefresh())

    def quitModelThreads(self):
        if self.model.comDev:
            self.model.comDev.terminateconnection = True
        self.model.closeSubThreads()
        self.modelThread.exit()
        self.modelThread.wait()

    @pyqtSlot()
    def resizeNameFields(self):
        self.surface.adjustColumnWidths()

    @pyqtSlot(BaseDevice)
    def editDevice(self, device):
        dialog = EditDeviceDialog(device, self.model.verConstants)
        if dialog.exec_():
            device.updateContent(dialog.editDict)
