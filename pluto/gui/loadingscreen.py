# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.qt import uic, pyqtSlot, QWidget

from pluto import getUiPath


class LoadingScreen(QWidget):
    def __init__(self, parent=None):
        super(LoadingScreen, self).__init__(parent)
        uic.loadUi(getUiPath('LoadingSplash.ui'), self)
        self.maxDevices = 0
        self.prefix = ''

    @pyqtSlot(dict)
    def updateStatus(self, progress):
        globalStep = progress['step']
        maxSteps = max(self.maxDevices, 1)
        if globalStep == 'last':
            self.prefix = '%d/%d: ' % (maxSteps, maxSteps)
        else:
            self.prefix = '%d/%d: ' % (globalStep, maxSteps)
            self.progressBar.setValue(globalStep // maxSteps * 100)

        self.labelStatus.setText('%s %s' % (self.prefix, progress['message']))


    def updateMessage(self, message):
        self.labelStatus.setText('%s %s' % (self.prefix, message))

    @pyqtSlot(int)
    def setDevicesAmount(self, amount):
        self.maxDevices = float(amount)
