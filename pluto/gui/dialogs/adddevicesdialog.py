# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2016 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.qt import uic, QDialog, QPushButton

from pluto import getUiPath


class AddDevicesDialog(QDialog):
    def __init__(self, constants=None, parent=None):
        super(AddDevicesDialog, self). __init__(parent)

        # code completion block start
        from pluto.gui.dialogs.devicesdisplay import PlutoDevicesDisplay

        button = QPushButton()
        self.buttonOffset = button
        self.buttonAdd = button
        self.buttonCancel = button
        self.devicesDisplay = PlutoDevicesDisplay()
        # code completion block end

        uic.loadUi(getUiPath('AddDevices.ui'), self)
        self.devicesDisplay.constants = constants

        self.buttonOffset.clicked.connect(self.devicesDisplay.autoAdjustOffsets)
        self.devicesDisplay.buttonRemove.clicked.connect(self.manageAddButtonState)
        self.devicesDisplay.buttonAdd.clicked.connect(self.manageAddButtonState)

    def generateDeviceDicts(self):
        return [device.getDeviceDict()
                for device in self.devicesDisplay.devices]

    def manageAddButtonState(self):
        self.buttonAdd.setEnabled(self.devicesDisplay.tableWidget.rowCount())
