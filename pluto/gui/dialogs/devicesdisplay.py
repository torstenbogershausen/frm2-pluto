# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2016 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from pluto.qt import uic, pyqtSlot, pyqtSignal, QHeaderView, QWidget, \
    QTableWidget, QTableWidgetItem, QPushButton

from pluto import getUiPath
from pluto.gui.dialogs.displaydevices import BaseDevice, CodeDevice, PlutoDevice


class BaseDevicesDisplay(QWidget):
    devicesAmountChanged = pyqtSignal(int)

    NAME = 1
    TYPECODE = 2
    UNIT = 3
    PARAMS = 4

    # addDevices: '' name typecode, unit, params
    # codeGen: '', name, typecode unit, params
    def __init__(self, constants=None, parent=None):
        super(BaseDevicesDisplay, self).__init__(parent)

        # code completion block start
        pushButton = QPushButton()
        self.buttonAdd = pushButton
        self.buttonRemove = pushButton
        self.tableWidget = QTableWidget()
        # code completion block end

        uic.loadUi(getUiPath('BaseDevicesDisplay.ui'), self)
        self.deviceIndex = 0
        self.devices = []

        self.constants = constants

        try:
            self.tableWidget.verticalHeader().setSectionsMovable(True)
            self.tableWidget.horizontalHeader().setSectionResizeMode(0, QHeaderView.Fixed)
        except AttributeError:  # Qt4
            self.tableWidget.verticalHeader().setMovable(True)
            self.tableWidget.horizontalHeader().setResizeMode(0, QHeaderView.Fixed)

        self.adjustWidths()

        self.buttonAdd.clicked.connect(self.addDevice)
        self.buttonRemove.clicked.connect(self.removeDevices)
        self.tableWidget.verticalHeader().sectionMoved.connect(self.updateDevicesList)

    @pyqtSlot(int, int, int)
    def updateDevicesList(self, _, old, new):
        moved = self.devices.pop(old)
        self.devices.insert(new, moved)

    def findDeviceByName(self, name):
        for device in self.devices:
            if device.getName() == name:
                return device

    def editConstants(self, constants):
        self.constants = constants

    def adjustWidths(self):
        self.tableWidget.setColumnWidth(0, 1)
        self.tableWidget.setColumnWidth(self.NAME, 200)
        self.tableWidget.setColumnWidth(self.TYPECODE, 250)
        self.tableWidget.setColumnWidth(self.UNIT, 75)

    def addDevice(self, paramDict=None):
        self.deviceIndex += 1
        device = self.createNewDevice(paramDict)
        self.devices.append(device)

        row = self.tableWidget.rowCount()
        self.insertDevice(row, device)

        self.devicesAmountChanged.emit(len(self.devices))

        return device

    def removeDevices(self):
        selectedRows = self.tableWidget.selectionModel().selectedRows()
        rowIndices = sorted([row.row() for row in selectedRows], reverse=True)
        removedDevs = []
        for i in rowIndices:
            dev = self.tableWidget.verticalHeaderItem(i).device
            removedDevs.append(dev)
            self.devices.remove(dev)
            self.tableWidget.removeRow(i)

        self.devicesAmountChanged.emit(len(self.devices))

        return removedDevs

    def createNewDevice(self, paramDict):
        return BaseDevice(self.constants, paramDict, self.deviceIndex)

    def insertDevice(self, row, device):
        self.tableWidget.insertRow(row)
        self.tableWidget.setVerticalHeaderItem(row, device.rowHeader)
        self.tableWidget.setCellWidget(row, self.NAME, device.nameWidget)
        self.tableWidget.setCellWidget(row, self.TYPECODE, device.typecodeWidget)
        self.tableWidget.setCellWidget(row, self.UNIT, device.unitWidget)
        self.tableWidget.setCellWidget(row, self.PARAMS, device.paramsWidget)


class CodeDevicesDisplay(BaseDevicesDisplay):
    AUX = 5
    ADEVS = 6
    MODULE = 7

    deviceAdded = pyqtSignal(CodeDevice)
    deviceRemoved = pyqtSignal(CodeDevice)

    def __init__(self, constants=None, parent=None):
        super(CodeDevicesDisplay, self).__init__(constants, parent)

        self.addColumns()

    def addColumns(self):
        self.tableWidget.insertColumn(self.AUX)
        self.tableWidget.insertColumn(self.ADEVS)
        self.tableWidget.insertColumn(self.MODULE)
        self.tableWidget.setHorizontalHeaderItem(self.AUX,
                                                 QTableWidgetItem('aux'))
        self.tableWidget.setHorizontalHeaderItem(self.ADEVS,
                                                 QTableWidgetItem('adevs'))
        self.tableWidget.setHorizontalHeaderItem(self.MODULE,
                                                 QTableWidgetItem('module'))
        self.tableWidget.setColumnWidth(self.AUX, 75)
        self.tableWidget.setColumnWidth(self.PARAMS, 75)
        self.tableWidget.setColumnWidth(self.ADEVS, 75)

    def createNewDevice(self, paramDict):
        return CodeDevice(self.constants, paramDict, self.deviceIndex)

    def addDevice(self, paramDict=None):
        device = BaseDevicesDisplay.addDevice(self, paramDict)
        device.deviceRequest.connect(self.startAdevInquiry)
        self.deviceAdded.emit(device)

    def addAdevs(self, adevs):
        for parentdevice in adevs:
            device = self.findDeviceByName(parentdevice)
            for adev in adevs[parentdevice]:
                device.adevs.append(self.findDeviceByName(adev))
            device.updateAdevWidgetText()

    def removeDevices(self):
        for dev in BaseDevicesDisplay.removeDevices(self):
            for device in self.devices:
                device.removeAdev(dev)
            self.deviceRemoved.emit(dev)

    def insertDevice(self, row, device):
        BaseDevicesDisplay.insertDevice(self, row, device)
        self.tableWidget.setCellWidget(row, self.AUX, device.auxWidget)
        self.tableWidget.setCellWidget(row, self.ADEVS, device.adevsWidget)
        self.tableWidget.setCellWidget(row, self.MODULE, device.moduleWidget)

    @pyqtSlot()
    def startAdevInquiry(self):
        self.sender().showAdevDialog(self.devices)


class PlutoDevicesDisplay(BaseDevicesDisplay):
    OFFSET = 5
    SIZE = 6

    def __init__(self, constants=None, parent=None):
        super(PlutoDevicesDisplay, self).__init__(constants, parent)
        self.addColumns()

    def addColumns(self):
        self.tableWidget.insertColumn(self.OFFSET)
        self.tableWidget.insertColumn(self.SIZE)
        self.tableWidget.setHorizontalHeaderItem(self.OFFSET,
                                                 QTableWidgetItem('offset'))
        self.tableWidget.setHorizontalHeaderItem(self.SIZE,
                                                 QTableWidgetItem('size'))
        self.tableWidget.setColumnWidth(self.PARAMS, 100)
        self.tableWidget.setColumnWidth(self.OFFSET, 75)

    def createNewDevice(self, paramDict):
        return PlutoDevice(self.constants, paramDict, self.deviceIndex)

    def insertDevice(self, row, device):
        BaseDevicesDisplay.insertDevice(self, row, device)
        self.tableWidget.setCellWidget(row, self.OFFSET, device.offsetWidget)
        self.tableWidget.setCellWidget(row, self.SIZE, device.sizeWidget)

    @pyqtSlot()
    def autoAdjustOffsets(self):
        offset = 0
        for device in self.devices:
            if offset:
                device.setOffset(offset)
                offset += device.getSize()
            else:
                device.setOffset(device.getOffset())
                offset = device.getOffset() + device.getSize()
