# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2015 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA


from pluto.qt import uic, pyqtSlot, QDialog

from pluto import getUiPath
from pluto.devices.fields.validators import FloatValidator


class StatemachineDebugDialog(QDialog):
    DEFAULTSPINBOXTEXT = '---'

    def __init__(self, model, register, parent=None):
        super(StatemachineDebugDialog, self).__init__(parent)
        uic.loadUi(getUiPath('StateMachineDebugDialog.ui'), self)

        self.model = model
        self.register = register

        self.comboBoxParams.addItems(list(self.model.verConstants.paramIndices))

        self.valueEdit.setValidator(FloatValidator())

        self.commandButtons = (
            self.buttonInit,
            self.buttonDoRead,
            self.buttonDoWrite,
            self.buttonBusy,
            self.buttonDone,
            self.buttonErrNoIdx,
            self.buttonErrReadonly,
            self.buttonErrRetry,
        )

        for button in self.commandButtons:
            button.clicked.connect(self.writeCommand)

        self.model.registerBuffer.dataUpdated.connect(self.updateValue)
        self.spinBoxParams.valueChanged.connect(self.adjustParams)
        self.comboBoxParams.currentIndexChanged.connect(self.adjustParams)
        self.buttonSetValue.clicked.connect(self.writeValue)

    @pyqtSlot()
    def adjustParams(self):
        if self.sender() is self.spinBoxParams:
            try:
                text = self.model.verConstants.parameters[self.spinBoxParams.value()]
                self.comboBoxParams.setCurrentIndex(self.getComboIndex(text))
            except KeyError:
                self.comboBoxParams.setCurrentIndex(0)
        elif self.sender() is self.comboBoxParams:
            text = self.comboBoxParams.currentText()
            if text == self.DEFAULTSPINBOXTEXT:
                return
            self.spinBoxParams.setValue(self.model.verConstants.paramIndices[text][0])

    def getComboIndex(self, text):
        for i in range(self.comboBoxParams.count()):
            if self.comboBoxParams.itemText(i) == text:
                return i

        return 0

    def writeValue(self):
        self.model.writeFloat(float(self.valueEdit.text()), self.register + 1)
        self.valueEdit.setDisplayState(self.valueEdit.DEFAULT)

    def writeCommand(self, state=None):
        if not state:
            state = self.commandButtons.index(self.sender())
        sub = self.spinBoxSubs.value()
        param = self.spinBoxParams.value()

        value = (state << 13) | (sub << 8) | param

        if state == 2:  # DO_WRITE
            # TODO
            self.model.writeWordFloat(value, float(self.valueEdit.text()),
                                      self.register)
        else:
            self.model.writeWord(value, self.register)
        self.valueEdit.enableUpdates()

    def updateValue(self):
        self.valueEdit.updateValue(self.model.getFloat(self.register + 1))

        cmd = self.model.getWord(self.register)
        for button in self.commandButtons:
            button.setDown(False)

        self.commandButtons[cmd >> 13].setDown(True)
