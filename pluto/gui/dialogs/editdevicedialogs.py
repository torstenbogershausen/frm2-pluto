# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2015 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA


import copy
import logging

from pluto.qt import uic, pyqtSlot, pyqtSignal, QSettings, Qt, QDialog, \
    QWidget, QPushButton, QMessageBox, QLineEdit, QComboBox, QSpinBox, \
    QLabel, QGridLayout

from pluto import getUiPath, getDefaultSubName
from pluto.devices.fields.validators import Int16Validator, AsciiValidator


class DeleteDeviceDialog(QDialog):
    def __init__(self, device, parent=None):
        super(DeleteDeviceDialog, self).__init__(parent)
        uic.loadUi(getUiPath('DeleteDeviceDialog.ui'), self)
        self.widgetLayout.addWidget(device)


class EditDeviceDialog(QDialog):
    # TODO: size calculation for devices with subs AND params. (not specified yet)
    def __init__(self, device, verConsts, parent=None):
        super(EditDeviceDialog, self).__init__(parent)

        # code completion block start
        from pluto.devices.fields.components.valueedit import ValueEdit

        self.lineEditName = QLineEdit()
        self.lineEditOffset = ValueEdit()
        self.comboBoxSubs = QComboBox()
        self.spinBoxParams = QSpinBox()
        self.spinBoxSubs = QSpinBox()
        self.labelSubs = QLabel()
        self.labelParams = QLabel()
        self.mainLayout = QGridLayout()
        self.buttonSubs = DeviceSubButton()
        self.buttonParams = ParamSubButton()
        # code completion block end

        uic.loadUi(getUiPath('EditDeviceDialog.ui'), self)
        self.editDict = copy.deepcopy(device.getEditDict())
        self.verConsts = verConsts

        self.lineEditName.setText(self.editDict['name'])
        self.lineEditOffset.setValidator(Int16Validator())
        self.lineEditOffset.updateValue(self.editDict['register'])

        displayname = device.DISPLAYNAME
        if displayname in self.verConsts.FLATDEVICES:
            self.hasSubs = False
            self.hasParams = True
            self.spinBoxParams.hide()
        elif displayname in self.verConsts.PARAMETERDEVICES:
            self.hasSubs = False
            self.hasParams = True
        elif displayname in self.verConsts.DEVICESWITHSUBS:
            self.hasSubs = True
            self.hasParams = False
        # integration pending
#         elif displayname in self.verConsts.DEVICESWITHSUBSANDPARAMS:
#             self.hasSubs=True
#             self.hasParams=True
        else:
            self.hasSubs = False
            self.hasParams = False

        if self.hasSubs:
            self.names = self.editDict['names']
            self.spinBoxSubs.setValue(len(self.names))
            self.setComboBoxSubs()
            self.buttonSubs.setProp('constants', self.verConsts)
            self.buttonSubs.setProp('compList', self.names)
            self.comboBoxSubs.currentIndexChanged.connect(self.updateParamButton)
            self.spinBoxSubs.setRange(self.verConsts.MINSUBS,
                                      self.verConsts.MAXSUBS)
            self.buttonSubs.compsChanged.connect(self.setComboBoxSubs)
            self.spinBoxSubs.valueChanged.connect(self.updateSubsAmount)
        else:
            self.labelSubs.hide()
            self.comboBoxSubs.hide()
            self.buttonSubs.hide()
            self.spinBoxSubs.hide()

        if self.hasParams:
            self.params = self.editDict['params']
            self.parameters = self.verConsts.parameters
            curParams = self.params[self.comboBoxSubs.currentIndex()]
            self.buttonParams.setProp('constants', self.verConsts)
            self.buttonParams.setProp('compList', curParams)
            self.spinBoxParams.setValue(len(curParams))
            self.spinBoxParams.setRange(self.verConsts.MINPARAMS,
                                        self.verConsts.MAXPARAMS)
            self.buttonParams.compsChanged.connect(self.updateParams)
            self.spinBoxParams.valueChanged.connect(self.updateParameterAmount)
        else:
            self.labelParams.hide()
            self.comboBoxSubs.hide()
            self.spinBoxParams.hide()
            self.buttonParams.hide()

    def adjustParamListsAmount(self):
        names = self.buttonSubs.compList
        if self.hasSubs and len(self.params) < len(names):
            for _ in range(len(names) - len(self.params)):
                self.params.append([0])

    def setComboBoxSubs(self):
        self.comboBoxSubs.clear()
        self.comboBoxSubs.addItems(self.buttonSubs.compList)

    @pyqtSlot()
    def updateComboBoxSubs(self):
        index = self.comboBoxSubs.currentIndex()
        self.setComboBoxSubs()
        self.comboBoxSubs.setCurrentIndex(index)

    @pyqtSlot()
    def updateParamButton(self):
        params = self.params[self.comboBoxSubs.currentIndex()]
        self.buttonParams.editCompList(params)
        self.spinBoxParams.setValue(len(params))

    @pyqtSlot()
    def updateParams(self):
        self.params[self.comboBoxSubs.currentIndex()] = self.buttonParams.compList

    def updateDisplayedParams(self):
        self.currentParams = self.comboBoxSubs.addItems

    @pyqtSlot(int)
    def updateParameterAmount(self, amount):
        params = self.buttonParams.compList
        current = len(params)
        if current > amount:
            # remove dublicates from list
            params = list(set(params))
            params.sort()
            # adjust listsize
            if len(params) > amount:
                params = params[:amount]
            else:
                for _ in range(amount - current):
                    params.append(0)
        else:
            for _ in range(amount - current):
                params.append(0)

        params.sort()

        self.editDict['size'] += (amount - current) * 2
        self.buttonParams.editCompList(params)
        self.updateSubDevice(amount)

    @pyqtSlot(int)
    def updateSubsAmount(self, amount):
        names = self.buttonSubs.compList
        current = len(names)
        if current >= amount:
            names = names[:amount]
            if self.hasParams:
                self.params = self.params[:amount]
        else:
            for i in range(len(names), amount):
                names.append(getDefaultSubName(i + 1))
            if self.hasParams:
                self.adjustParamListsAmount()

        self.editDict['size'] += (amount - current) * 3
        self.buttonSubs.editCompList(names)
        self.updateSubDevice(amount)
        if self.hasParams:
            self.setComboBoxSubs()

    def updateSubDevice(self, amount):
        self.editDict['subType'] = (self.editDict['subType'][0], amount)

    def accept(self):
        self.editDict['register'] = int(self.lineEditOffset.text())
        self.editDict['name'] = self.lineEditName.text()
        self.editDict['names'] = (self.buttonSubs.compList
                                  if self.hasSubs else [])
        return QDialog.accept(self)


class EditDialog(QDialog):
    def __init__(self, parent=None):
        super(EditDialog, self).__init__(parent)
        uic.loadUi(getUiPath('EditComponentsDialog.ui'), self)
        self.buttonAdd.clicked.connect(self.addItem)

    def getResults(self):
        return []

    def addItem(self):
        self.insertItem(self.mainLayout.count() + 1, 0)

    @pyqtSlot()
    def insertItem(self, index, data):
        pass

    def removeItem(self):
        pass


class EditComponentsDialog(EditDialog):
    def __init__(self, constants, parent=None):
        super(EditComponentsDialog, self).__init__(parent)
        self.constants = constants


class EditParameterDialog(EditComponentsDialog):
    parameterAmountAboutToChange = pyqtSignal(int)

    def __init__(self, constants, params, parent=None):
        super(EditParameterDialog, self).__init__(constants, parent)

        self.reasonWidget.hide()
        self.indexerWidget.hide()

        self.params = []

        for i, param in enumerate(params):
            self.insertItem(i + 1, param)

        if len(self.params) == 1:
            self.params[0].buttonRemove.setDisabled(True)

        self.changeAccepted = False

    def getResults(self):
        return [param.getValue() for param in self.params]

    @pyqtSlot()
    def insertItem(self, index, data):
        paramEdit = ParamEdit(self.constants, index, data)
        paramEdit.buttonRemove.clicked.connect(self.removeItem)
        self.params.append(paramEdit)
        self.mainLayout.addWidget(paramEdit)

        if len(self.params) == 2:
            for i in range(2):
                self.params[i].buttonRemove.setEnabled(True)

    @pyqtSlot()
    def removeItem(self):
        paramEdit = self.sender().parent()
        self.params.remove(paramEdit)
        self.mainLayout.removeWidget(paramEdit)
        paramEdit.hide()
        paramEdit.deleteLater()

        for i, param in enumerate(self.params):
            param.setIndex(i + 1)

        if len(self.params) == 1:
            self.params[0].buttonRemove.setDisabled(True)

        self.buttonAdd.setEnabled(True)

    def accept(self):
        if len(self.params) > 16 and not self.changeAccepted:
            self.parameterAmountAboutToChange.emit(len(self.params))
        else:
            EditComponentsDialog.accept(self)

    @pyqtSlot()
    def showConfirmationDialog(self):
        msgBox = QMessageBox()
        msgBox.setText('The amount of parameters exceeds 16. \n'
                       'Do you want to change your device to a ParamDevice?')
        msgBox.setStandardButtons(QMessageBox.Yes | QMessageBox.Cancel)

        # if exec_() does not work here since the clicked buttoncode is returned
        result = msgBox.exec_()

        if result == QMessageBox.Yes:
            self.changeAccepted = True
            self.accept()


class EditSubsDialog(EditComponentsDialog):
    def __init__(self, constants, subs, parent=None):
        super(EditSubsDialog, self).__init__(constants, parent)

        self.plusWidget.hide()
        self.indexerWidget.hide()

        self.subs = []

        for i, sub in enumerate(subs):
            subEdit = SubEdit(i + 1, sub)
            self.subs.append(subEdit)
            self.mainLayout.addWidget(subEdit)

    def getResults(self):
        return [sub.getValue() for sub in self.subs]


class EditAuxDialog(EditDialog):
    def __init__(self, amount, reasons, parent=None):
        super(EditAuxDialog, self).__init__(parent)

        self.plusWidget.hide()
        self.indexerWidget.hide()

        self.auxReasonWidgets = []

        self.setAuxReasons(amount, reasons)

        # load previous checkstate
        settings = QSettings()
        self.checkBoxKeep.setCheckState(settings.value('keepEmptyReasons', type=int)
                                        if settings.contains('keepEmptyReasons')
                                        else Qt.Checked)
        del settings

    def setAuxReasons(self, amount, reasons):
        for i in range(amount):
            if i < len(reasons):
                reason = reasons[i]
            else:
                reason = ''

            reasonWidget = AuxEdit(i, reason)

            self.auxReasonWidgets.append(reasonWidget)
            reasonWidget.lineEditReason.textChanged.connect(self.adjustIndSize)

            self.mainLayout.addWidget(reasonWidget)

    def getReasons(self):
        reasons = []

        for reasonWidget in self.auxReasonWidgets:
            reason = reasonWidget.getReason()
            if reason or self.checkBoxKeep.isChecked():
                reasons.append(reasonWidget.getReason())

        return reasons

    @pyqtSlot()
    def adjustIndSize(self):
        maxSize = 0
        for item in self.auxReasonWidgets:
            size = len(item.lineEditReason.text()) + 2
            if size > maxSize:
                maxSize = size

        self.spinBoxSize.setValue(maxSize)

    def accept(self):
        # save checkbox state
        settings = QSettings()
        settings.setValue('keepEmptyReasons', self.checkBoxKeep.checkState())
        del settings

        return QDialog.accept(self)


class ComponentsEditButton(QPushButton):
    compsChanged = pyqtSignal()

    # pylint: disable=dangerous-default-value
    def __init__(self, deviceType, constants=None, compList=None, parent=None):
        '''
            deviceType: type of device for which the editSubsDialog should be opened on click (str)
            compList: list of subDevices (str-list)
        '''

        super(ComponentsEditButton, self).__init__(parent)

        self.defaultStyleSheet = self.styleSheet()
        self.constants = constants

        self.setFixedWidth(29)
        self.setText('...')
        self.compsSatisfactory = True

        self.deviceType = deviceType

        if compList:
            self.setProp('compList', compList)
        else:
            self.compList = []

        if constants:
            self.setProp('constants', constants)
        else:
            self.constants = None

        self.clicked.connect(self.initiateEdit)

    def setProp(self, pname, value):
        if pname == 'constants':
            self.constants = value
            if self.deviceType not in ('params', 'subdevices'):
                if self.deviceType in self.constants.FLATDEVICES:
                    self.deviceType = 'params'
                elif self.deviceType in self.constants.DEVICESWITHSUBS:
                    self.deviceType = 'subdevices'
        elif pname == 'compList':
            self.compList = value

    def editConstants(self, constants):
        self.constants = constants

    def editCompList(self, subList):
        self.compList = subList
        self.updateBackground()

    def initiateEdit(self):
        if not self.compList or not self.constants:
            logging.warning('necessary components missing (compList and/or constants)')
            return

        # deviceType can already be params/subdevices or the following
        if self.deviceType in self.constants.FLATDEVICES + self.constants.PARAMETERDEVICES:
            self.deviceType = 'params'
        elif self.deviceType in self.constants.DEVICESWITHSUBS:
            self.deviceType = 'subdevices'

        if self.deviceType == 'params':
            editDialog = EditParameterDialog(self.constants, self.compList)
        else:
            editDialog = EditSubsDialog(self.constants, self.compList)

        self.signalConnectionHook(editDialog)

        if editDialog.exec_():
            self.compList = editDialog.getResults()
            if self.deviceType == 'params':
                self.compList.sort()
            self.updateBackground()

    def signalConnectionHook(self, editDialog):
        pass

    def updateBackground(self):
        if len(self.compList) == len(set(self.compList)):
            self.setStyleSheet(self.defaultStyleSheet)
            self.setToolTip('')
            self.compsSatisfactory = True
        else:
            if self.deviceType == 'params':
                self.setStyleSheet('background-color: red')
            elif self.deviceType == 'subdevices':
                self.setStyleSheet('background-color: yellow')
            self.setToolTip('Dublicate entries in device')
            self.compsSatisfactory = False
        self.compsChanged.emit()


class DeviceSubButton(ComponentsEditButton):
    def __init__(self, verConstants=None, subdevs=None, parent=None):
        super(DeviceSubButton, self).__init__('subdevices', verConstants,
                                              subdevs, parent=parent)


class ParamSubButton(ComponentsEditButton):
    parameterAmountAboutToChange = pyqtSignal(int)
    confirmTypecodeChange = pyqtSignal()

    def __init__(self, verConstants=None, params=None, parent=None):
        super(ParamSubButton, self).__init__('params', verConstants,
                                             params, parent=parent)

    def signalConnectionHook(self, editDialog):
        editDialog.parameterAmountAboutToChange.connect(
            self.parameterAmountAboutToChange)
        self.confirmTypecodeChange.connect(editDialog.showConfirmationDialog)


class ParamEdit(QWidget):
    def __init__(self, constants, index, param, parent=None):
        '''
            index: The number for the label (int)
            param: The current parameter for the index (str)
        '''

        super(ParamEdit, self).__init__(parent)
        uic.loadUi(getUiPath('ParamEdit.ui'), self)
        self.constants = constants
        self.comboBox.addItems(self.constants.wholeRangeParameters)
        self.label.setText(str(index))

        param = (self.constants.parameters[param]
                 if param in self.constants.parameters
                 else param)

        comboIndex = self.constants.wholeRangeParameters.index(param)
        self.comboBox.setCurrentIndex(comboIndex)

    def getValue(self):
        val = self.comboBox.currentText()

        if val in self.constants.paramIndices:
            value = self.constants.paramIndices[val][0]
        else:
            value = int(val)

        return value

    def setIndex(self, index):
        self.label.setText(str(index))

    def setText(self, index):
        text = self.constants.parameters[int(index)]
        self.comboBox.setCurrentIndex(self.comboBox.findText(text))


class SubEdit(QWidget):
    def __init__(self, index, sub, parent=None):
        '''
            index: The number for the label (int)
        '''

        super(SubEdit, self).__init__(parent)
        uic.loadUi(getUiPath('SubEdit.ui'), self)
        self.label.setText(str(index))
        self.lineEdit.setText(sub)

    def getValue(self):
        return self.lineEdit.text()

    def setText(self, text):
        self.lineEdit.setText(text)


class AuxEdit(QWidget):
    def __init__(self,
                 number, reason, parent=None):
        super(AuxEdit, self).__init__(parent)
        uic.loadUi(getUiPath('AuxWidget.ui'), self)

        self.lineEditReason.setValidator(AsciiValidator())

        self.labelNumber.setText(str(number))
        self.lineEditReason.setText(reason)

    def getReason(self):
        return self.lineEditReason.text()
