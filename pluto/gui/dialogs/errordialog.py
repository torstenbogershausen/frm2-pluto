# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

import logging

from pluto.qt import uic, QSize, QDialog, QLabel, QWidget, QHBoxLayout, QPixmap

from pluto import getUiPath
from pluto.model.model import Model


class ErrorDialog(QDialog):
    def __init__(self, parent=None):
        super(ErrorDialog, self).__init__(parent)
        uic.loadUi(getUiPath('ErrorDialog.ui'), self)

    def addError(self, error):
        self.errorLayout.addWidget(Error(error))


class Error(QWidget):
    def __init__(self, error, parent=None):
        super(Error, self).__init__(parent)
        self.labelIcon = QLabel()
        self.labelIcon.setFixedSize(QSize(29, 29))
        self.labelError = QLabel(error[1])
        if error[0] == Model.ERROR_FATAL:
            self.labelIcon.setPixmap(QPixmap(':/errors/error'))
            logging.error(error[1])
        elif error[0] == Model.ERROR_WARNING:
            self.labelIcon.setPixmap(QPixmap(':/errors/warning'))
            logging.warning(error[1])

        layout = QHBoxLayout()
        layout.addWidget(self.labelIcon)
        layout.addWidget(self.labelError)
        self.setLayout(layout)
