# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2016 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA


import logging

from pluto.qt import uic, pyqtSlot, pyqtSignal, QRegExp, QObject, QEvent, \
    QPushButton, QLineEdit, QRegExpValidator, QComboBox, QTableWidgetItem, \
    QWidget, QCheckBox, QDialog, QSpinBox, QVBoxLayout

from pluto import getUiPath
from pluto.devices.fields.components.valueedit import ValueEdit
from pluto.devices.fields.validators import Int16Validator, DeviceNameValidator
from pluto.gui.dialogs.editdevicedialogs import EditAuxDialog


class BaseDevice(QObject):
    confirmTypecodeChange = pyqtSignal()

    def __init__(self, constants, devValues, index, parent=None):
        super(BaseDevice, self).__init__(parent)

        self.wheelFilter = MouseWheelFilter()
        self.nameWidget = QLineEdit('Device%s' % index)
        self.nameWidget.setValidator(DeviceNameValidator())
        self.typecodeWidget = QComboBox()
        self.typecodeWidget.installEventFilter(self.wheelFilter)
        self.unitWidget = QComboBox()
        self.unitWidget.installEventFilter(self.wheelFilter)
        self.paramsWidget = ParamsCellWidget(constants)
        self.rowHeader = RowHeader(self)

        self.constants = constants
        self.parameters = devValues['params'] if devValues else []

        self.fillUnitWidget()
        self.fillTypecodeWidget(devValues['typecode'] if devValues else None)

        if devValues:
            self.setInitialValues(devValues)

        self.typecodeWidget.currentIndexChanged.connect(self.setParamsAmount)
        self.paramsWidget.paramAmountChanged.connect(self.adjustTypecode)
        self.paramsWidget.paramAmountAboutToChange.connect(self.checkTypecodeChange)
        self.confirmTypecodeChange.connect(self.paramsWidget.confirmTypecodeChange)

    def setParamsAmount(self):
        device = self.constants.deviceTypeCodes[self.getTypecode()][1]
        if device[0].DISPLAYNAME in self.constants.FLATDEVICES:
            self.paramsWidget.updateParameterAmount(device[1])
            self.paramsWidget.setMode(ParamsCellWidget.PARAM)
        elif device[0].DISPLAYNAME in self.constants.PARAMETERDEVICES:
            if self.paramsWidget.paramAmount == 0:
                self.paramsWidget.updateParameterAmount(1)
            self.paramsWidget.setMode(ParamsCellWidget.PARAM)
        else:
            self.paramsWidget.updateParameterAmount(0)
            self.paramsWidget.setMode(ParamsCellWidget.NOPARAM)

    @pyqtSlot(int)
    def checkTypecodeChange(self, paramamount):
        if self.getDisplayname() in self.constants.FLATDEVICES \
                and paramamount > 16:
            self.confirmTypecodeChange.emit()

    @pyqtSlot()
    def adjustTypecode(self):
        paramamount = self.paramsWidget.paramAmount
        if paramamount == len(self.parameters):
            return

        name = self.getDisplayname()
        if name in self.constants.FLATDEVICES:
            if paramamount > 16:
                if name == 'FlatOutput':
                    name = 'ParamOutput'
                    addition = ''
                elif name == 'FlatInput':
                    name = 'ParamInput'
                    addition = ''
            else:
                addition = '(%d)' % paramamount

            for i in range(self.typecodeWidget.count()):
                text = self.typecodeWidget.itemText(i)
                if name in text and addition in text:
                    self.typecodeWidget.setCurrentIndex(i)
                    return

    def resetVersion(self, constants):
        self.constants = constants
        self.fillTypecodeWidget()
        self.fillUnitWidget()
        self.paramsWidget.editConstants(constants)

    def fillTypecodeWidget(self, typecode=None):
        typeCodes = []
        index = 0
        for i, key in enumerate(sorted(self.constants.deviceTypeCodes)):
            if key == typecode:
                index = i
            device = self.constants.deviceTypeCodes[key][1]
            if len(device) == 2:
                name = '%s (%d)' % (device[0].DISPLAYNAME, device[1])
            else:
                name = device[0].DISPLAYNAME
            typeCodes.append('%s\t%s' % (hex(key), name))

        self.typecodeWidget.clear()
        self.typecodeWidget.addItems(typeCodes)
        self.typecodeWidget.setCurrentIndex(index)

    def fillUnitWidget(self):
        self.unitWidget.clear()
        # XXX: better split the widget into two: for unit and exponent
        items = sorted(s for s in self.constants.getUnitsList()
                       if isinstance(s, str) and '10^' not in s)
        self.unitWidget.addItems(items)

    def setInitialValues(self, devValues):
        self.nameWidget.setText(devValues['name'])

        unit = devValues['unit']

        if isinstance(unit, int):
            unit = self.constants.units[unit]

        self.unitWidget.setCurrentIndex(self.unitWidget.findText(unit))

        self.setParamsAmount()
        if devValues['params']:
            self.paramsWidget.setParams(devValues['params'])

    @pyqtSlot(list)
    def adjustParameters(self, paramList):
        self.parameters = paramList

    def getDeviceDict(self):
        return dict(
            name=self.getName(),
            unit=self.constants.extUnits[self.unitWidget.currentText()],
            typecode=self.getTypecode(),
            params=self.getParams(),
        )

    def getName(self):
        return self.nameWidget.text()

    def getTypecode(self):
        return int(self.typecodeWidget.currentText()[:6], 16)

    def getParams(self):
        return self.paramsWidget.buttonParams.compList

    def getDisplayname(self):
        return self.constants.deviceTypeCodes[self.getTypecode()][1][0].DISPLAYNAME


class CodeDevice(BaseDevice):
    deviceRequest = pyqtSignal()

    def __init__(self, constants, devValues, index, parent=None):
        super(CodeDevice, self).__init__(constants, devValues, index, parent)
        if devValues and 'aux' in devValues:
            reasons = devValues['aux']
        else:
            reasons = []
        self.auxWidget = AuxCellWidget(reasons)
        self.adjustAuxWidget()
        self.adevsWidget = QPushButton('0')
        self.moduleWidget = QLineEdit()
        self.moduleWidget.setValidator(QRegExpValidator(QRegExp('[kKeE][lLsSmM][1-9][0-9]{3}(-[0-9]{4})?')))

        self.adevs = []

        if devValues:
            self.setExtendedValues(devValues)

        self.adevsWidget.clicked.connect(self.initiateAdevInquiry)
        self.typecodeWidget.currentIndexChanged.connect(self.adjustAuxWidget)

    def initiateAdevInquiry(self):
        self.deviceRequest.emit()

    def setExtendedValues(self, devValues):
        if 'module' in devValues and devValues['module']:
            self.moduleWidget.setText(devValues['module'])

    def showAdevDialog(self, devices):
        adevDialog = AdevsDialog(devices, self.adevs, self)
        if adevDialog.exec_():
            self.adevs = adevDialog.getAdevs()
            self.updateAdevWidgetText()

    def updateAdevWidgetText(self):
        self.adevsWidget.setText(str(len(self.adevs)))

    def setAuxReasons(self, reasons):
        self.auxWidget.setAuxReasons(reasons)

    @pyqtSlot()
    def adjustAuxWidget(self):
        self.auxWidget.adjustButton(self.constants.auxAmounts[self.getTypecode()])

    def removeAdev(self, dev):
        if dev in self.adevs:
            self.adevs.remove(dev)
            self.adevsWidget.setText(str(len(self.adevs)))

    def getDeviceDict(self):
        deviceDict = BaseDevice.getDeviceDict(self)
        deviceDict.update(dict(adevs=[dev.getName() for dev in self.adevs],
                               module=self.moduleWidget.text(),
                               aux=self.auxWidget.getReasons(),
                              )
                         )
        return deviceDict

    def isGeneratable(self):
        # to be extended when more conditions have to be checked
        return all([self.paramsWidget.buttonParams.compsSatisfactory])


class PlutoDevice(BaseDevice):
    def __init__(self, constants, devValues, index, parent=None):
        super(PlutoDevice, self).__init__(constants, devValues, index, parent)

        self.size = 1

        self.offsetWidget = ValueEdit()
        self.offsetWidget.setValidator(Int16Validator())
        self.sizeWidget = ValueEdit()
        self.sizeWidget.setValidator(Int16Validator())
        self.offsetWidget.setValidator(Int16Validator())
        self.sizeWidget.setReadOnly(True)
        self.adjustSize()

        self.typecodeWidget.currentIndexChanged.connect(self.adjustSize)

    def adjustSize(self):
        self.sizeWidget.updateValue(int(hex(self.getTypecode())[4:], 16))

    def getSize(self):
        return int(self.sizeWidget.text())

    def getOffset(self):
        return int(self.offsetWidget.text()) if self.offsetWidget.text() else 0

    def setOffset(self, offset):
        self.offsetWidget.setText(offset)
        self.offsetWidget.setDisplayState(self.offsetWidget.DEFAULT)

    def getDeviceDict(self):
        baseDict = BaseDevice.getDeviceDict(self)

        return dict(
            deviceCode=self.getTypecode(),
            number=-1,  # TODO: add propper number?
            deviceType=self.getDeviceType(),
            name=baseDict['name'],
            unit=baseDict['unit'],
            size=self.getSize(),
            firmware='',
            register=self.getOffset(),
            subType=self.getSubtype(),
            params=[baseDict['params']],
            names=[]  # nosubsyet
        )

    def getDeviceType(self):
        return self.constants.deviceTypeCodes[self.getTypecode()][0]

    def getSubtype(self):
        return self.constants.deviceTypeCodes[self.getTypecode()][1]


class RowHeader(QTableWidgetItem):
    def __init__(self, device):
        super(RowHeader, self).__init__()
        self.setText('>')
        self.device = device


class ParamsCellWidget(QWidget):
    paramAmountChanged = pyqtSignal(int)
    paramAmountAboutToChange = pyqtSignal(int)
    confirmTypecodeChange = pyqtSignal()

    NOPARAM = 0
    PARAM = 1

    def __init__(self, constants, parent=None):
        super(ParamsCellWidget, self).__init__(parent)

        # code completion block start
        from pluto.gui.dialogs.editdevicedialogs import ParamSubButton
        self.buttonParams = ParamSubButton()
        self.spinBox = QSpinBox()
        # code completion block end

        uic.loadUi(getUiPath('ParamsCellWidget.ui'), self)

        self.editConstants(constants)
        self.mode = 0
        self.paramAmount = 0

        self.buttonParams.compsChanged.connect(self.updateLabel)
        self.buttonParams.parameterAmountAboutToChange.connect(self.paramAmountAboutToChange)
        self.confirmTypecodeChange.connect(self.buttonParams.confirmTypecodeChange)

        self.adjustVisibility()

    def setMode(self, mode):
        if mode in [self.NOPARAM, self.PARAM]:
            self.mode = mode
            self.adjustVisibility()
        else:
            logging.warning('%s: Attempt to set invalid mode:  %d.',
                            self.__name__, mode)

    def adjustVisibility(self):
        if self.mode == self.NOPARAM:
            self.label.hide()
            self.buttonParams.hide()
        elif self.mode == self.PARAM:
            self.updateLabel()
            self.label.show()
            self.buttonParams.show()
        else:
            logging.error('%s: Invalid mode set: %d', self.__name__, self.mode)

    @pyqtSlot(int)
    def updateParameterAmount(self, amount):
        self.paramAmount = amount
        self.buttonParams.show()
        if not self.buttonParams.compList:
            self.buttonParams.editCompList([0] * amount)
            return
        # remove dublicates from list
        params = list(set(self.buttonParams.compList))
        params.sort()
        if len(params) > amount:
            params = params[:amount]
        else:
            for _ in range(amount - len(params)):
                params.append(0)

        params.sort()

        self.buttonParams.editCompList(params)
        self.updateLabel()

    def updateLabel(self):
        self.paramAmount = len(self.buttonParams.compList)
        self.label.setText(str(self.paramAmount))
        self.paramAmountChanged.emit(self.paramAmount)

    def editConstants(self, constants):
        self.buttonParams.editConstants(constants)

    def getParams(self):
        return sorted(list(set(self.buttonParams.compList)))

    def setParams(self, params):
        self.buttonParams.compList = params
        self.updateLabel()


class AuxCellWidget(QWidget):
    reasonsChanged = pyqtSignal()

    def __init__(self, reasons, parent=None):
        super(AuxCellWidget, self).__init__(parent)
        uic.loadUi(getUiPath('AuxCellWidget.ui'), self)

        self.maxAuxAmount = 0
        self.auxReasons = reasons

        self.adjustButton()

        self.buttonAux.clicked.connect(self.showEditAuxDialog)

    def adjustButton(self, maxAuxAmount=None):
        if maxAuxAmount:
            self.maxAuxAmount = maxAuxAmount
        if self.maxAuxAmount:
            auxAmount = 0
            for reason in self.auxReasons:
                if reason:
                    auxAmount += 1

            auxAmount = min(auxAmount, self.maxAuxAmount)

            self.buttonAux.setText('%d/%d' % (auxAmount,
                                              self.maxAuxAmount))
            self.buttonAux.show()
        else:
            self.buttonAux.hide()

    def showEditAuxDialog(self):
        auxDialog = EditAuxDialog(self.maxAuxAmount, self.auxReasons)

        if auxDialog.exec_():
            self.auxReasons = auxDialog.getReasons()
            self.adjustButton()
            self.reasonsChanged.emit()

    def getReasons(self):
        reasons = [reason for reason in self.auxReasons if reason]

        if len(reasons) <= self.maxAuxAmount:
            return reasons
        return reasons[:self.maxAuxAmount]

    def getMaxReasonLength(self):
        length = 0
        for reason in self.auxReasons:
            if len(reason) > length:
                length = len(reason)

        return length


class AdevsDialog(QDialog):
    def __init__(self, devices, selected, parentDevice, parent=None):
        super(AdevsDialog, self).__init__(parent)

        # code completion block start
        self.deviceLayout = QVBoxLayout()
        # code completion block end

        uic.loadUi(getUiPath('AdevDialog.ui'), self)

        self.devices = []
        self.setAdevs(devices, selected, parentDevice)
        self.resize(self.sizeHint())

    def setAdevs(self, devices, selected, parent):
        for device in devices:
            if device == parent:
                continue
            devCheckbox = DevCheckbox(device, device in selected)
            self.deviceLayout.addWidget(devCheckbox)
            self.devices.append(devCheckbox)

        self.deviceLayout.addStretch()

    def getAdevs(self):
        return [checkBox.device for checkBox in self.devices if checkBox.isChecked()]


class DevCheckbox(QCheckBox):
    def __init__(self, device, checked, parent=None):
        super(DevCheckbox, self).__init__(parent)
        self.device = device
        self.setChecked(checked)
        self.setText(self.device.getName())


class MouseWheelFilter(QObject):
    def eventFilter(self, obj, event):
        if event.type() == QEvent.Wheel:
            return True
        return QObject.eventFilter(self, obj, event)
