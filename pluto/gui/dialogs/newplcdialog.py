# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
from pluto.model.communication.utils import getConnectionDevice, \
    getValidAddresses
from pluto.qt import uic, pyqtSlot, QSettings, QDialog, QMessageBox, QComboBox

from pluto import getUiPath
from pluto.model.versions import getSupportedVersions
from pluto.model.communication.tango import TANGO


class NewPLCDialog(QDialog):
    def __init__(self, parent=None):
        super(NewPLCDialog, self).__init__(parent)

        # code completion block start
        combobox = QComboBox()
        self.comboBoxCommunication = combobox
        self.comboBoxAddress = combobox
        # code completion block end

        uic.loadUi(getUiPath('NewPLC.ui'), self)
        settings = QSettings()
        self.knownHosts = (settings.value('knownHosts', type=str)
                           if settings.contains('knownHosts') else [])
        self.lastHost = (settings.value('lastHost', type=str)
                         if settings.contains('lastHost') else '')
        self.addConnectionOptions()
        if self.lastHost.startswith('tango://'):
            self.comboBoxCommunication.setCurrentIndex(self.comboBoxCommunication.findText('TANGO'))
        elif self.lastHost.startswith('ads://'):
            self.comboBoxCommunication.setCurrentIndex(self.comboBoxCommunication.findText('ADS'))
        self.adjustAddress(self.comboBoxCommunication.currentIndex())
        self.fillVersionBox()

        self.widgetRawMode.setVisible(False)
        self.widgetConversion.setVisible(False)
        self.comboBoxAddress.setFocus()
        self.resize(self.sizeHint())

        self.buttonBox.accepted.connect(self.checkData)
        self.checkBoxRawMode.toggled.connect(self.updateConversionVisibility)
        self.checkBoxRawMode.toggled.connect(self.uncheckIndexerCheckbox)
        self.checkBoxIndexer.toggled.connect(self.updateConversionVisibility)
        self.comboBoxCommunication.currentIndexChanged.connect(self.adjustAddress)

    def checkData(self):
        if self.comboBoxAddress.currentText() == '':
            msgBox = QMessageBox()
            msgBox.setWindowTitle('No address')
            msgBox.setText('Need Address to connect to.')
            msgBox.setIcon(QMessageBox.Warning)
            msgBox.exec_()
        else:
            self.accept()

    @pyqtSlot(int)
    def adjustAddress(self, index):
        comdev = getConnectionDevice(self.comboBoxCommunication.itemText(index))

        hosts = getValidAddresses(comdev, self.knownHosts)

        self.comboBoxAddress.clear()
        self.comboBoxAddress.addItems(hosts)

        if self.lastHost in hosts:
            self.comboBoxAddress.setCurrentIndex(hosts.index(self.lastHost))

        self.addressTemplateLabel.setText(comdev.TEMPLATE)
        self.addressTemplateLabel.setToolTip(comdev.HELP)

    def fillVersionBox(self):
        versions = getSupportedVersions()
        self.comboBoxVersion.addItems(versions)
        self.comboBoxVersion.setCurrentIndex(self.comboBoxVersion.count() - 1)

    def addConnectionOptions(self):
        if TANGO:
            self.comboBoxCommunication.addItem('TANGO')
        self.comboBoxCommunication.addItem('ADS')

    def getRefresh(self):
        return self.timerWidget.getRefresh()

    def getFirstRegister(self):
        return self.spinBoxFirst.value()

    def getLastRegister(self):
        return self.spinBoxLast.value()

    def getConversion(self):
        return ('%sf' % self.comboBoxFloat.currentText(),
                '%sHH' % self.comboBoxUShort.currentText())

    def getVersion(self):
        return self.comboBoxVersion.currentText()

    def getAddress(self):
        return self.comboBoxAddress.currentText()

    def getOffset(self):
        return 0

    def getName(self):
        return self.lineEditName.text() or 'PLC'

    def getStats(self):
        return {
            'address': self.getAddress(),
            'name': self.getName(),
            'refresh': self.getRefresh(),
            'offset': self.getOffset()
        }

    def getRawModeData(self):
        if self.checkBoxRawMode.isChecked():
            return {
                'first': self.getFirstRegister(),
                'last': self.getLastRegister(),
                'conversion': self.getConversion(),
                'version': self.getVersion()
            }
        return None

    def getManualModeData(self):
        if self.checkBoxRawMode.isChecked() or self.checkBoxIndexer.isChecked():
            return None
        return {'conversion': self.getConversion(),
                'version': self.getVersion()}

    def getCommunicationMode(self):
        return self.comboBoxCommunication.currentText()

    @pyqtSlot()
    def updateConversionVisibility(self):
        if (not self.checkBoxRawMode.isChecked() and
                self.checkBoxIndexer.isChecked()):
            self.widgetConversion.setVisible(False)
        else:
            self.widgetConversion.setVisible(True)

    @pyqtSlot()
    def uncheckIndexerCheckbox(self):
        self.checkBoxIndexer.setChecked(False)

    def getUseIndexer(self):
        return self.checkBoxIndexer.isChecked()

    def accept(self):
        # save addresslist
        knownHosts = self.knownHosts
        lastHost = self.getAddress()

        if lastHost not in knownHosts:
            knownHosts.append(lastHost)

        settings = QSettings()
        settings.setValue('knownHosts', knownHosts)
        settings.setValue('lastHost', lastHost)
        del settings

        return QDialog.accept(self)
