# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2015 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA


from collections import OrderedDict
from os import makedirs, listdir
from os.path import join, exists, split, dirname, isdir, abspath

from pluto.qt import uic, pyqtSlot, QDialog, QLineEdit, QComboBox, \
    QPushButton, QFileDialog, QSpinBox, QCheckBox, QLabel

from pluto import getUiPath, saveAsIni, getCurrentTimeString, loadFromIni
from pluto.gen import PLC, PLCPROJECT
from pluto.devices.fields.validators import AsciiValidator
from pluto.gui.dialogs.devicesdisplay import CodeDevice
from pluto.model.versions import getSupportedVersions


def generatePLCCode(devices, plcSpec):
    codeDialog = PLCCodeDialog(devices, plcSpec)

    codeDialog.exec_()


class PLCCodeDialog(QDialog):

    DEFAULTDIR = abspath(join(dirname(__file__), '..', '..', '..', 'PLCCodes'))

    def __init__(self, devices, plcSpecs, parent=None):
        super(PLCCodeDialog, self).__init__(parent)

        # code completion block start
        from pluto.gui.dialogs.devicesdisplay import CodeDevicesDisplay

        lineEdit = QLineEdit()
        spinBox = QSpinBox()
        pushButton = QPushButton()
        comboBox = QComboBox()
        checkBox = QCheckBox()
        label = QLabel()
        self.lineEditName = lineEdit
        self.lineEditVersion = lineEdit
        self.lineEditAuthor1 = lineEdit
        self.lineEditAuthor2 = lineEdit
        self.lineEditFileName = lineEdit
        self.spinSize = spinBox
        self.spinOffset = spinBox
        self.comboBoxMagic = comboBox
        self.comboBoxCoupler = comboBox
        self.buttonGenerate = pushButton
        self.buttonCancel = pushButton
        self.checkOneFile = checkBox
        self.labelExistingFilesWarning = label
        self.codeDevicesDisplay = CodeDevicesDisplay()
        # code completion block end

        uic.loadUi(getUiPath('PLCCodeDialog.ui'), self)

        self.indexerrelevants = [self.lineEditName,
                                 self.lineEditVersion,
                                 self.lineEditAuthor1,
                                 self.lineEditAuthor2]
        self.path = self.lineEditFileName.text()
        self.lastConfigSave = ''

        self.fillMagicBox(plcSpecs['version'])
        self.fillLineEdits(plcSpecs)
        self.setVersion()
        self.setIndexerStats(plcSpecs)
        self.comboBoxCoupler.addItems(PLC.COUPLER_TYPES)

        validator = AsciiValidator()

        self.lineEditName.setValidator(validator)
        self.lineEditAuthor1.setValidator(validator)
        self.lineEditAuthor2.setValidator(validator)

        self.buttonFileName.clicked.connect(self.inquireFileName)
        self.comboBoxMagic.currentIndexChanged.connect(self.setVersion)
        self.lineEditName.textChanged.connect(self.adjustIndexerSize)
        self.lineEditVersion.textChanged.connect(self.adjustIndexerSize)
        self.lineEditAuthor1.textChanged.connect(self.adjustIndexerSize)
        self.lineEditAuthor2.textChanged.connect(self.adjustIndexerSize)
        self.checkOneFile.stateChanged.connect(self.adjustPath)
        self.codeDevicesDisplay.deviceAdded.connect(self.handleDevAddition)
        self.codeDevicesDisplay.deviceRemoved.connect(self.handleDevRemoval)
        self.buttonGenerate.clicked.connect(self.generateCode)
        self.buttonSave.clicked.connect(self.saveConfig)
        self.buttonLoad.clicked.connect(self.loadConfig)

        self.addDevices(devices)
        self.adjustPath()

    def setVersion(self):
        version = self.comboBoxMagic.currentText().replace('.', '_')
        try:
            self.constants = eval('v%s.VersionConstants' % version)()
        except NameError:
            # pylint: disable=exec-used
            exec('from pluto.model.versions import v%s' % version)
            self.constants = eval('v%s.VersionConstants' % version)()

        self.codeDevicesDisplay.constants = self.constants

        for device in self.codeDevicesDisplay.devices:
            device.resetVersion(self.constants)

    def fillMagicBox(self, version):
        versions = getSupportedVersions()
        if '2014.0701' in versions:
            versions.remove('2014.0701')
        self.comboBoxMagic.addItems(versions)
        versionIndex = self.comboBoxMagic.findText(version)
        if versionIndex != -1:
            self.comboBoxMagic.setCurrentIndex(versionIndex)

    def fillLineEdits(self, specs):
        specMap = [('name', self.lineEditName),
                   ('versionString', self.lineEditVersion),
                   ('author1', self.lineEditAuthor1),
                   ('author2', self.lineEditAuthor2), ]

        for key, lineEdit in specMap:
            if key in specs:
                lineEdit.setText(specs[key])

    @pyqtSlot(CodeDevice)
    def handleDevAddition(self, device):
        self.indexerrelevants.append(device)

        device.auxWidget.reasonsChanged.connect(self.adjustIndexerSize)
        (device.paramsWidget.buttonParams.compsChanged.
         connect(self.manageGenerateButtonState))

    @pyqtSlot(CodeDevice)
    def handleDevRemoval(self, device):
        self.indexerrelevants.pop(self.indexerrelevants.index(device))
        device.auxWidget.reasonsChanged.disconnect(self.adjustIndexerSize)
        (device.paramsWidget.buttonParams.compsChanged.
         disconnect(self.manageGenerateButtonState))

    @pyqtSlot()
    def adjustIndexerSize(self):
        size = 0
        for item in self.indexerrelevants:
            strLen = 0
            if isinstance(item, QLineEdit):
                strLen = len(item.text())
            elif isinstance(item, CodeDevice):
                strLen = item.auxWidget.getMaxReasonLength()
            else:
                raise NotImplementedError('No strlenfinding defined for %s (%s)' %
                                          (type(item), item))

            if strLen > size:
                size = strLen

        self.spinSize.setMinimum(size + 2)

    def setIndexerStats(self, plcSpecs):
        if plcSpecs['indexeroffset']:
            self.spinOffset.setValue(plcSpecs['indexeroffset'])

        if plcSpecs['indexersize']:
            self.spinSize.setValue(plcSpecs['indexersize'])

    def addDevices(self, devicesList):
        for paramDict in devicesList:
            self.codeDevicesDisplay.addDevice(paramDict)

    @pyqtSlot()
    def manageGenerateButtonState(self):
        self.buttonGenerate.setEnabled(all(d.isGeneratable() for d
                                           in self.codeDevicesDisplay.devices))

    def getPLCDict(self):
        return dict(
            name=self.lineEditName.text(),
            version=self.lineEditVersion.text(),
            author1=self.lineEditAuthor1.text(),
            author2=self.lineEditAuthor2.text(),
            indexer_size=self.spinSize.value(),
            indexer_offset=self.spinOffset.value(),
            magic=float(self.comboBoxMagic.currentText()),
            coupler=self.comboBoxCoupler.currentText(),
        )

    def getDeviceDicts(self):
        return [device.getDeviceDict()
                for device in self.codeDevicesDisplay.devices]

    @pyqtSlot()
    def adjustPath(self):
        if self.checkOneFile.isChecked() and self.path.endswith('.exp'):
            path = dirname(self.path)
        else:
            path = self.path

        self.lineEditFileName.setText(path)

        if isdir(path) and listdir(path):
            self.labelExistingFilesWarning.show()
        else:
            self.labelExistingFilesWarning.hide()

    def extractSavePath(self):
        path = self.lineEditFileName.text()
        if path:
            if path.endswith('.exp'):
                directory, filename = split(path)
            else:
                directory = path
                filename = None
        else:
            directory = join(self.DEFAULTDIR)
            filename = None

        if not self.directoryExists(directory):
            directory = abspath('.')

        return directory, filename

    def directoryExists(self, dirpath):
        if not exists(dirpath):
            try:
                makedirs(dirpath)
            except OSError:
                return False

        return True

    def inquireFileName(self):
        if self.path:
            directory = dirname(self.path)
        else:
            if self.directoryExists(self.DEFAULTDIR):
                directory = self.DEFAULTDIR
            else:
                directory = abspath('.')

        if self.checkOneFile.isChecked():
            path = QFileDialog.getExistingDirectory(
                caption='Save %s in' % self.lineEditName.text(),
                directory=directory,
                options=QFileDialog.ShowDirsOnly
                | QFileDialog.DontResolveSymlinks
            )
        else:
            path, _ = QFileDialog.getSaveFileName(
                caption='Save %s' % self.lineEditName.text(),
                directory=directory,
                filter='PLCTemplates (*.exp)'
            )
        if not path:
            return

        self.path = path
        self.adjustPath()

    def generateCode(self):
        plc = self.getPLCDict()
        devices = self.getDeviceDicts()

        project = PLCPROJECT()
        project.build(plc, devices)
        directory, filename = self.extractSavePath()

        if directory == self.DEFAULTDIR:

            directory = join(directory, getCurrentTimeString())
            self.directoryExists(directory)

        if self.checkOneFile.isChecked():
            project.save(dirname=directory)
        else:
            if not filename:
                filename = 'PLC_CodeTemplate.exp'
            project.save(filename=filename, dirname=directory)


        self.saveConfig(directory)
        self.checkOverwrite.setChecked(False)

    def saveConfig(self, path=None):
        data = OrderedDict()

        data['global'] = self.getPLCDict()

        data['devices'] = dict(devices = self.getDeviceDicts())

        if not path and self.checkOverwrite.isChecked() and self.lastConfigSave:
            path = self.lastConfigSave
        elif not path or isdir(path):
            directory, filename = self.extractSavePath()

            if not filename:
                filename = '.'.join([getCurrentTimeString(), 'pgs'])
            else:
                filename = '.'.join([filename[:-4], 'pgs'])

            path = join(path if path else directory, filename)

        saveAsIni(data, path)

        self.labelSetupSavePath.setText('Setup saved to %s ' % path)
        self.lastConfigSave = path

    def loadConfig(self):
        loadPath, _ = QFileDialog.getOpenFileName(
            caption='Load Setup',
            directory=self.DEFAULTDIR,
            filter='Pluto generation setups (*.pgs)')

        if not loadPath:
            return

        data = loadFromIni(loadPath)

        globs = data['global']
        self.lineEditName.setText(globs['name'])
        self.lineEditVersion.setText(globs['version'])
        self.lineEditAuthor1.setText(globs['author1'])
        self.lineEditAuthor2.setText(globs['author2'])

        self.spinOffset.setValue(globs['indexer_offset'])
        self.spinSize.setValue(globs['indexer_size'])
        for i in range(self.comboBoxMagic.count()):
            if self.comboBoxMagic.itemText(i) == globs['magic']:
                self.comboBoxMagic.setCurrentIndex(i)
                break
        for i in range(self.comboBoxCoupler.count()):
            if self.comboBoxCoupler.itemText(i) == globs['coupler']:
                self.comboBoxCoupler.setCurrentIndex(i)
                break

        adevs = {}
        for device in data['devices']['devices']:
            self.codeDevicesDisplay.addDevice(device)
            if 'adevs' in device:
                adevs[device['name']] = device['adevs']

        self.codeDevicesDisplay.addAdevs(adevs)
