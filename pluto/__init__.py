# -*- coding: utf-8 -*-
# PLC Debug Tool (Pluto)
# Copyright (C) 2014 Stefan Rainow
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

import sys
import pickle
from os.path import join, dirname
from time import strftime
from collections import OrderedDict
try:
    from configparser import RawConfigParser
except ImportError:
    from ConfigParser import RawConfigParser

from pluto.version import get_version

__version__ = get_version()

DEFAULTAUX = 'Aux%s'
DEFAULTDEV = 'Device%s'


# pylint: disable=protected-access
# pylint: disable=no-member
def resource_path(*elements):
    if hasattr(sys, "_MEIPASS"):
        return join(sys._MEIPASS, *elements)
    return join(dirname(__file__), '..', *elements)


def convertFilename(defaultName, fileExtention):
    if defaultName.endswith(fileExtention):
        return defaultName
    return defaultName + fileExtention


def getUiPath(filename):
    return resource_path('pluto', 'gui', 'ui', filename)


def getDefaultSubName(index):
    return DEFAULTDEV % index


def isDefault(string, defaultType):
    if defaultType == 'aux':
        template = DEFAULTAUX
    elif defaultType == 'dev':
        template = DEFAULTDEV
    else:
        raise TypeError('No typematch for %s' % defaultType)

    # not very elaborate, but will do for now
    return string.startswith(template[:template.find('%')])


def getCurrentTimeString():
    return strftime('%y%m%d_%H%M%S')


def saveAsIni(sections, fileName):
    '''
    Pickles the provided dictionaries and saves them to the given file.

    :param sections: OrderedDict with the data.
    :param fileName: Filename to be saved to.
    '''

    saveParser = RawConfigParser()

    for section in sections:
        saveParser.add_section(section)
        for option in sections[section]:
            saveParser.set(section, option,
                           pickle.dumps(sections[section][option], 0).decode())

    with open(fileName, 'w') as f:
        saveParser.write(f)


def loadFromIni(fileName):
    '''
    Unpickles the data from the given file and returns it as an OrderedDict.

    :param fileName: File which holds the data.
    :return: Ordered Dict containing the data saved in filename
    '''

    data = OrderedDict()
    loadParser = RawConfigParser()
    loadParser.readfp(open(fileName))

    for section in loadParser.sections():
        options = OrderedDict()
        for option in loadParser.options(section):
            obj = loadParser.get(section, option)
            if not isinstance(obj, bytes):
                obj = obj.encode()
            options[option] = pickle.loads(obj)

        data[section] = options

    return data
